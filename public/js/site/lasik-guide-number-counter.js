$( document ).ready(function() {
	var counterStatus = true;
	
	if( $('.lasik-glance').length ) {
		var a = 0;
							
		$(window).scroll(function() {
          if (counterStatus)  {
			  var oTop = $('.lasik-glance').offset().top - window.innerHeight;
			  if (a == 0 && $(window).scrollTop() > oTop) {
				  counterStatus = false;	
				$('.count').each(function() {
				  if (a == 0)  {
					  $(this).text('');
				  }
				  var $this = $(this),
					countTo = $this.attr('data-count');
				  $({
					countNum: $this.text()
				  }).animate({
					  countNum: countTo
					},

					{

					  duration: 2000,
					  easing: 'swing',
					  step: function() {
						$this.text(Math.floor(this.countNum));
					  },
					  complete: function() {
						 countNum = parseInt( this.countNum ).toLocaleString(); 		
						$this.text(countNum);
						a = 0;
					  }

					});
				});
				a = 1;
			  }
		 	}	
		});		

		
	}				
});	