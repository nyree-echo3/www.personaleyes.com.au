$( document ).ready(function() {
	var homeCounterStatus = true;
	
	if( $('.home-intro').length ) {
		var a = 0;
		$(window).scroll(function() {
		  if (homeCounterStatus)  {	
			  var oTop = $('.home-intro').offset().top - window.innerHeight;
			  if (a == 0 && $(window).scrollTop() > oTop) {
				homeCounterStatus = false;  
				$('.count').each(function() {
				  if (a == 0)  {
					  $(this).text('');
				  }
				  var $this = $(this),
					countTo = $this.attr('data-count');
				  $({
					countNum: $this.text()
				  }).animate({
					  countNum: countTo
					},

					{

					  duration: 2000,
					  easing: 'swing',
					  step: function() {
						$this.text(Math.floor(this.countNum));
					  },
					  complete: function() {
						 countNum = parseInt( this.countNum ).toLocaleString(); 		
						$this.text(countNum);
						a = 0;
					  }

					});
				});
				a = 1;
			  }
		  }
		});
	}
});	