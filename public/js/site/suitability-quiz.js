$(document).ready(function() {
    var form = $("#frmSuitabilityQuiz");
	var hubspotForm = $("#hsForm_3352a4d4-7a7e-4931-b2af-b43f97d20721");

    form.validate({
	    rules: {
			txtEmail: {
			  required: true,
			  email: true			  
			},
			txtPhone: {
			  digits: true,
			  rangelength: [8, 10]			  
			},
			txtPostcode: {
			  required: true,
			  digits: true,
			  rangelength: [4, 4]	
			}
		},
		messages: {
		    txtPhone: "This field has a length between 8-10 digits."	,
			txtPostcode: "This field is required and length is 4."	
		},
	
        errorPlacement: function errorPlacement(error, element) { 
			if(element.attr("name") == "txtFirstName" || element.attr("name") == "txtLastName" || element.attr("name") == "txtEmail" || element.attr("name") == "txtPhone" || element.attr("name") == "selState" || element.attr("name") == "txtPostcode") {
			   element.after(error); 	
			} else  {
			   error.appendTo($("#error"));
			   //element.after(error); 
			}
		}
    });

    var settings = {
        headerTag: "h5",
        bodyTag: "section",
        stepsOrientation: "horizontal",
        autoFocus: true,
		titleTemplate: "#title#",		
        labels:
            {
                finish: "Send me my results!",
            },
        onInit: function (event, currentIndex)
        {												
            $("#frmSuitabilityQuiz").show();  			
        },
		onStepChanging: function (event, currentIndex, newIndex)
        {								
		    // Google Tag Manager - Custom Event Trigger
			buttonIndex = currentIndex + 1;
			dataLayer.push({'event': 'suitability-button' + buttonIndex});
            //
			
			if (currentIndex > newIndex) {				
				return (true);
			} else {				
               form.validate().settings.ignore = ":disabled,:hidden";
               return form.valid();
			}
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            var result = form.valid();

            if(result){
                iAgeGroup = $('input[type=radio][name=radAgeGroup]:checked').val();
                iGlassesContacts = $('input[type=radio][name=radGlassesContacts]:checked').val();
                iPrescription = $('input[type=radio][name=radPrescription]:checked').val();

                iKeratoconus = $('input[type=checkbox][name=chkEyeConditionKeratoconus]:checked').length;
                iCataracts = $('input[type=checkbox][name=chkEyeConditionCataracts]:checked').length;
                iOcularHerpes = $('input[type=checkbox][name=chkEyeConditionOcularHerpes]:checked').length;
                iRetinalDesease = $('input[type=checkbox][name=chkEyeConditionRetinalDisease]:checked').length;

                //if (iAgeGroup != "Over 56 years" && iAgeGroup != "Under 18" && iGlassesContacts != "Only for reading" && iPrescription != "Yes" && iKeratoconus == 0 && iCataracts == 0 && iOcularHerpes == 0 && iRetinalDesease == 0)  {
                if (iAgeGroup != "Over 56 years" && iAgeGroup != "Under 18" && iGlassesContacts != "Only for reading" && iPrescription != "Yes" && iOcularHerpes == 0 && iRetinalDesease == 0)  {

                    if ($('select[name=selState]').val() == "NSW" || $('select[name=selState]').val() == "ACT")  {
                        $("#hsForm_3352a4d4-7a7e-4931-b2af-b43f97d20721").submit();
                    }

                    if (iKeratoconus == 0 && iCataracts == 0)	{
                        $("#frmSuitabilityQuiz").attr('action', $('#frmSuitabilityQuiz').attr('action') + "/suitable");
                        $("#frmSuitabilityQuiz").submit();
                    } else  {
                        $("#frmSuitabilityQuiz").attr('action', $('#frmSuitabilityQuiz').attr('action') + "/maybe-suitable");
                        $("#frmSuitabilityQuiz").submit();
                    }
                } else  {
                    $("#frmSuitabilityQuiz").attr('action', $('#frmSuitabilityQuiz').attr('action') + "/not-suitable");
                    $("#frmSuitabilityQuiz").submit();
                }
			}
        },
        onFinished: function (event, currentIndex)
        {


        }
    };

        settings.enableAllSteps = true;
        settings.startIndex = 0;
   

    form.steps(settings);
    
        $("li[role='tab']").removeClass('current').addClass('done');
        //form.validate().settings.ignore = ":disabled";		
	
	$('input[type=radio][name=radAgeGroup]').change(function() {		
		$('[name="age_group"]').removeAttr('checked');
		$("input[name=age_group][value='" + $(this).val() + "']").prop('checked', true);			
	});
	
	$('input[type=radio][name=radGlassesContacts]').change(function() {		
		$('[name="when_do_you_wear_glasses_or_contacts_"]').removeAttr('checked');
		$("input[name=when_do_you_wear_glasses_or_contacts_][value='" + $(this).val() + "']").prop('checked', true);			
	});
	
	$('input[type=radio][name=radAstigmatism]').change(function() {		
		$('[name="do_you_have_astigmatism_"]').removeAttr('checked');
		$("input[name=do_you_have_astigmatism_][value='" + $(this).val() + "']").prop('checked', true);			
	});
	
	$('input[type=radio][name=radPrescription]').change(function() {		
		$('[name="has_your_prescription_changed_in_the_last_12_months_"]').removeAttr('checked');
		$("input[name=has_your_prescription_changed_in_the_last_12_months_][value='" + $(this).val() + "']").prop('checked', true);			
	});
	
	$('input[type=radio][name=radPrescription]').change(function() {		
		$('[name="has_your_prescription_changed_in_the_last_12_months_"]').removeAttr('checked');
		$("input[name=has_your_prescription_changed_in_the_last_12_months_][value='" + $(this).val() + "']").prop('checked', true);			
	});
	
	// Eye Conditions
	$('input[type=checkbox][name=chkEyeConditionGlaucoma]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Glaucoma']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Glaucoma']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionDryEyes]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Dry eyes']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Dry eyes']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionKeratoconus]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Keratoconus']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Keratoconus']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionCornealScarring]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Corneal Scarring']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Corneal Scarring']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionCataracts]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Cataracts']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Cataracts']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionOcularHerpes]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Ocular herpes']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Ocular herpes']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionRetinalDisease]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Retinal disease']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Retinal disease']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionNoneOfThese]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='None of these']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='None of these']").prop('checked', false);	
		}		
	});
	
	$('input[type=checkbox][name=chkEyeConditionNotSure]').change(function() {		
		if ($(this).is(":checked")) {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Not sure']").prop('checked', true);	
		} else  {
			$("input[name=do_you_have_any_other_eye_conditions_][value='Not sure']").prop('checked', false);	
		}		
	});
	
	// User Details
	$('input[name=txtFirstName]').change(function() {		
		$("input[name=firstname]").val($(this).val());			
	});
	
	$('input[name=txtLastName]').change(function() {				
		$("input[name=lastname]").val($(this).val());			
	});
	
	$('input[name=txtEmail]').change(function() {				
		$("input[name=email]").val($(this).val());			
	});		
	
	$('select[name=selState]').change(function() {			
		$("select[name=location_state]").val($(this).val());			
	});
});

function updateValues()  {	
	$('input:checkbox').prop('checked', false);	
	
	$('input[name="txtFirstName"]').val($("input[name=firstname]").val());   
	$('input[name="txtLastName"]').val($("input[name=lastname]").val());   
	$('input[name="txtEmail"]').val($("input[name=email]").val());   	
	$('select[name="selState"]').val($("select[name=location_state]").val());   	
}