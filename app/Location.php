<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Location extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'locations';

    public $sortable = ['title', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function members()
    {
        return $this->belongsToMany(TeamMember::class);
    }

    public function getServicesArrayAttribute()
    {
        return $this->jsonToArray($this->attributes['services']);
    }

    private function jsonToArray($value)
    {
        $items = json_decode($value);
        $array = array();

        if($items!=''){

            $count = 0;
            foreach ($items as $item) {
                array_push($array, $item->{$count});
                $count++;
            }

        }
        return $array;
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('locations-filter');
        $select = $query;
        
        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
