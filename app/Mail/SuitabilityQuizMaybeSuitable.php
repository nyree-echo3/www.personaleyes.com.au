<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Setting;

class SuitabilityQuizMaybeSuitable extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;
	public $eye_conditions;
	public $eye_conditions_alt;
	public $this_these;

    public function __construct($first_name, $eye_conditions, $eye_conditions_alt, $this_these)
    {
        $this->first_name = $first_name;
		$this->eye_conditions = $eye_conditions;
		$this->eye_conditions_alt = $eye_conditions_alt;
		$this->this_these = $this_these;
    }

    public function build()
    {

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value; 
		
		$settings = Setting::where('key', '=', 'suitability-quiz-maybe-suitable')->first();
        $suitability_quiz_maybe_suitable = $settings->value;  				
		
        return $this->subject('personalEYES | Suitability Quiz')
            ->from($contactEmail)
            ->view('site/emails/suitability-quiz-maybe-suitable', array(         
				'first_name' => $this->first_name,				
				'suitability_quiz_maybe_suitable' => $suitability_quiz_maybe_suitable				
			   ));	
    }
}
