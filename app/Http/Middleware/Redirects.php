<?php

namespace App\Http\Middleware;

use Closure;

class Redirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $redirects = [
            //About
            'why-us/advanced-equipment' => 'why-us/about-us',
            'why-us/commitment-to-safety' => 'why-us/about-us',
            'why-us/experience-reputation' => 'why-us/about-us',
            'why-us/lifetime-of-vision-commitment' => 'why-us/about-us',
            'why-us/overview' => 'why-us/about-us',
            'why-us/personalised-care' => 'why-us/about-us',
            //'why-us/six-point-yearly-eye-checkup' => 'why-us/about-us',
            'why-us/useful-links' => 'why-us/about-us',
            'why-us/careers' => 'why-us/about-us',
            //Our Doctors & Opthalmologists
            //'our-doctors-and-ophthalmologists/dr-richard-parker' => 'our-doctors-and-ophthalmologists',
            'our-doctors-and-ophthalmologists/dr-phil-larkin' => 'our-doctors-and-ophthalmologists',
            //Eye Conditions
            'vision/overview' => 'vision',
            'vision/symptoms' => 'vision',
            //Treatments
			'near-vision/presbia-flexivue-microlens' => 'near-vision',
            'cataracts/laser-cataract-surgery-customlens' => 'cataracts',
            'dry-eyes/additional-tests' => 'dry-eyes',
            'dry-eyes/dry-eye-assessment' => 'dry-eyes',
            'dry-eyes/dry-eye-treatment-options' => 'dry-eyes',
            'dry-eyes/lipiflow' => 'dry-eyes',
            'dry-eyes/types-of-dry-eyes' => 'dry-eyes',
            'dry-eyes/what-causes-dry-eyes' => 'dry-eyes',
            'near-vision/raindrop-near-vision-inlay' => 'near-vision',
            'near-vision/supracor' => 'near-vision',
            'other-services/kera-rings' => 'services',
            'conductive-keroplasty-ck' => 'services',
            'contoura-vision' => 'services',
            'envista-iol' => 'services',
            'finevision-iol' => 'services',
            'high-mag-iol' => 'services',
            'iol-acrysof-restor' => 'services',
            'iol-crystalens' => 'services',
            'iol-lentis-mplus' => 'services',
            'visian-icl' => 'services',
            'other-services' => 'services',
            //Clinics
            'locations/north-strathfield' => 'locations/contact-details',
            'locations/nowra' => 'locations/contact-details',
            'locations/penrith' => 'locations/contact-details',
            'resources' => 'news',
            //Costs
            'costs/special-offer' => 'special-offer',
            'winter-promotion' => 'special-offer',
            'summer-promotion' => 'special-offer',
            //FAQs
            'faq/ask-the-doctor' => 'contact',
            'faq/during-the-operation' => 'faq/lasik-faqs',
            'faq/post-surgery-care' => 'faq/lasik-faqs',
            'faq/surgery-day' => 'faq/lasik-faqs',
            'find-an-affiliated-doctor' => '/',
            //All about LASIK
            'lasik/asla-correction-prk' => '/lasik',
            'lasik/iol-intraocular-lens' => '/lasik',
            'lasik/iol-verisyse-artisan' => '/lasik',
            'lasik/lasik' => '/lasik',
            'lasik/lasik-xtra' => '/lasik',
            'lasik/nasa-approved' => '/lasik',
            'lasik/personaleyes-custom-lasik' => '/lasik',
			//Articles
			'27-contact-lenses-found-womans-eye' => '/news',
			'5-signs-you-might-have-glaucoma' => '/news',
			'5-things-youre-every-day-hurting-eyes' => '/news',
			'6-childrens-habits-that-could-harm-their-sight' => '/news',
			'6-unexpected-diseases-eye-test-can-spot' => '/news',
			'7028' => '/news',
			'7316' => '/news',
			'7316' => '/news',
			'7-foods-for-beautiful-eyes-and-20-20-vision' => '/news',
			'7-sight-saving-tips-older-eyes' => '/news',
			'7-silent-signs-might-eye-cataracts' => '/news',
			'alcon-survey-reveals-people-happier-satisfied-lives-cataract-surgery' => '/news',
			'are-contact-lenses-a-safe-option-for-over-60s' => '/news',
			'astigmatism-clearing-blurry-situation' => '/news',
			'blind-family-hopes-see' => '/news',
			'canadian-woman-whose-eyeball-tattoo-went-wrong-may-eye-removed' => '/news',
			'can-vitamins-protect-blue-light-damage' => '/news',
			'closer-look-lasik-success-rates' => '/news',
			'colored-contact-lenses-halloween-costumes-safe' => '/news',
			'combating-eye-strain-workplace-eye-opening-stats' => '/news',
			'contact-lens-truth' => '/news',
			'diabetes-medication-may-protect-common-cause-blindness' => '/news',
			'diabetic-retinopathy-brush-blindness' => '/news',
			'dont-put-off-eye-checks' => '/news',
			'eating-leafy-greens-help-prevent-macular-degeneration' => '/news',
			'eating-plenty-salmon-sardines-mackerel-protects-sight-loss' => '/news',
			'end-monthly-misery-injections-sight-loss-sufferers-new-jab-needed-just-5-times-year' => '/news',
			'excimer-laser-versus-phakic-intraocular-lenses-for-the-correction-of-moderate-to-high-short-sightedness' => '/news',
			'eyeball-tattoos-even-worse-sound' => '/news',
			'eye-freckles-may-predict-sun-related-problems' => '/news',
			'eye-problems-affect-diabetes' => '/news',
			'eye-scan-reveals-alzheimers-years-symptoms-appear' => '/news',
			'eyes-hold-secrets-preventing-disease' => '/news',
			'eyes-treated-with-intracor-stable-at-2-years-patient-satisfaction-high' => '/news',
			'eye-surgeons-fulfill-dying-mans-wish-see-family' => '/news',
			'eye-tattoos-today-tonight-perth' => '/news',
			'fat-professor-eye-health-low-carb-healthy-fat-diets' => '/news',
			'focused-on-the-future-with-a-clear-vision' => '/news',
			'future-children-appearance-guide' => '/news',
			'glaucoma-genetic-risk-factors-identified' => '/news',
			'glaucoma-new-test-save-sight-millions-starting-treatment-blindness-symptoms-begin' => '/news',
			'glaucoma-risk-calculator' => '/news',
			'implant-laser-eye-surgery-frees-british-mother-of-dual-vision-woes' => '/news',
			'importance-getting-kids-vision-checked' => '/news',
			'important-wear-sunglasses-even-cloudy-days' => '/news',
			'injury-to-the-eye' => '/news',
			'instagram-model-destroys-eyes-cosmetic-surgery-gone-wrong' => '/news',
			'intrastromal-corneal-ring-segment-implantation-by-femtosecond-laser-for-keratoconus-correction' => '/news',
			'jeepers-creepers-look-after-those-peepers-1' => '/news',
			'juleye-2015' => '/news',
			'kamra-corneal-inlay-utilizes-camera-technology-sharpen-clear-blurry-near-vision-patients' => '/news',
			'kicking-bad-habits-keep-vision-sharper-longer' => '/news',
			'laser-pointer-burns-hole-boys-retina' => '/news',
			'laser-sharp-vision-20-years-of-lasik-eye-surgery-in-australia' => '/news',
			'lens-implants-new-fix-nearsightedness-astigmatism' => '/news',
			'low-dose-atropine-kids-myopia' => '/news',
			'majority-adults-eye-problems-seek-help-survey-says' => '/news',
			'lasik-eye-surgery-helping-people-in-australia-to-throw-away-their-glasses' => '/news',
			'many-facets-glaucoma' => '/news',
			'mascara-embedded-womans-eyelid' => '/news',
			'medicine-today-new-intracor-procedure-for-treatment-of-presbyopia' => '/news',
			'men-singled-out' => '/news',
			'models-eye-tattoo-goes-horribly-wrong' => '/news',
			'myopia-epidemic-can-stop' => '/news',
			'no-blue-light-smartphone-not-blinding' => '/news',
			'not-just-mediterranean-diet-secret-also-lifestyle' => '/news',
			'online-survey' => '/news',
			'orange-day-keeps-macular-degeneration-away-15-year-study' => '/news',
			'professor-cured-blindness-dogs-sets-sights-humans' => '/news',
			'queen-cataract-surgery-last-month-palace-confirms' => '/news',
			'really-toy-guns-dangerous-eyes' => '/news',
			'researchers-discover-association-hot-tea-eye-health' => '/news',
			'seconds-getting-eyelash-extensions-megan-lost-sight-two-hours' => '/news',
			'shedding-light-night-blindness' => '/news',
			'six-gene-variations-linked-to-glaucoma-risk' => '/news',
			'spring-fever-one-in-five-aussies-will-suffer-with-seasonal-dry-itchy-eyes' => '/news',
			'spring-fever-the-top-ways-to-avoid-those-springtime-allergies' => '/news',
			'star-trek-headset-blind-launched-australia' => '/news',
			'suffering-dry-eye' => '/news',
			'sun-safe-hats-not-worn-nearly-half-sydney-primary-school-students-report' => '/news',
			'sydney-ophthalmologist-eyes-off-new-ways-save-sight' => '/news',
			'taking-proper-care-eyes-avoid-vision-loss' => '/news',
			'technology-lets-vision-impaired-see-world-new-eyes' => '/news',
			'the-7-health-checks-we-delay' => '/news',
			'treating-vision-loss-help-older-adults-live-longer' => '/news',
			'the-7-health-checks-we-delay' => '/news',
			'truth-blue-light-really-cause-insomnia-increased-risk-cancer' => '/news',
			'two-perth-children-may-blinded-ferocious-magpie-attack' => '/news',
			'visian-icl-benefits' => '/news',
			'woman-goes-blind-one-eye-cosmetic-filler-injected-face' => '/news',
			'women-consider-laser-vision-correction' => '/news',
			'world-first-aprof-chandra-bala-laser-lens-removal' => '/news',
			'woman-goes-blind-one-eye-cosmetic-filler-injected-face' => '/news',
			'world-no-3-has-lasik-what-are-you-waiting-for' => '/news',
			'suitability-quiz' => '/vision-correction-suitability',
			//Broken Links 12-10-2020
			'dr-andrew-white' => '/our-doctors-and-ophthalmologists/dr-andrew-white',
			'dr-chandra-bala' => '/our-doctors-and-ophthalmologists/aprof-chandra-bala',
			'dr-kerrie-meades' => '/our-doctors-and-ophthalmologists/dr-kerrie-meades',
        ];



        foreach($redirects as $from => $to){

            if($request->path()==$from){

                return redirect($to,301);
            }
        }

        return $next($request);
    }
}
