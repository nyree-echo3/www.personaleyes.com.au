<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Helpers\NavigationBuilder;
use App\Page;
use App\PageCategory;
use Illuminate\Support\Facades\Cookie;
use App\ImagesHomeSlider;
use App\Service;

class HomeController extends Controller
{
    public function index(){ 		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
			
		Cookie::queue('services_slug', '', 45000);
		
		$builder = new NavigationBuilder();		
	    $navigation = $builder->build();
	    $footer =  $builder->footerRaw();
		
		$images = ImagesHomeSlider::where('status', '=', 'active')->where('service_id', 'like', '%null%')->orderBy('position', 'desc')->get();      
		
		if (isset($_ENV['APP_MODE']) && $_ENV['APP_MODE'] == "dev")  {
		   return view('site/holding', array(         
			'company_name' => $company_name,			
           ));	
		   
		} else  {
    	   return view('site/index', array(			
			'navigation_override' => $navigation,
			'home_style_override' => true,
			'images_override' => $images,
			'footer_override' => $footer,
        ));
		}

    }
	
	public function indexDev(){ 	
		Cookie::queue('services_slug', '', 45000);
		
		return view('site/index');			
    }
	
	public function styleGuide(){   
		Cookie::queue('services_slug', '', 45000);
		
    	return view('site/style-guide');

    }
	
	public function makeABooking(){

    	return view('site/make-a-booking', array(         
			'category_name' => "Make a Booking",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }		
	
	public function generalBooking(){

    	return view('site/general-booking', array(         
			'category_name' => "Make a General Booking",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }

    public function telehealthBooking(){

        return view('site/telehealth-booking', array(
            'category_name' => "Telehealth Booking",
            'header_image_ctrl' => "images/site/booking-header.jpg"
        ));

    }
	
	public function lasikBooking(){
        $page_slug = 'lasik';
		
        $service = Service::where('slug', '=', $page_slug)->first();

        $side_nav = '';
        $category = PageCategory::where('slug', '=', $page_slug)->first();
        if($category){
            $side_nav = (new NavigationBuilder())->buildSideNavigation($page_slug);
        }

        $builder = new NavigationBuilder();
       
		Cookie::queue('services_slug', $service->slug, 45000);
		$navigation = $builder->build($service->id, $service->slug);
		$footer =  $builder->footerRaw("", $service->slug);
		$page = 'lasik';
		$home_style_override = false;
		
		$images = ImagesHomeSlider::where('status', '=', 'active')->where('service_id', 'like', '%"' . $service->id . '"%')->orderBy('position', 'desc')->get();        

		return view('site/services/'.$page , array(
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'images_override' => $images,
            'side_nav' => $side_nav,
			'show_booking_form' => true,
        ));

    }
	
	public function ophthalmologyReferral(){
        $category = PageCategory::where('slug', '=', "healthcare-professionals")->first();	
		
		$side_nav = Page::where('status', '=', 'active')->where('category_id', '=', $category->id)->whereNull('parent_page_id')->orderBy('position', 'asc')->get();		
		
		foreach ($side_nav as $page):		   
		   $page['url'] = $page->url;	
		endforeach;				   
		
    	return view('site/ophthalmology-referral', array(         
			'side_nav' => $side_nav,
			'category_name' => "Healthcare Professionals",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function freeInfoPack(){

    	return view('site/free-info-pack', array(         
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function siteMap(){    
		$builder = new NavigationBuilder();
		$navigation = $builder->build();			
		
		$navigation = str_replace("nav-item", "", $navigation);
		$navigation = str_replace("dropdown", "", $navigation);
		$navigation = str_replace("nav-link", "", $navigation);
		
    	return view('site/site-map', array(         
			'navigation' => $navigation,			
           ));	
    }
	
	public function landingVideoDoctors(){   
		$service = Service::where('slug', '=', "lasik")->first();
		
		$builder = new NavigationBuilder();
		Cookie::queue('services_slug', $service->slug, 45000);
		$navigation = $builder->build($service->id, $service->slug);
		$footer =  $builder->footerRaw($service->id, $service->slug);
		$page = 'lasik';
		$home_style_override = false; 
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
    	return view('site/landing-video-doctors', array(         
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'home_doctors_text' => $home_doctors_text,		
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function landingSuitabilityQuiz(){   
		$service = Service::where('slug', '=', "lasik")->first();
		
		$builder = new NavigationBuilder();
		Cookie::queue('services_slug', $service->slug, 45000);
		$navigation = $builder->build($service->id, $service->slug);
		$footer =  $builder->footerRaw($service->id, $service->slug);
		$page = 'lasik';
		$home_style_override = false; 
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;    
		
    	return view('site/landing-suitability-quiz', array(         
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'home_doctors_text' => $home_doctors_text,		
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg",
			'suitability_quiz' => $suitability_quiz
           ));	

    }
	
	public function cataractBooking(){
        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
			
		Cookie::queue('services_slug', '', 45000);
		
		$builder = new NavigationBuilder();		
	    $navigation = $builder->build();
	    $footer =  $builder->footerRaw();
		
		$images = ImagesHomeSlider::where('status', '=', 'active')->where('service_id', 'like', '%null%')->orderBy('position', 'desc')->get();      
		
        return view('site/index', array(			
			'navigation_override' => $navigation,
			'home_style_override' => true,
			'images_override' => $images,
			'footer_override' => $footer,
			'show_booking_form_cataract' => true,
        ));				
    }
	
	public function thankyouInformationPack(){

    	return view('site/thankyou-info-pack', array(         
			'category_name' => "Thank-you",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function thankyouUltimateGuide(){

    	return view('site/thankyou-ultimate-guide', array(         
			'category_name' => "Thank-you",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function thankyouGeneralBooking(){

    	return view('site/thankyou-general-booking', array(         
			'category_name' => "Thank-you",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
}
