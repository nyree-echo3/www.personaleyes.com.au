<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

use App\Faq;
use App\FaqCategory;
use App\Module;
use App\Service;

class FaqsController extends Controller
{
    public function index($category_slug = "", $mode = ""){
		$module = Module::where('slug', '=', "faq")->first();
		
		$side_nav = $this->getCategories();									
		
		if (sizeof($side_nav) > 0)  {
			if ($category_slug == "")  {		   		   
			   $items = $this->getItems($side_nav[0]->id, $mode);		
			} else {
			  $category = $this->getCategory($category_slug);
			  $items = $this->getItems($category[0]->id, $mode);		
			}		
		}
		
		$builder = new NavigationBuilder();
		$navigation = $builder->build();
        $footer =  $builder->footerRaw();
		$home_style_override = true;
		
		$service_id = "";
		if (isset($category))  {
			if ($category[0]->service_id != "")  {
				$arr_service = json_decode($category[0]->service_id);

				foreach ($arr_service as $obj_service)  {				
					$service = Service::where('id', '=', $obj_service)->first();
					if (isset($service)) {					
					   Cookie::queue('services_slug', $service->slug, 45000);
					   $navigation = $builder->build($service->id, $service->slug);	
					   $home_style_override = false;
					}
				}
			}	
		}

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $this->getCategories();
            $side_nav_mode = 'auto';
        }
		
		return view('site/faqs/list', array(
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
			'items' => (sizeof($side_nav) > 0 ? $items : null),	
			'mode' => $mode,
			'category_name' => $module->display_name,
			'header_image' => $module->header_image,
			'header_image_ctrl' => $module->header_image,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'faq_category_name' => (isset($category) ? $category[0]->name : "FAQs"),
        ));

    }
	
	public function getCategories(){
		$categories = FaqCategory::whereHas("faqs")->where('status', '=', 'active')->get();
        foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}
	
	public function getCategory($category_slug){
		$categories = FaqCategory::where('slug', '=', $category_slug)->get();		
		return($categories);
	}
	
	public function getItems($category_id, $mode){
		if ($mode == "preview") {
		   $items = Faq::where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		} else {
		   $items = Faq::where('status', '=', 'active')->where('category_id', '=', $category_id)->orderBy('position', 'asc')->get();						
		}
		return($items);
	}		
	
	public function getFaqs($category_id = "", $limit = 12){
		if ($category_id == "")  {			
			$faqs = Faq::where('status', '=', 'active')						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {		    
		   $faqs = Faq::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($faqs);
	}
}
