<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class BookingController extends Controller
{    		
	public function bookingLocation(Request $request)
    {   			
		if ($request->postcode != "")  {
			// Get Location details based on entered postcode
			// ----------------------------------------------
			$xmlLocation = $this->getLatLongFromPostcodeOrSuburb($request->postcode);

			if ($xmlLocation->status != "ZERO_RESULTS")  {
				$latitude = (double)$xmlLocation->result->geometry->location->lat;
				$longitude = (double)$xmlLocation->result->geometry->location->lng;		

				// Get Locations nearby to entered postcode - closest 3
				$locations = $this->getLocationsByLatAndLng($latitude, $longitude, $request->postcode);		
				//$locations = array_slice($locations, 0, 3); 
			} else  {
				$locations = $this->getAcuityCalendars();
			}
		} else  {
			// Get Location using current location
			// -----------------------------------	
			//$request->latitude = "-37.813629"; // Melbourne - For testing only
			//$request->longitude = "144.963058"; // Melbourne - For testing only
			
			if ($request->latitude != "" && $request->longitude != "")  {
				$latitude = $request->latitude;
				$longitude = $request->longitude;		

				$current_postcode = "";
				$xmlLocation = $this->getPostcodeFromLatLong($request->latitude, $request->longitude);
				if ($xmlLocation->status != "ZERO_RESULTS")  {								
					if (isset($xmlLocation->result->address_component[6]->short_name)) {
 				       $current_postcode = (int)$xmlLocation->result->address_component[6]->short_name;
					}
				}				
				// Get Locations nearby to entered postcode - closest 3
				$locations = $this->getLocationsByLatAndLng($latitude, $longitude, $current_postcode);		
				//$locations = array_slice($locations, 0, 3); 
			} else  {
				$locations = $this->getAcuityCalendars();
			}
		}		
		
		return Response::json(['status' => 'success', 'locations' => $locations]);
    }
	
	function getLatLongFromPostcodeOrSuburb($postcodeOrSuburb){    
        $postcodeOrSuburb = urlencode(trim($postcodeOrSuburb));

		// Connect to the google geocode service 
		$file = "https://maps.google.com/maps/api/geocode/xml?address=$postcodeOrSuburb+Australia&key=AIzaSyA5papzfh_eu6xDIvk4CLmyQ_0pDcKYJA8";    

		$xml = simplexml_load_file($file);	
		return ($xml);
	}
	
	function getPostcodeFromLatLong($latitude, $longitude){            
		// Connect to the google geocode service 
		$file = "https://maps.google.com/maps/api/geocode/xml?latlng=$latitude,$longitude&key=AIzaSyA5papzfh_eu6xDIvk4CLmyQ_0pDcKYJA8";    

		$xml = simplexml_load_file($file);	
		return ($xml);
	}
	
	function getLocationsByLatAndLng($lat, $lng, $postcode = "")  {  
		$distance = 505; // 5km radius
		//$earthRadius = '6371.0'; // In km
		$latitudeRad = deg2rad($lat); 
		$longitudeRad = deg2rad($lng);

		$calendars = $this->getAcuityCalendars();			
				
		foreach ($calendars as &$calendar) {									
			$calendarPostcode = substr(trim($calendar["location"]), -4);			
			
			//$xmlCalendar = $this->getLatLongFromPostcodeOrSuburb($calendarPostcode);			
			//$calendarLatitude = (double)$xmlCalendar->result->geometry->location->lat;
            //$calendarLongitude = $xmlCalendar->result->geometry->location->lng;
			//$calendarPostcodeDistance = ROUND($earthRadius * ACOS(SIN($latitudeRad) * SIN( $calendarLatitude* PI()/180 ) + COS($latitudeRad) * COS( $calendarLatitude*PI()/180 )  *  COS( ($calendarLongitude* PI()/180) - $longitudeRad)), 1);						
			$calendarPostcodeDistance = $this->getLocationDistance($calendarPostcode, $latitudeRad, $longitudeRad);									
			
			$calendar["distance"] = $calendarPostcodeDistance;						
			
			if ($postcode != "" && substr($postcode, 0, 1) != "2" && $calendar["name"] == "Sydney CBD Clinic, NSW")  {
			   $calendar["distance"] = 0;
			}
		}				
		
		usort($calendars, function($a, $b) {
			return $a['distance'] > $b['distance'];
		});		
		
		return($calendars);
	}
	
	function getAcuityCalendars()  {
		$userID = '17659918';
		$key = '67bc89018e3ece2480eb9541399252e8';

		// URL for all calendars
		$url = 'https://acuityscheduling.com/api/v1/calendars.json';

		// Initiate curl:
		// GET request, so no need to worry about setting post vars:
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		// Grab response as string:
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// HTTP auth:
		curl_setopt($ch, CURLOPT_USERPWD, "$userID:$key");

		// Execute request:
		$result = curl_exec($ch);

		// Close the connection!
		curl_close($ch);

		$data = json_decode($result, true);
		
		return($data);
	}
	
	function getLocationDistance($postcode, $latitudeRad, $longitudeRad)  {  
		$earthRadius = '6371.0'; // In km
		
		switch ($postcode)  {
			case '2830': // Dubbo
				$calendarLatitude = -32.1754247;
				$calendarLongitude = 148.6001005;
				break;
				
			case '2850': // Mudgee
				$calendarLatitude = -32.5593561;
				$calendarLongitude = 149.5642369;
				break;
				
			case '2154': // Castle Hill
				$calendarLatitude = -33.7270691;
				$calendarLongitude = 150.9947439;
				break;
				
			case '2600': // Canberra
				$calendarLatitude = -35.3075699;
				$calendarLongitude = 149.1099161;
				break;
				
			case '2170': // Liverpool
				$calendarLatitude = -33.9477825;
				$calendarLongitude = 150.9190988;
				break;	
				
			case '2264': // Morisset
				$calendarLatitude = -33.1186851;
				$calendarLongitude = 151.4552213;
				break;	
				
			case '2121': // Epping
				$calendarLatitude = -33.7716764;
				$calendarLongitude = 151.0791612;
				break;		
				
			case '2150': // Parramatta
				$calendarLatitude = -33.8151823;
				$calendarLongitude = 151.0092954;
				break;
				
			case '2000': // Sydney
				$calendarLatitude = -33.8708464;
				$calendarLongitude = 151.2073300;
				break;	
				
			case '2134': // Burwood
				$calendarLatitude = -33.8799121;
				$calendarLongitude = 151.1024569;
				break;	
				
					
		}
		
		$calendarPostcodeDistance = ROUND($earthRadius * ACOS(SIN($latitudeRad) * SIN( $calendarLatitude* PI()/180 ) + COS($latitudeRad) * COS( $calendarLatitude*PI()/180 )  *  COS( ($calendarLongitude* PI()/180) - $longitudeRad)), 1);	
		
		return($calendarPostcodeDistance);
	}
	
}
