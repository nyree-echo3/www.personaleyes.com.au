<?php

namespace App\Http\Controllers\Site\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Auth\Guard;

use App\Setting;
use App\Member;
use App\MemberType;
use App\Mail\MembersMessageAdmin;
use App\Mail\MembersMessageUser;

use Auth;

class RegisterController extends Controller
{       
    protected $redirectTo = '/members-portal';
  
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
        $member_types = MemberType::where('status','=','active')->orderBy('position', 'desc')->get();        

        return view('site/members/register', array(
            'company_name' => $company_name,           
			'member_types' => $member_types,			
        ));

    } 
	
    public function store (Request $request)
    {
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Google Recaptcha Validation
		$rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];
		
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('register')->withErrors($validator)->withInput();
        }
		
		// Field Validation
	    $rules = array(                        
            'membershipType' => 'required',
			'firstName' => 'required',
			'lastName' => 'required',
			'phoneNumber' => 'required',
			'email' => 'required|string|email|max:255|unique_store:members',
			'address1' => 'required',
			'suburb' => 'required',
			'state' => 'required',
			'postcode' => 'required',
			'password' => 'required',						
			'confirmation' => 'required',
        );

        $messages = [           
			'membershipType.required' => 'Please choose your membership',
			'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'phoneNumber.required' => 'Please enter phone number',
			'email.required' => 'Please enter email',
			'email.unique_store' => 'We already have a member account using the provided email address',
			'address1.required' => 'Please enter address line 1',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',
			'password.required' => 'Please enter password',			
			'confirmation.required' => 'Please agree to terms & conditions',			
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('register')->withErrors($validator)->withInput();
        }

        $member = new Member();              
		$member->type_id = $request->membershipType;
        $member->firstName = $request->firstName;
		$member->lastName = $request->lastName;		
		$member->address1 = $request->address1;
		$member->address2 = $request->address2;
		$member->suburb = $request->suburb;
		$member->state = $request->state;
		$member->postcode = $request->postcode;
		$member->phoneMobile = $request->phoneNumber;		
		$member->email = $request->email;
		$member->dateJoin = date('Y-m-d');
		$member->dateExpire = date('Y-m-d', strtotime('+1 years'));
		$member->password = Hash::make($request->password);        
        if($request->live=='on'){
           $member->status = 'active'; 
        }

        $member->save();		
				
		 // Process Payment
		 // ***************
		 // Membership Type Details
		 $memberType = MemberType::where('id', '=', $member->type_id)->first();	
		
		 // eWAY API
		 $apiKey = $_ENV['EWAY_API_KEY'];
         $apiPassword = $_ENV['EWAY_API_PASSWORD'];
         $apiEndpoint = $_ENV['EWAY_API_MODE'];
		
         $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);
		
		 // Order Items
		 $items = array();		 				
		 //$items[$arrSize]['SKU'] = 1;
		 $items[0]['Description'] = "Membership to " . $company_name;
		 $items[0]['Quantity'] = 1;
		 $items[0]['UnitCost'] = $memberType->price * 100;			 

		 $orderTotal = $memberType->price * 100;		
		 	
		 // Payment Transaction
		 $transaction = [
			'Customer' => [
				'Reference' => $member->id,			
				'FirstName' => $member->firstName,
			    'Email' => $member->email,	
				'LastName' => $member->lastName,								
				'Street1' => $member->address1,				
				'City' => $member->suburb,
				'State' => $member->state,
				'PostalCode' => $member->postcode,
				'Country' => "AU",			    
//				'Phone' => '09 889 0986',
				'Mobile' => $member->phoneNumber,							
			],
//			'ShippingAddress' => [
//				'ShippingMethod' => \Eway\Rapid\Enum\ShippingMethod::NEXT_DAY,
//				'FirstName' => "First 1",
//				//'LastName' => $order->billpayer->lastname,
//				'Street1' => "Street 1",			
//				'City' => "City 1",
//				'State' => "VIC",
//				'Country' => "AU",
//				'PostalCode' => "1111",
//				//'Phone' => '09 889 0986',
//			],
			'Items' => $items,
			'Payment' => [
				'TotalAmount' => $orderTotal,
				'InvoiceNumber' => $member->id,
				'InvoiceDescription' => "Membership to " . $company_name,				
				'CurrencyCode' => 'AUD',
//			   'InvoiceReference' => '513456',
			],
			'RedirectUrl' => url('/') . "/register/success/" . $member->id,
			'CancelUrl' =>  url('/') . "/register/success/" . $member->id,			
			'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
			'Capture' => true,
			//'LogoUrl' => url('/') . "/images/site/logo.png",  // MUST START WITH HTTPS
			'HeaderText' => $company_name,
			'Language' => 'EN',
			'CustomerReadOnly' => true
		];
        //dd ($transaction);
		
		$response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::RESPONSIVE_SHARED, $transaction);
		//dd($response);
		
		if (!$response->getErrors()) {
			// Redirect to the Responsive Shared Page
			header('Location: '.$response->SharedPaymentUrl);
			die();
		} else {
			$errorMessage = "<span class='alert-error'>";
			foreach ($response->getErrors() as $error) {
				$errorMessage .= "<i class='fas fa-exclamation'></i> Error: ".\Eway\Rapid::getMessage($error)."<br>";
			}
			$errorMessage .= "<i class='fas fa-exclamation'></i> eWAY payment error.  Please <a href='" . url('/') . "/contact'>contact</a> website owner.";
			$errorMessage .= "</span>";
			
			flash($errorMessage);
			
			return \Redirect::to('register/success/' . $member->id);
		}	
		
  	    return \Redirect::to('register/success/' . $member->id);
	}

	public function success($member_id){
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		
		// Member Details
		$member = Member::where('id', '=', $member_id)->first();
		
		// Process Payment Return
		// **********************
		$apiKey = $_ENV['EWAY_API_KEY'];
        $apiPassword = $_ENV['EWAY_API_PASSWORD'];
        $apiEndpoint = $_ENV['EWAY_API_MODE'];		
		
        $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);    
		
		$ewayAccessCode = $_GET['AccessCode'];  
		
		$response = $client->queryTransaction($ewayAccessCode);
		//dd($response);

		// Update Member Payment Details
		$member = Member::where('id','=',$member_id)->first();              
	    $member->payment_type = "eWAY";
		$member->payment_status = "completed";
		$member->payment_transaction_number = $response->Transactions[0]->TransactionID;
		$member->payment_transaction_result = ($response->Transactions[0]->ResponseCode == "00" ? "Successful" : "Error");
        $member->save();
		
		if ($response->Transactions[0]->ResponseCode == "00")  {
		   // Successful Payment	
		   $message  = "<p>Thanks " . $member->firstName . " for your membership.  Your member number is: " . $member_id . ".<br>We will send you an email confirmation of your new membership.</p>";
		   $message .= "<div><a href='" . url('') . "/members-portal'>View Members Portal</a></div>";
			
           flash($message);
			
		   // Activate member & auto login
		   $this->activate($member_id);		   
		   Auth::guard('member')->login($member);
			
		} else  {
		   // Unsuccessful Payment	
		   $message = "Thanks " . $member->firstName . " for your membership.  Your payment was not successful.  Your unpaid member number is: " . $member_id . ".<br>We will send you an email confirmation of your unpaid member.";	
		   flash("<span class='alert-error'><i class='fas fa-exclamation'></i> " . $message . "</span>");	
		}
				
		$this->sendWelcomeEmail($member->id);
		
		return view('site/members/success', array(
			'company_name' => $company_name,           
            'member' => $member            
        ));
	}
	
	private function sendWelcomeEmail($member_id)  {	
		// Contact Email
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Member Details
		$member = Member::where('id', '=', $member_id)->first();				
			
		// Email Website Owner
        Mail::to($contactEmail)->send(new MembersMessageAdmin($member_id));
		
		// Email User
        Mail::to($member->email)->send(new MembersMessageUser($member_id));
		
	}
	
	private function activate($member_id)  {	
		$member = Member::where('id','=',$member_id)->first();              
	    $member->status = "active";		
        $member->save();
	}
}
