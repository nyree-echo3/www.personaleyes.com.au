<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;
use App\Module;
use App\Service;
use Illuminate\Support\Facades\Cookie;

class TeamController extends Controller
{
    public function index($slug = "")
    {
        $module = Module::where('name', '=', "Team")->first();

        $member = TeamMember::where('slug', '=', $slug)->with('category')->first();
        if (isset($member)) {
            return ($this->member($member->category->slug, $member->slug));
        }
        $service_id = "";
        if (Cookie::get('services_slug')!= "") {
            $service = Service::where('slug', '=', Cookie::get('services_slug'))->first();
            //session()->put('services_id', $service->id);
            //session()->put('services_slug', $service->slug);
            $service_id = $service->id;
            $items = TeamMember::where('service_id', 'like', '%"' . $service->id . '"%')->where('status', '=', 'active')->with("category")->orderBy('position', 'desc')->get();
            $category_name = 'Why Us?';
        } else {
            //session()->forget('services_id');
            //session()->forget('services_slug');
            $items = TeamMember::where('status', '=', 'active')->with("category")->orderBy('position', 'desc')->get();
            $category_name = 'About';
        }

        $meta_title_inner = "Our Doctors and Ophthalmologists";
        $meta_keywords_inner = "Our Doctors and Ophthalmologists";
        $meta_description_inner = "Our Doctors and Ophthalmologists";

        return view('site/team/list', array(
            'module' => $module,
            'items' => $items,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner,
            'category_name' => $category_name,
        ));

    }

    public function member($category, $team_member)
    {
        if ($team_member != "") {
            $services = Service::where('slug', '=', $team_member)->where('status', '=', 'active')->orderBy('title', 'asc')->first();

            if (isset($services)) {
                return ($this->index($category, $services->id));
            }
        }

        $module = Module::where('slug', '=', "team")->first();
        $categories = TeamCategory::where('status', '=', 'active')->orderBy('position', 'desc')->get();
        $category = TeamCategory::where('slug', '=', $category)->first();
        $member = TeamMember::where('slug', '=', $team_member)->first();

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation($member->category->url);

        $side_nav_mode = 'manual';
        if ($side_navV2 == null) {
            $side_navV2 = $categories;
            $side_nav_mode = 'auto';
        }

        return view('site/team/item', array(
            'module' => $module,
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
            'categories' => $categories,
            'category' => $category,
            'team_member' => $member,
            'meta_title_inner' => $member->name . '- Team',
            'meta_keywords_inner' => $member->name,
            'meta_description_inner' => $member->name,
            'category_name' => "Why Us?",
        ));
    }
}
