<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\Helpers\NavigationBuilder;
use App\Page;
use App\PageCategory;
use Illuminate\Support\Facades\Cookie;
use App\ImagesHomeSlider;
use App\Service;
use App\Http\Controllers\Site\SuitabilityQuizController;
use App\Http\Controllers\Site\PagesController;
use App\Popup;

class LandingController extends Controller
{   	
	public function videoDoctors(){   
		$service = Service::where('slug', '=', "lasik")->first();
		
		$builder = new NavigationBuilder();
		Cookie::queue('services_slug', $service->slug, 45000);
		$navigation = $builder->build($service->id, $service->slug);
		$footer =  $builder->footerRaw($service->id, $service->slug);
		$page = 'lasik';
		$home_style_override = false; 
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
    	return view('site/landing/video-doctors', array(         
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'home_doctors_text' => $home_doctors_text,		
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg"
           ));	

    }
	
	public function suitabilityQuiz(){   		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;   
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/suitability-quiz/index', array(         			
			'company_name' => $company_name,	
			'home_doctors_text' => $home_doctors_text,					
			'suitability_quiz' => $suitability_quiz
           ));	

    }
	
	public function suitabilityQuizNotSuitable(Request $request){  		
		$suitabilityQuiz = new SuitabilityQuizController();
        $suitabilityQuiz->notSuitable($request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-not-suitable')->first();
        $suitability_quiz_not_suitable = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-ocular-herpes')->first();
        $ocular_herpes = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-retinal-disease')->first();
        $retinal_disease = $settings->value;    
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/suitability-quiz/not-suitable', array(         			
			'request' => $request,
			'suitability_quiz_not_suitable' => $suitability_quiz_not_suitable,
			'ocular_herpes' => $ocular_herpes,
			'retinal_disease' => $retinal_disease,
			'company_name' => $company_name,
           ));	

    }
	
	public function suitabilityQuizMaybeSuitable(Request $request){   		
		$suitabilityQuiz = new SuitabilityQuizController();
        $suitabilityQuiz->maybeSuitable($request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-maybe-suitable')->first();
        $suitability_quiz_maybe_suitable = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-keratoconus')->first();
        $keratoconus = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-cataracts')->first();
        $cataracts = $settings->value;   
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/suitability-quiz/maybe-suitable', array(         
			'request' => $request,
			'suitability_quiz_maybe_suitable' => $suitability_quiz_maybe_suitable,
			'keratoconus' => $keratoconus,
			'cataracts' => $cataracts,
			'company_name' => $company_name,
           ));	
    }
	
	public function suitabilityQuizSuitable(Request $request){   
		$suitabilityQuiz = new SuitabilityQuizController();
        $suitabilityQuiz->suitable($request);
		
		$settings = Setting::where('key', '=', 'suitability-quiz-suitable')->first();
        $suitability_quiz_suitable = $settings->value;  
		
		$settings = Setting::where('key', '=', 'suitability-quiz-keratoconus')->first();
        $keratoconus = $settings->value;    
		
		$settings = Setting::where('key', '=', 'suitability-quiz-cataracts')->first();
        $cataracts = $settings->value; 
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/suitability-quiz/suitable', array(         
			'request' => $request,
			'suitability_quiz_suitable' => $suitability_quiz_suitable,
			'keratoconus' => $keratoconus,
			'cataracts' => $cataracts,
			'company_name' => $company_name,
           ));	
    }
	
	public function lasikGuide(){   
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
		$pages_controller = new PagesController();       
		
		$category = $pages_controller->getCategory('lasik-guide');
        $allPages = $pages_controller->getPages($category[0]->id);
		
		$popups = Popup::orderBy('created_at', 'desc')->where('status', '=', 'active')->get();
		
    	return view('site/landing/lasik-guide', array( 
			'company_name' => $company_name,
			'category' => $category,
            'allPages' => $allPages,
            'category_name' => $category[0]->name,
            'popups' => $popups,
           ));	

    }
	
	public function video($slide = ""){   
		$service = Service::where('slug', '=', "lasik")->first();
		
		$builder = new NavigationBuilder();
		Cookie::queue('services_slug', $service->slug, 45000);
		$navigation = $builder->build($service->id, $service->slug);
		$footer =  $builder->footerRaw($service->id, $service->slug);
		$page = 'lasik';
		$home_style_override = false; 
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
    	return view('site/landing/video', array(         
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'home_doctors_text' => $home_doctors_text,		
			'category_name' => "",
			'header_image_ctrl' => "images/site/booking-header.jpg",
			'slide' => $slide
           ));	

    }
	
	public function springCampaign2020(){   
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;   
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/spring-campaign-2020', array(         			
			'company_name' => $company_name,	
			'home_doctors_text' => $home_doctors_text,					
			'suitability_quiz' => $suitability_quiz
           ));	

    }	
	
	public function summerCampaign2020(){   
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;   
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/summer-campaign-2020', array(         			
			'company_name' => $company_name,	
			'home_doctors_text' => $home_doctors_text,					
			'suitability_quiz' => $suitability_quiz
           ));	

    }

    public function deliveroo(Request $request){
        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;

        $settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;

        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $show_booking_popup = "false";

        if($request->return=="true"){
            $show_booking_popup = "true";
        }

        return view('site/landing/dinner-is-on-us-deliveroo', array(
            'company_name' => $company_name,
            'home_doctors_text' => $home_doctors_text,
            'suitability_quiz' => $suitability_quiz,
            'show_booking_popup' => $show_booking_popup
        ));

    }

    public function parramatta(Request $request){
        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;

        $settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;

        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $show_booking_popup = "false";

        if($request->return=="true"){
            $show_booking_popup = "true";
        }

        return view('site/landing/parramatta', array(
            'company_name' => $company_name,
            'home_doctors_text' => $home_doctors_text,
            'suitability_quiz' => $suitability_quiz,
            'show_booking_popup' => $show_booking_popup
        ));

    }

    public function morisset(Request $request){
        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;

        $settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;

        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;

        $show_booking_popup = "false";

        if($request->return=="true"){
            $show_booking_popup = "true";
        }

        return view('site/landing/morisset', array(
            'company_name' => $company_name,
            'home_doctors_text' => $home_doctors_text,
            'suitability_quiz' => $suitability_quiz,
            'show_booking_popup' => $show_booking_popup
        ));

    }
	
	public function cataractsBooking(){   
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
		$settings = Setting::where('key', '=', 'home-doctors-text')->first();
        $home_doctors_text = $settings->value;
		
		$settings = Setting::where('key', '=', 'suitability-quiz')->first();
        $suitability_quiz = $settings->value;   
		
		$settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;
		
    	return view('site/landing/cataracts-booking', array(         			
			'company_name' => $company_name,	
			'home_doctors_text' => $home_doctors_text,					
			'suitability_quiz' => $suitability_quiz
           ));	

    }		
}
