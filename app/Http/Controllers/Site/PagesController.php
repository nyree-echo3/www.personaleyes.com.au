<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\SpecialUrl;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;
use App\Popup;
use App\Service;
use Illuminate\Support\Facades\Cookie;

class PagesController extends Controller
{
    public function category($category_slug, $mode = "")
    {
        $category = $this->getCategory($category_slug)->first();

        $side_nav = null;
        $sidebar_mode = '';
        $side_nav_parent_text = '';

        if ($category->slug == "healthcare-professionals") {
            $side_nav = $this->getPages($category->id)->toArray();
            $sidebar_mode = 'array';
            $main_parent_text = 'Health Care Professionals';

        } else {

            $level = (new NavigationHelper())->navigationItems('get-level', 'category', 'pages', $category->slug);
            if ($level != 1) {
                $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
                $side_nav = (new NavigationBuilder())->buildSideNavigation($special_url->url);
                $side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $special_url->url);
                $sidebar_mode = 'prepared';
            }

            if ($level == 1) {
                $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
                $side_nav = (new NavigationBuilder())->buildSideNavigation($special_url->url);
                $side_nav_parent_text = $category->name;
                $sidebar_mode = 'main';
            }

            $main_parent_text = (new NavigationHelper())->navigationItems('main-parent-text', 'category', 'pages', $category->slug);

        }

        //$popup = Popup::where('id', '=', $page->popup_type)->first();

        $cookie_services_slug = Cookie::get('services_slug');
        if (isset($cookie_services_slug) && $cookie_services_slug != "") {
            $service = Service::where('slug', '=', $cookie_services_slug)->first();
            Cookie::queue('services_slug', $service->slug, 45000);
            $home_style_override = false;
        } else if ($category->service_id != "" && $category->service_id != -1) {
            $service = Service::where('id', '=', $category->service_id)->first();
            Cookie::queue('services_slug', $service->slug, 45000);
            $home_style_override = false;
        } else {
            Cookie::queue('services_slug', '', 45000);
            $home_style_override = true;
        }

        $builder = new NavigationBuilder();
        if (isset($service)) {
            $navigation = $builder->build($service->id, $service->slug);
            $footer = $builder->footerRaw($service->id, $service->slug);
        } else {
            $navigation = $builder->build();
            $footer = $builder->footerRaw();
        }

        if ($category->slug == "eye-conditions") {
            $builder = new NavigationBuilder();
            $side_nav = $builder->buildSideNavigation('vision', true);

            return view('site/pages/category-menu', array(
                'page_type' => "Pages",
                'side_nav' => $side_nav,
                'side_nav_parent_text' => $side_nav_parent_text,
                'sidebar_mode' => $sidebar_mode,
                'category' => $category,
                //'page' => $page,
                'mode' => $mode,
                'category_name' => $main_parent_text,
                'header_image_ctrl' => $category->header,
                //'popup' => $popup,
                'navigation_override' => $navigation,
                'footer_override' => $footer,
                'home_style_override' => $home_style_override
            ));
        } else if ($sidebar_mode == 'main') {

            return view('site/pages/category-menu', array(
                'page_type' => "Pages",
                'side_nav' => $side_nav,
                'side_nav_parent_text' => $side_nav_parent_text,
                'sidebar_mode' => $sidebar_mode,
                'category' => $category,
                //'page' => $page,
                'mode' => $mode,
                'category_name' => $main_parent_text,
                'header_image_ctrl' => $category->header,
                //'popup' => $popup,
                'navigation_override' => $navigation,
                'footer_override' => $footer,
                'home_style_override' => $home_style_override
            ));

        } else {

            return view('site/pages/category', array(
                'page_type' => "Pages",
                'side_nav' => $side_nav,
                'side_nav_parent_text' => $side_nav_parent_text,
                'sidebar_mode' => $sidebar_mode,
                'category' => $category,
                //'page' => $page,
                'mode' => $mode,
                'category_name' => $main_parent_text,
                'header_image_ctrl' => $category->header,
                //'popup' => $popup,
                'navigation_override' => $navigation,
                'footer_override' => $footer,
                'home_style_override' => $home_style_override
            ));
        }
    }

    public function getCategory($category_slug)
    {
        $categories = PageCategory::where('slug', '=', $category_slug)->orderBy('position', 'asc')->get();
        return ($categories);
    }

    public function getPages($category_id)
    {
        $pages = Page::where('status', '=', 'active')->where('category_id', '=', $category_id)->whereNull('parent_page_id')->orderBy('position', 'asc')->get();

        foreach ($pages as $page):
            $page['nav_sub'] = $this->getSubPages($page->id);
            $page['url'] = $page->url;
        endforeach;

        return ($pages);
    }

    public function getSubPages($page_id)
    {
        $pages = Page::where('status', '=', 'active')->where('parent_page_id', '=', $page_id)->orderBy('position', 'asc')->get();
        return ($pages);
    }

    public function index($category_slug, $page_slug = "", $mode = "")
    {

        $category = $this->getCategory($category_slug);

        $page = ($page_slug == "" ? $this->getPages($category[0]->id)->first() : $this->getPage($category[0]->id, $page_slug, $mode));

        $side_nav = null;
        $sidebar_mode = null;
        $side_nav_parent_text = null;
        $main_parent_text = '';

        if ($category[0]->slug == "healthcare-professionals") {
            $side_nav = $this->getPages($category[0]->id)->toArray();
            $main_parent_text = 'Health Care Professionals';
        } else {

            //get level
            $level = (new NavigationHelper())->navigationItems('get-level', 'page', 'pages', $page->slug);

            if ($level == 3) {

                $parent_href = (new NavigationHelper())->navigationItems('parent-href', 'page', 'pages', $page->slug);

                //$special_url = SpecialUrl::where('item_id', '=', $page->category->id)->where('module', '=', 'pages')->where('type', '=', 'category')->first();
                $side_nav = (new NavigationBuilder())->buildSideNavigation($parent_href);
                $side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $parent_href);
                $sidebar_mode = 'prepared';

            } else if ($level != 1) {
                //$special_url = SpecialUrl::where('item_id', '=', $page->id)->where('module', '=', 'pages')->where('type', '=', 'item')->first();
                $side_nav = (new NavigationBuilder())->buildSideNavigation($page->url);
                $side_nav_parent_text = (new NavigationHelper())->navigationItems('parent-text', null, null, null, $page->url);
                $sidebar_mode = 'prepared';
            }

            $main_parent_text = (new NavigationHelper())->navigationItems('main-parent-text', 'page', 'pages', $page->slug);

            if (!is_string($main_parent_text)) {
                $main_parent_text = $page->title;
            }
        }

        $popup = Popup::where('id', '=', $page->popup_type)->first();


        if (!request()->raw_request_path == '/vision/online-eye-test') {

            if ($page->service_id != "") {
                $service = Service::where('id', '=', $page->service_id)->first();
                Cookie::queue('services_slug', $service->slug, 45000);
                $home_style_override = false;
            } else {
                Cookie::queue('services_slug', '', 45000);
                $home_style_override = true;
            }

            $builder = new NavigationBuilder();
            if (isset($service)) {
                $navigation = $builder->build($service->id, $service->slug);
                $footer = $builder->footerRaw($service->id, $service->slug);
            } else {
                $navigation = $builder->build();
                $footer = $builder->footerRaw();
            }

        } else {

            $home_style_override = true;
            $builder = new NavigationBuilder();

            if(Cookie::get('services_slug')=='lasik'){
                $home_style_override = false;

                $service = Service::where('slug', '=', 'lasik')->first();

                $navigation = $builder->build($service->id, $service->slug);
                $footer = $builder->footerRaw($service->id, $service->slug);

            }else{
                $navigation = $builder->build();
                $footer = $builder->footerRaw();
            }

        }

        return view('site/pages/pages', array(
            'page_type' => "Pages",
            'side_nav' => $side_nav,
            'side_nav_parent_text' => $side_nav_parent_text,
            'sidebar_mode' => $sidebar_mode,
            'category' => $category,
            'page' => $page,
            'mode' => $mode,
            'category_name' => $main_parent_text,
            'popup' => $popup,
            'navigation_override' => $navigation,
            'footer_override' => $footer,
            'home_style_override' => $home_style_override,
        ));
    }

    public function getPage($category_id, $page_slug, $mode)
    {
        if ($mode == "preview") {
            $pages = Page::where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();
        } else {
            $pages = Page::where('status', '=', 'active')->where('slug', '=', $page_slug)->where('category_id', '=', $category_id)->orderBy('position', 'asc')->first();
        }

        return ($pages);
    }

    public function lasikGuide()
    {
        $service = Service::where('slug', '=', "lasik")->first();

        Cookie::queue('services_slug', $service->slug, 45000);

        $builder = new NavigationBuilder();
        if (isset($service)) {
            $navigation = $builder->build($service->id, $service->slug);
            $footer = $builder->footerRaw($service->id, $service->slug);
        } else {
            $navigation = $builder->build();
            $footer = $builder->footerRaw();
        }

        $category = $this->getCategory('lasik-guide');
        $allPages = $this->getPages($category[0]->id);

        $side_nav = (new NavigationBuilder())->buildSideNavigation();

        $popups = Popup::orderBy('created_at', 'desc')->where('status', '=', 'active')->get();

        return view('site/pages/lasik-guide', array(
            'page_type' => "Pages",
            'side_nav' => $side_nav,
            'category' => $category,
            'allPages' => $allPages,
            'category_name' => $category[0]->name,
            'popups' => $popups,
            'navigation_override' => $navigation,
            'footer_override' => $footer,
            'home_style_override' => false,
        ));
    }
}
