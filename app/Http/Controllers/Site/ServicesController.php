<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Page;
use App\PageCategory;
use Illuminate\Http\Request;
use App\Setting;
use App\Service;
use App\Helpers\NavigationBuilder;
use Illuminate\Support\Facades\Cookie;
use App\ImagesHomeSlider;

class ServicesController extends Controller
{
    public function index(){		
        $services = Service::where('status', '=', 'active')->get();						
		
		$category_name = "Treatments";
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation(null, true);		
		
		return view('site/services/index', array(			
            'services' => $services,						
			'category_name' => $category_name,
			'side_nav' => $side_nav,
        ));
    }	
	
	public function service($page_slug = ""){	
        $service = Service::where('slug', '=', $page_slug)->first();

        $side_nav = '';
        $category = PageCategory::where('slug', '=', $page_slug)->first();
        if($category){
            $side_nav = (new NavigationBuilder())->buildSideNavigation($page_slug);
        }

        $builder = new NavigationBuilder();

        if($page_slug=='lasik'){
            Cookie::queue('services_slug', $service->slug, 45000);
            $navigation = $builder->build($service->id, $service->slug);
            $footer =  $builder->footerRaw($service->id, $service->slug);
            $page = 'lasik';
            $home_style_override = false;
        }else{
            Cookie::queue('services_slug', '', 45000);
            $navigation = $builder->build();
            $footer =  $builder->footerRaw();
            $page = 'service';
            $home_style_override = true;
        }
		
		$images = ImagesHomeSlider::where('status', '=', 'active')->where('service_id', 'like', '%"' . $service->id . '"%')->orderBy('position', 'desc')->get();        

		return view('site/services/'.$page , array(
			'navigation' => null,
            'service' => $service,	
			'services_id' => $service->id,
			'services_slug' => $service->slug,
			'navigation_override' => $navigation,
            'footer_override' => $footer,
			'home_style_override' => $home_style_override,
			'images_override' => $images,
            'side_nav' => $side_nav
        ));
    }	
}
