<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use App\Page;
use App\PageCategory;
use Illuminate\Http\Request;

use App\Location;
use App\Module;
use App\Popup;
use App\Service;
use Illuminate\Support\Facades\Cookie;

class LocationsController extends Controller
{
    public function index($mode = ""){		
		$module = Module::where('slug', '=', "locations")->first();
		
		$service_id = "";
		if (Cookie::get('services_slug') != "")  {
			$service = Service::where('slug', '=', Cookie::get('services_slug'))->first();	
			$service_id = $service->id;
	    }
		
		if ($service_id == "") {
		   $items = Location::where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();
		} else  {
		   $items = Location::where('service_id', 'like', '%"' . $service_id . '"%')->where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();	
		}
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
		
		return view('site/locations/list', array(           
			'module' => $module,
			'items' => $items,
			'mode' => $mode,
			'side_nav' => $side_nav,
			'category_name' => $module->display_name,
        ));

    }
	
	public function item ($item_slug = "", $mode = "")
    {   
		if (Cookie::get('services_slug') != "")  {
			$service = Service::where('slug', '=', Cookie::get('services_slug'))->first();	
			$service_id = $service->id;
	    }
		
        $module = Module::where('slug', '=', "locations")->first();
		
		$items = Location::where('status', '=', 'active')->orderBy('position', 'asc')->get();
		$location = Location::where('status', '=', 'active')->where('slug', '=', $item_slug)->orderBy('position', 'asc')->first();
		
		$side_nav = (new NavigationBuilder())->buildSideNavigation();
		
		$popup = Popup::where('id', '=', $location->popup_type)->first();

		$services = $this->getAvailableServices($location);

		return view('site/locations/item', array(
            'module' => $module,
			'items' => $items,
			'location' => $location,
			'mode' => $mode,
			'side_nav' => $side_nav,
			'category_name' => $module->display_name,
			'popup' => $popup,
			'services' => $services
        ));
    }
	
	public function getLocations(){
		$locations = Location::where('status', '=', 'active')->orderBy('state', 'desc')->orderBy('name', 'asc')->get();				
		return($locations);
	}

    public function getAvailableServices($location)
    {

        $services_category = PageCategory::where('slug','=','treatments')->first();
        $services_array = array();

        $counter = 0;
        foreach($location->services_array as $service){

            $title = "";
            $desc = "";
            $url = "";
            $icon = "";

            $service_page = Page::where('category_id','=',$services_category->id)->where('title','=',$service)->first();

            if($service_page){

                $title = $service_page->title;
                $desc = $service_page->short_description;
                $url = $service_page->url;
                $icon = $service_page->icon;

                if($service_page->title == 'Paediatric Ophthalmology'){
                    $icon = 'web/public/media/Locations/icons/PaediatricOphthalmologists.png';
                }

                if($service_page->title == 'Myopia Management'){
                    $icon = 'web/public/media/Locations/icons/ShortSightednessTreatment.png';
                }

                if($service_page->title == 'Keratoconus Treatment'){
                    $icon = 'web/public/media/Locations/icons/KeratoconusTreatment.png';
                }

                if($service_page->title == 'Macular Degeneration Treatment'){
                    $icon = 'web/public/media/Locations/icons/MacularDegenerationTreatment.png';
                }

                if($service_page->title == 'Oculoplasty'){
                    $icon = 'web/public/media/Locations/icons/Oculoplasty.png';
                }

            }else{

                switch ($service) {
                    case "Eye Tests":

                        $title = 'Eye Tests';
                        $desc = 'You can take a comprehensive online eye test or visit us for more information. Our experts can test your vision and provide treatment if necessary';
                        $url = 'vision/online-eye-test';
                        $icon = 'web/public/media/Locations/icons/EyeTests.png';
                        break;

                    case "LASIK Eye Surgery":

                        $title = 'LASIK Eye Surgery';
                        $desc = 'A life-changing procedure that can correct common vision conditions like as myopia, astigmatism and hyperopia quickly and painlessly.';
                        $url = 'lasik';
                        $icon = 'web/public/media/Locations/icons/LASIKEyeSurgery.png';
                        break;

                    case "Cataract Eye Surgery":

                        $title = 'Cataract Eye Surgery';
                        $desc = 'The most common forms of vision loss worldwide. Our surgeons perform cataract eye surgery to remove the cloudy film that hinders your vision and replace it with a new intraocular lens.';
                        $url = 'cataracts';
                        $icon = 'web/public/media/Locations/icons/CataractEyeSurgery.png';
                        break;

                    case "Diabetic Retinopathy Treatment":

                        $title = 'Diabetic Retinopathy Treatment';
                        $desc = 'Individuals with diabetes are at high risk for developing retinopathy, which can permanently damage the retina. If you are at risk, take action early. Diabetic Retinopathy treatment can slow damage and prevent further vision loss.';
                        $url = 'vision/diabetic-retinopathy';
                        $icon = 'web/public/media/Locations/icons/DiabeticRetinopathyTreatment.png';
                        break;
                }

            }

            $services_array[$counter]['title'] = $title;
            $services_array[$counter]['desc'] = $desc;
            $services_array[$counter]['url'] = $url;
            $services_array[$counter]['icon'] = $icon;

            $counter++;
        }

        return $services_array;
	}
				
}
