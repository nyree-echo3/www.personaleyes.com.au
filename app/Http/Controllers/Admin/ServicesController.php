<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\NavigationHelper;
use App\Http\Controllers\Controller;
use App\Module;
use App\Service;
use App\Helpers\General;
use App\SpecialUrl;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'services')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {      
        $paginate_count = session()->get('pagination-count');
        
        $services = Service::sortable()->orderBy('position', 'desc')->paginate($paginate_count);
       
        
        return view('admin/services/services', array(
            'services' => $services,                     
        ));
    }

    public function add()
    {       
        return view('admin/services/add', array(           
        ));
    }

    public function edit($service_id)
    {
        $service = Service::where('id', '=', $service_id)->first();
        
        return view('admin/services/edit', array(
            'service' => $service,         
        ));
    }
	
    public function store(Request $request)
    {
        $rules = array(           
            'title' => 'required',
			'slug' => 'required|unique_store:services',
            'description' => 'required'
        );

        $messages = [            
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/services/add')->withErrors($validator)->withInput();
        }

        $service = new Service();       
        $service->title = $request->title;
		$service->slug = $request->slug;
        $service->description = $request->description;
		$service->header_image = $request->header_image;

		if($request->sub_site=='on'){
           $service->sub_site = 'true'; 
        } 
		
        if($request->live=='on'){
           $service->status = 'active'; 
        }

        $service->save();
   
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/services/' . $service->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/services/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(           
            'title' => 'required',
			'slug' => 'required|unique_update:services,' . $request->id,
            'description' => 'required'
        );

        $messages = [            
            'title.required' => 'Please enter title',
			'slug.required' => 'Please enter unique SEO Name',
            'slug.unique_store' => 'The SEO Name is already taken',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/services/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $service = Service::where('id','=',$request->id)->first();

        (new NavigationHelper())->navigationItems('update', 'service', 'services', $service->slug, null, $request->title, $request->slug);

        $service->title = $request->title;
		$service->slug = $request->slug;
        $service->description = $request->description;
		$service->header_image = $request->header_image;
		
		if($request->sub_site=='on'){
           $service->sub_site = 'true'; 
		} else {
			$service->sub_site = 'false';
        }
		
		if($request->live=='on'){
           $service->status = 'active'; 
		} else {
			$service->status = 'passive';
        }
        $service->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/services/' . $service->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/services/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
    }

    public function delete($service_id)
    {
        $service = Service::where('id','=',$service_id)->first();
        $service->is_deleted = true;
        $service->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeServiceStatus(Request $request, $service_id)
    {
        $service = Service::where('id', '=', $service_id)->first();
        if ($request->status == "true") {
            $service->status = 'active';
        } else if ($request->status == "false") {
            $service->status = 'passive';
        }
        $service->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $services = Service::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/services/sort', array(
            'services' => $services
        ));
    }

}