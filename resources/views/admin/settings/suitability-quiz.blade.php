@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Home Page</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home Page</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Home Page Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/suitability-quiz-update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">                                
                                <div class="form-group {{ ($errors->has('suitability_quiz')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suitability Quiz</label>

                                    <div class="col-sm-10">
                                        <textarea id="home_intro_text_main" name="suitability_quiz" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz->value }}</textarea>
                                        @if ($errors->has('suitability_quiz'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_suitable')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suitability Quiz<br>Suitable</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_suitable" name="suitability_quiz_suitable" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_suitable->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_suitable'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_suitable') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_not_suitable')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suitability Quiz<br>Not Suitable</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_not_suitable" name="suitability_quiz_not_suitable" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_not_suitable->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_not_suitable'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_not_suitable') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_maybe_suitable')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Suitability Quiz<br>Maybe Suitable</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_maybe_suitable" name="suitability_quiz_maybe_suitable" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_maybe_suitable->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_maybe_suitable'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_maybe_suitable') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_keratoconus')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Keratoconus</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_keratoconus" name="suitability_quiz_keratoconus" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_keratoconus->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_keratoconus'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_keratoconus') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_cataracts')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Cataracts</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_cataracts" name="suitability_quiz_cataracts" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_cataracts->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_cataracts'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_cataracts') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_ocular_herpes')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Ocular Herpes</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_ocular_herpes" name="suitability_quiz_ocular_herpes" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_ocular_herpes->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_ocular_herpes'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_ocular_herpes') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('suitability_quiz_retinal_disease')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Retinal Disease</label>

                                    <div class="col-sm-10">
                                        <textarea id="suitability_quiz_retinal_disease" name="suitability_quiz_retinal_disease" rows="10" cols="80"
                                                  style="height: 500px;">{{ $suitability_quiz_retinal_disease->value }}</textarea>
                                        @if ($errors->has('suitability_quiz_retinal_disease'))
                                            <small class="help-block">{{ $errors->first('suitability_quiz_retinal_disease') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('suitability_quiz');
			CKEDITOR.replace('suitability_quiz_suitable');
			CKEDITOR.replace('suitability_quiz_not_suitable');			
			CKEDITOR.replace('suitability_quiz_maybe_suitable');
			
			CKEDITOR.replace('suitability_quiz_keratoconus');
			CKEDITOR.replace('suitability_quiz_cataracts');
			CKEDITOR.replace('suitability_quiz_ocular_herpes');
			CKEDITOR.replace('suitability_quiz_retinal_disease');
        });
    </script>
@endsection