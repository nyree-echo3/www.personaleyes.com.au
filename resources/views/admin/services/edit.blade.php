@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/services') }}"><i class="fas fa-th"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/services/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $service->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                                               value="{{ old('title',$service->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$service->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="description"
                                                  placeholder="Description">{{ old('description', $service->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            @php
								if(count($errors)>0){
								   if(old('header_image')!=''){
									$header_image = old('header_image');
								   }else{
									$header_image = '';
								   }
								}else{
									$header_image = $service->header_image;
								}
							@endphp
							<div class="form-group {{ ($errors->has('header_image')) ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Icon Image</label>
								<div class="col-sm-10">
									<input type="hidden" id="header_image" name="header_image"
										   value="{{ old('header_image',$header_image) }}">
									<button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
										Image
									</button>
									@php
										$class = ' invisible';
										if($header_image!=''){
											$class = '';
										}
									@endphp
									<button id="remove-image" type="button"
											class="btn btn-danger btn-sm{{ $class }}">Remove Image
									</button>
									<br/><br/>
									<span id="added_image" class="added_icon">
									@if($header_image!='')
											<image src="{{ old('header_image',$header_image) }}"/>
										@endif
									</span>
									@if ($errors->has('header_image'))
										<small class="help-block">{{ $errors->first('header_image') }}</small>
									@endif
								</div>                                
						   </div>

						
                            
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $service->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>
                            
                            @php
                                if(count($errors)>0){
                                   if(old('sub_site')=='on'){
                                    $sub_site = 'true';
                                   }else{
                                    $sub_site = '';
                                   }
                                }else{
                                    $sub_site = $service->sub_site;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Mini Site *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="sub_site" {{ $sub_site == 'true' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/services') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
    
    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$service->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>    
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            CKEDITOR.replace('description');

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$('#title').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#header_image').val('');
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#header_image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#header_image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection