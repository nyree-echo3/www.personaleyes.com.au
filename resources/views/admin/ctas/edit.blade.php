@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/ctas') }}"><i class="fas fa-file"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/ctas/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $cta->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" placeholder="Title"
                                               value="{{ old('title',$cta->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>    
                                
                                <div class="form-group{{ ($errors->has('button')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Button Text *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="button" placeholder="Button Text"
                                               value="{{ old('button',$cta->button) }}">
                                        @if ($errors->has('button'))
                                            <small class="help-block">{{ $errors->first('button') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">URL *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="url" placeholder="URL"
                                               value="{{ old('url',$cta->url) }}">
                                        @if ($errors->has('url'))
                                            <small class="help-block">{{ $errors->first('url') }}</small>
                                        @endif
                                    </div>
                                </div>                                                                                                                             

                                <!--<div class="form-group {{ ($errors->has('script')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Script</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="script"
                                                  placeholder="Script">{{ old('script', $cta->script) }}</textarea>
                                        @if ($errors->has('script'))
                                            <small class="help-block">{{ $errors->first('script') }}</small>
                                        @endif
                                    </div>
                                </div>-->
                                
                                @php
                                    if(count($errors)>0){
                                       if(old('image')!=''){
                                        $image_image = old('image');
                                       }else{
                                        $image_image = '';
                                       }
                                    }else{
                                        $image_image = $cta->image;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('image')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="image" name="image"
                                               value="{{ old('image',$image_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($image_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($image_image!='')
                                                <image src="{{ old('image',$image_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('image'))
                                            <small class="help-block">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $cta->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/ctas') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#image').val('');
            });
        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection