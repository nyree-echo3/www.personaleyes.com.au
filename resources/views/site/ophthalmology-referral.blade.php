@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
		    @include('site/partials/sidebar-pages') 
		    
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Ophthalmology Referral</h1>
                    
                    <!-- Hubspot Form -->
                    <!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({
						portalId: "5064462",
						formId: "48b6bcbd-47c1-44d8-b9f6-8f1eeab6b5b4"
					});
					</script>
					<!-- End HubSpot Form -->
					
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
