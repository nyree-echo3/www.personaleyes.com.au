<div class="home-doctors">
    <div class="container">
        <div class="row">
            {!! $home_doctors_text !!}

            <div class="col-lg-6 home-doctors-video home-doctors-divider">
                <a data-toggle="modal" class="dr-bala-video-lasik" href="#modalDrBala">
                    <div class="home-doctors-video">
                        <h3>A/Prof Chandra Bala</h3>
                        <img src="{{ url('') }}/images/site/video-drbala.jpg" alt="Video" title="Video">
                        <div class="home-doctors-video-txt">Managing Director at personalEYES, A/Prof Bala discusses maintaining optimum safety and getting the absolute best results for our patients.</div>
                    </div>
                </a>
            </div>

            <div class="col-lg-6 home-doctors-video">
                <a data-toggle="modal" class="dr-meades-video-lasik" href="#modalDrMeades">
                    <div class="home-doctors-video">
                        <h3>Dr Kerrie Meades </h3>
                        <img src="{{ url('') }}/images/site/video-drmeades.jpg" alt="Video" title="Video">
                        <div class="home-doctors-video-txt">LASIK pioneer and Founding Director of personalEYES, Dr Meades outlines the fundamentals of LASIK.</div>
                    </div>
                </a>
            </div>

        </div>
    </div>
</div>

<!-- The Modal - Dr Bala -->
<div class="modal fade" id="modalDrBala">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9" id="DrBalaIframe-container"></div>
            </div>

        </div>
    </div>
</div>

<!-- The Modal - Dr Meades -->
<div class="modal fade" id="modalDrMeades">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9" id="DrKerrieIframe-container">

                </div>
            </div>

        </div>
    </div>
</div>

@section('inline-scripts-video')
    <script language="javascript">

        var dr_bala_player = new Vimeo.Player(document.getElementById('DrBalaIframe'));

        $('#modalDrBala').on('shown.bs.modal', function () {
            dr_bala_player.play();
        });
        $('#modalDrBala').on('hidden.bs.modal', function () {
            dr_bala_player.pause();
        });

        var dr_kerrie_player = new Vimeo.Player(document.getElementById('DrKerrieIframe'));

        $('#modalDrMeades').on('shown.bs.modal', function () {
            dr_kerrie_player.play();
        })
        $('#modalDrMeades').on('hidden.bs.modal', function () {
            dr_kerrie_player.pause();
        })
    </script>
@endsection