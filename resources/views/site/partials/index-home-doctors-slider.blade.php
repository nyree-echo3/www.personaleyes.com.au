@if(count($specialists)>0)
    <div class="container-fluid our-specialists">
        <div class="container">
            <div class="row justify-content-center no-gutters">
                <div class="col-12 service-header-cont">
                    <h2>Our Specialists</h2>
                </div>
            </div>
            <div class="row justify-content-center no-gutters">

                <div class="arrow-cont">
                    <div class="arrow owl-prev-specialists">
                        <i class="fas fa-angle-left"></i>
                    </div>
                </div>
                <div class="slides-cont">
                    <div class="owl-carousel-specialists owl-carousel owl-theme">
                        @foreach($specialists as $specialist)
                            <div class="slide-cont">
                                <div class="specialist-photo">
                                    <a href="{{ url('') }}/{{ $specialist->url }}">
                                        <img src="{{ url('') }}/{{ $specialist->photo }}" />
                                    </a>
                                </div>
                                <div class="slide-title">{{ $specialist->name }}</div>
                                <div class="slide-desc">{{ $specialist->job_title }}</div>
                                <div class="slide-role">{{ $specialist->role }}</div>
                                <div class="slide-more-button">
                                    <a href="{{ url('') }}/{{ $specialist->url }}">VIEW PROFILE</a>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>

                <div class="arrow-cont">
                    <div class="arrow owl-next-specialists">
                        <i class="fas fa-angle-right"></i>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endif
<div class="container-fluid pe-difference">
    <div class="container">
        <div class="diff-text-cont">
            <div class="diff-text">
                <h2>Our Services</h2>
                <ol class="fa-ul">
                    <li><span class="fa-li"><i class="fas fa-check"></i></span>25+ years specializing in Opthalmology</li>
                    <li><span class="fa-li"><i class="fas fa-check"></i></span>10 state-of-the-art clinics across NSW & ACT</li>
                    <li><span class="fa-li"><i class="fas fa-check"></i></span>Safer & better results with the latest technology in Australia</li>
                    <li><span class="fa-li"><i class="fas fa-check"></i></span>Highly-experienced & industry-awarded specialists & surgeons</li>
                </ol>
                <div class="difference-book-btn-cont">
                    <a href="{{ url('make-a-booking') }}" class="difference-book-btn">BOOK AN APPOINTMENT</a>
                </div>
            </div>
        </div>
    </div>
</div>