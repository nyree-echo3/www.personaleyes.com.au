<link rel="stylesheet" href="{{ asset('css/site/hs-form.css?v=0.1') }}">
<div class="modal fade" id="scheduling-page-clinic" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row booking-header-part no-gutters">
					<div class="col-sm-6">
						<img src="{{ url('') }}/images/site/logo-booking.gif" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
						<h2 class="scheduling-page-header">Free LASIK Assessment Booking</h2>
						<!-- <span class="booking-header-part2">Book your free appointment in under 2 minutes.</span> -->
						<div class="divBookingLocation-txt">Choose your preferred clinic</div>
					</div>
					<div class="col-sm-6 d-none d-lg-block">
						<img class="img-fluid" src="{{ url('') }}/images/site/pop-up-booking-form.gif" title="Secure" alt="Secure">
					</div>
															   
					<div class="col-sm-6 col-clinic">	
					    <div class='divPostcode'>			   					   
						   <input id="txtPostcode" name="txtPostcode" type="textbox" class="txtPostcode" placeholder="Enter postcode" maxlength="4" />	
						</div>
						
						<div class='divBookingLocation-submit'>
				       	  <button id="btnFind" type="button">
							  <div class="iconLoader">
								  <div class="loader"></div>
							  </div>
							  <div class="iconArrow">
								  <i class="fas fa-arrow-right"></i>
							  </div>
				       	  </button>
				       </div>			       					   			       					   
					</div>	  
			        
				    <div class="col-sm-1 col-clinic">		
				       <div class='txtOR'>OR</div>
					</div>
					
					<div class="col-sm-5 col-clinic">					   		    	         		          				   		    	         		        		       			 					  
					    <a href='#' onclick='btnCurrentLocation()' class='divCurrentLocation'>Use my current location</a>				   					  
					</div> 
		        
	                <div class="divBookingLocationResultsNone">
						<div class='col-12'>
						   <a href='#' onclick='btnLocationMore()' class='divLocationMore')'>See all available NSW clinics <i class='fa fa-chevron-right'></i></a>
						</div>
                    </div>
	        
		        </div>	

		             
					
			   <div class="divBookingLocation-results">			       					
					<div class="divBookingLocationResults">
					    <div class="container">
							<div class="row">
								@foreach ($locations as $location) 
									  @php								  
										 $locationName = str_replace(" ACT", "", str_replace(" NSW", "", str_replace("Clinic, ", "", $location["name"])));							  
									  @endphp

									  <div class='col-lg-3 col-sm-12 divClinic'>
										  <a href='#' onclick='btnClinic({{ $location["id"] }})'>
											  <div id='divLocation{{ $location["id"] }}' class='divLocation'>
											  <div id='divLocationName{{ $location["id"] }}' class='divLocationName'>{{ $locationName }}</div>
											  <div id='divLocationAddress{{ $location["id"] }}' class='divLocationAddress divHide'>{{ $location["location"] }}</div>									  
											  </div>
										  </a>
									  </div>						  					      	              
							   @endforeach
						   </div>  
					   </div>  
					</div>                         				   
			   </div>
              
               <div class="row scheduling-page-footer">
                    <div class="col-sm-12">
                        <a href='#' onclick='btnNoTime()' class='divNoTime'>Can't find a time that suits? <i class='fa fa-chevron-right'></i></a>                        
                    </div>
					<div class="col-sm-12">
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
                            hbspt.forms.create({
                                portalId: "5064462",
								formId: "808dff57-6d05-4dcf-8c0f-a7a863797ce3"
                            });
						</script>
					</div>
				</div>
               
               <div class="divBookingLocation-details">					                       				   
			   </div>                              
                
               <div class="divBookingLocation-acuity"></div>               
			   <script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
			   				   
				<div class="row">
					<div class="col-sm-12 bookingFooterAwardsWrapper">
						<a href="https://www.google.com.au/search?source=hp&ei=5gcxXYyHI9fB9QPepo-YDQ&q=personaleyes&oq=personaleyes&gs_l=psy-ab.3..0l8j0i10j0.981.5143..5221...3.0..0.194.1856.0j12......0....1..gws-wiz.....0..0i131.JBQ53A4pF5Y&ved=2ahUKEwjH86q51r_jAhUEOSsKHXzbAykQvS4wAHoECAcQLA&uact=5&npsic=0&rflfq=1&rlha=0&rllag=-33799595,151105731,11802&tbm=lcl&rldimm=12210723549260763504&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:650661978133997922;mv:!1m2!1d-33.098168367811844!2d151.26419858984377!2m2!1d-34.716511351066714!2d149.16580991796877!4m2!1d-33.91118087282168!2d150.21500425390627!5i9" target="_blank">
							<img class="img-fluid" src="{{ url('') }}/images/site/Google-Reviews.gif" title="Google Reviews" alt="Google Reviews">
						</a>
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-celebrating-22years-bladeless-vision-correction.gif" title="Celebrating 22years Bladeless Vision Correction" alt="Celebrating 22years Bladeless Vision Correction">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-lasik.gif" title="Award - Bladeless LASIK" alt="Award - Bladeless LASIK">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-cataract.gif" title="Award - Bladeless Cataract" alt="Award - Bladeless Cataract">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-icl.gif" title="Award - ICL" alt="Award - ICL">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-secure.gif" title="Secure" alt="Secure">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="divAllClinics">
   <div class="container">
		<div class="row">
			@foreach ($locations as $location) 
				  @php								  
					 $locationName = str_replace("Clinic, ", "", $location["name"]);							  
				  @endphp

				  <div class='col-lg-3 col-sm-12 divClinic'>
					  <a href='#' onclick='btnClinic({{ $location["id"] }})'>
						  <div id='divLocation{{ $location["id"] }}' class='divLocation'>
						  <div id='divLocationName{{ $location["id"] }}' class='divLocationName'>{{ $locationName }} <i class='fa fa-chevron-right'></i></div>
						  <div id='divLocationAddress{{ $location["id"] }}' class='divLocationAddress divHide'>{{ $location["location"] }}</div>									  
						  </div>
					  </a>
				  </div>						  					      	              
		   @endforeach
		   
	    </div>
	 </div>
</div>


@section('inline-scripts-scheduling-modal-clinic')
	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$('#scheduling-page-clinic').on('show.bs.modal', function (e) {
		   //$("#txtPostcode").val('');
		})
		
		$('a[href$="#modalBookingLasik"]').on( "click", function() {	   
		   $('#modal-popup').hide();			   
		   $('#modalBookingLasik').modal();		   
		});
				
		
		$("#txtPostcode").keypress(function(event) {			
            return /\d/.test(String.fromCharCode(event.keyCode));
        });
		
		$("#txtPostcode").keyup(function(event) {
			if ($("#txtPostcode").val().length == 4)  {
				btnFind("", "");
			} else {
				$('.iconLoader').hide();
				$('.iconArrow').show();	
				$('.divBookingLocationResults').hide();
				$('.divBookingLocationResultsNone').show();		
				
				html = $('.divAllClinics').html();
				$('.divBookingLocationResults').html(html);						
			}          
        });
		
		$("#btnFind").click(function() {
	        postcode = $('#txtPostcode').val();
	          	
		    if (postcode.length == 4)  {
			   btnFind("", "");		
			}
		});
		
		function btnFind(latitude, longitude)  {				
			$('.iconArrow').hide();
			$('.iconLoader').show();
			
			$.ajax({
				type: "POST",
				url: "{{ url('/booking-location') }}",
				data:  {
					'postcode':$('#txtPostcode').val(),
					'latitude':latitude,
					'longitude':longitude,
				},
				success: function (response) {					
					if(response.status == "success"){
				      $('.divBookingLocationResults').html("");
						
					  locations = response.locations;
						
					  html = "";
					  html += "<div class='container'>";
					  html += "<div class='row'>";
					
					  strClass = "col-lg-4 col-sm-12";
						
					  for (i = 0; i < locations.length; i++) { 								  
						  locationName = locations[i]["name"];
						  locationName = locationName.replace("Clinic, ", "");
						  locationName = locationName.replace(" NSW", "");
						  locationName = locationName.replace(" ACT", "");
						  
						  if (i == 3)  {
							 strClass = "col-lg-3 col-sm-12 divClinic";
							 html += "<div class='col-12'>"; 
							 html += "<a href='#' onclick='btnLocationMore()' class='divLocationMore'>See more available clinics <i class='fa fa-chevron-right'></i></a>"; 
							 html += "</div>"; 
						  }
						  
						  html += "<div class='" + strClass + "'>";
						  html += "<a href='#' onclick='btnClinic(" + locations[i]["id"] + ")'>";
						  html += "<div id='divLocation" + locations[i]["id"] + "' class='divLocation'>";
						  html += "<div id='divLocationName" + locations[i]["id"] + "' class='divLocationName'>" + locationName + "</div>";
						  html += "<div id='divLocationAddress" + locations[i]["id"] + "' class='divLocationAddress divHide'>" + locations[i]["location"] + "</div>";
						  //html += "<div class='divLocationMore'>View Availabilities <i class='fa fa-chevron-right'></i></div>";
		                  html += "</div>";
						  html += "</a>";
						  html += "</div>";						  					      	              
					  }
					
					  html += "</div>";
					  html += "</div>";	
					  		
					  $('.divBookingLocationResults').append(html);
					  $('.divBookingLocationResults').show();						  						  					  					 
					  	  
					  $('.iconLoader').hide();
					  $('.iconArrow').show();	
					  $('.divBookingLocationResultsNone').hide();	
											  						
					  // Show closest clinic calendar
					  btnClinic(locations[0]["id"], false);	
					}
				}
			});
		}
		
		function btnClinic(clinicID, buttonClick = true)  {	
			html  = "<div class='divLocationName'>" + $('#divLocationName' + clinicID).html() + "</div>"; 		
			html += "<div class='divLocationAddress'>" + $('#divLocationAddress' + clinicID).html() + "</div>"; 				
			
			$('.divBookingLocation-details').html(html);	
			$('.divBookingLocation-details').show();
			
			html  = "";		
			html += '<iframe src="https://personaleyes-lasik-booking.as.me/?calendarID=' + clinicID + '" width="100%" height="650" frameBorder="0"></iframe>';			
			$('.divBookingLocation-acuity').html(html);
			//$('.divBookingLocationResults').hide();
			$('.divBookingLocation-acuity').show();
			
			$('#scheduling-page-clinic .hbspt-form').hide();	
			
			if (buttonClick && $(window).width() <= 480)  {
			   $('.divBookingLocationResults').hide();
			}	
		}
		
		function btnNoTime()  {
			$('#scheduling-page-clinic .hbspt-form').toggle();	
			
			changeArrow('divNoTime');			
		}
		
		function btnLocationMore()  {			    	        
		    $('.divBookingLocationResults').show();					   
			$('.divClinic').toggle();	
			
			changeArrow('divLocationMore');
		}
		
		function changeArrow(className)  {
			if ($('.' + className + ' i').hasClass('fa-chevron-right'))  {	
			   $('.' + className + ' i').removeClass('fa-chevron-right');
			   $('.' + className + ' i').addClass('fa-chevron-down');			   
		    } else  {	
			   $('.' + className + ' i').removeClass('fa-chevron-down');
			   $('.' + className + ' i').addClass('fa-chevron-right');			   
		    }
		}
		
		function btnCurrentLocation()  {
	   	      $('#txtPostcode').val('');	  	   	     
		   	  getLocation(); 
		}
			
		function getLocation() {
	       //btnFind(-37.8109, 144.9644); // Doncaster VIC
	       
		   if (navigator.geolocation) {
			  navigator.geolocation.getCurrentPosition(usePosition);			 
		   } else {
			 alert("Sorry, geolocation is not supported by this browser.");
		   }
		}
			
		function usePosition(position) {
		    //alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
		    btnFind(position.coords.latitude, position.coords.longitude);
		}
			
		$( ".divBookingLocation-acuity" ).focus(function() {
			alert( "Handler for .focus() called." );
		});							
				
	</script>

@endsection