 <div class="home-double-panel">
	 <div class="container">
        <div class="row">
           <div class="col-lg-6">
              @include('site/partials/index-faqs')
		   </div>
		   <div class="col-lg-6">
              @include('site/partials/index-askdoctor')
           </div>
        </div>
   </div>
</div>
