<div class="home-intro1">
    <div class="container">
        <div class="row">            
            <div class="col-lg-12 ">
                <div class="home-intro-main">
                   <?php echo $home_intro_text_main; ?>
				</div>
                
                 <div class='home-intro-booking'>   
			        <button class="btn-booking" onclick="window.location.href='{{ url('') }}/make-a-booking'">Book an Appointment</button>
		         </div>
            </div>            
        </div>                                                
    </div>
</div>
