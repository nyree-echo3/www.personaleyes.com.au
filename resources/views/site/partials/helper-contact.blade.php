<div class='navbar-contacts'>    
  @php
     $service_slug = Cookie::get('services_slug');       
  @endphp
  
  @if ((isset($home_style_override) && $home_style_override) || (!isset($home_style_override) && $service_slug == ""))    
      <a class="contact-healthcare" href="{{ url('') }}/healthcare-professionals">For Health Care Professionals</a>
   @endif
   
   <a class="contact-icon" href="{{ url('contact') }}"><i class="fas fa-envelope"></i></a>
   <a id="phone-number" href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class="fas fa-phone-alt"></i> {{ $phone_number }}</a>
</div>