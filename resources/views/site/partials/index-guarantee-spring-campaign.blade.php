
 <div class="home-guarantee">   
   <h2>Australia's first satisfaction guarantee</h2>
   
   <div class="home-guarantee-icons">
	   <img src="{{ asset('images/site/icon-LifetimeVisionProgram2.png') }}" alt="Lifetime Vision Program">
	   <img src="{{ asset('images/site/icon-Aus1stSatisfactionGurantee2.png') }}" alt="Australia's First Satisfaction Gurantee">
	   <img src="{{ asset('images/site/icon-PriceGuarantee2.png') }}" alt="Price Guarantee">
   </div>
</div>	
