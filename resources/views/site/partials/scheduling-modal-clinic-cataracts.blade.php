<link rel="stylesheet" href="{{ asset('css/site/hs-form.css?v=0.1') }}">
<div class="modal fade" id="scheduling-page-cataracts" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row booking-header-part no-gutters">
					<div class="col-sm-12 text-center">
						<img src="{{ url('') }}/images/site/logo-onlinebooking.gif" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
						<h2 class="scheduling-page-header scheduling-page-header-cataracts">Let's book your Cataracts Appointment!</h2>						
					</div>																				   							        	                	        
		        </div>	
		             					
			   <div class="divBookingLocation-results">			       					
					<div class="divBookingLocationResults divBookingLocationResults-cataracts">
					    <div class="container">
							<div class="row">
							    <div class='col-12'><a href='#' onclick='btnShowClinicCataract()' class='divNoTime'><h3>Choose your clinic <i class='fa fa-chevron-right'></i></h3></a></div>
								@foreach ($locations as $location) 
									  @php								  
										 $locationName = str_replace(" ACT", "", str_replace(" NSW", "", str_replace("Clinic, ", "", $location["name"])));							  
									  @endphp

									  <div class='col-lg-3 col-sm-12 divClinic'>
										  <a href='#' onclick='btnClinicCataracts({{ $location["id"] }})'>
											  <div id='divLocation{{ $location["id"] }}' class='divLocation divLocation-cataracts'>
											  <div id='divLocationName{{ $location["id"] }}' class='divLocationName'>{{ $locationName }}</div>
											  <div id='divLocationAddress{{ $location["id"] }}' class='divLocationAddress divHide'>{{ $location["location"] }}</div>									  
											  </div>
										  </a>
									  </div>						  					      	              
							   @endforeach
						   </div>  
					   </div>  
					</div>                         				   
			   </div>
              
               <div class="row scheduling-page-footer divInvisible">
                    <div class="col-sm-12 divNoTimeWrapper divHide">
                        <a href='#' onclick='btnNoTimeCataracts()' class='divNoTime'>Can't find a time that suits? <i class='fa fa-chevron-right'></i></a>
						<input id="hs-form-show-cataracts" type="hidden" value="0">
                    </div>

				   <!--[if lte IE 8]>
				   <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
				   <![endif]-->
				   <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>

					<div class="col-sm-12 divNoTimeForm divHide" id="hs-form-container-cataracts">

					</div>
				</div>
               
               <div class="divBookingLocation-details-cataracts">					                       				   
			   </div>                              
                
               <div class="divBookingLocation-acuity-cataracts">               
			      <script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
			   </div>
			   				   				
			</div>
		</div>
	</div>
</div>

<div class="divAllClinics">
   <div class="container">
		<div class="row">
			@foreach ($locations as $location) 
				  @php								  
					 $locationName = str_replace("Clinic, ", "", $location["name"]);							  
				  @endphp

				  <div class='col-lg-3 col-sm-12 divClinic'>
					  <a href='#' onclick='btnClinic({{ $location["id"] }})'>
						  <div id='divLocation{{ $location["id"] }}' class='divLocation'>
						  <div id='divLocationName{{ $location["id"] }}' class='divLocationName'>{{ $locationName }} <i class='fa fa-chevron-right'></i></div>
						  <div id='divLocationAddress{{ $location["id"] }}' class='divLocationAddress divHide'>{{ $location["location"] }}</div>									  
						  </div>
					  </a>
				  </div>						  					      	              
		   @endforeach

	    </div>
	 </div>
</div>


@section('inline-scripts-scheduling-modal-cataracts')
	<script type="text/javascript">																			
		function btnClinicCataracts(clinicID, buttonClick = true)  {	
			$('.divLocation').removeClass('divBookingLocationActive');
			$('#divLocation' + clinicID).addClass('divBookingLocationActive');	
			
			$('.scheduling-page-footer').css("visibility", "visible");
			$('.scheduling-page-footer').css("height", "42");
			resetArrow('divNoTime');	
			$('.divNoTimeWrapper').hide();
			
			html  = "<div class='divLocationName'>" + $('#divLocationName' + clinicID).html() + "</div>"; 		
			html += "<div class='divLocationAddress'>" + $('#divLocationAddress' + clinicID).html() + "</div>"; 				
			
			$('.divBookingLocation-details-cataracts').html(html);	
			$('.divBookingLocation-details-cataracts').show();
			
			html  = "";		
			html += '<iframe src="https://personaleyes-lasik-booking.as.me/?appointmentType=17794438&calendarID=' + clinicID + '" width="100%" height="650" frameBorder="0"></iframe>';			
			$('.divBookingLocation-acuity-cataracts').html(html);			
			$('.divBookingLocation-acuity-cataracts').show();
			
			$('#scheduling-page-cataracts .hbspt-form').hide();	
			
			if (buttonClick && $(window).width() <= 480)  {
			   //$('.divBookingLocationResults').hide();
			   $('.divClinic').hide();				
			}

            $('#hs-form-container-cataracts').hide();
						
			$('.divNoTimeWrapper').fadeIn(5000);
		}	
		
		function btnShowClinicCataract()  {
			$('.divClinic').css("height", "auto");									
			$('.divClinic').toggle();	
			
			changeArrow('divNoTime');	
		}

        function btnNoTimeCataracts()  {
            $('.scheduling-page-footer').css("height", "auto");

            if($('#hs-form-container-cataracts').is(":visible")){
                $('#hs-form-container-cataracts').hide();
            }else if($('#hs-form-container-cataracts').is(":hidden")){
                $('#hs-form-container-cataracts').show();
            }

            if($("#hs-form-show-cataracts").val()=="0"){

                hbspt.forms.create({
                    portalId: "5064462",
                    formId: "808dff57-6d05-4dcf-8c0f-a7a863797ce3",
                    target: "#hs-form-container-cataracts"
                });
            }

            $("#hs-form-show-cataracts").val('1');

            changeArrow('divNoTime');
        }
	</script>

@endsection