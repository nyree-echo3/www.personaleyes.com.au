<div class="panelTestimonials">
	<div class="containter">
		<div class="row">
			<div class="col-lg-4">
	           <div class='panelTestimonials-box'>
				   <div class='panelTestimonials-txt'>
					  Surgery was very quick and easy.  The recovery took me less than 3 days and I was back to work! I would definitely recommend personalEYES to anyone looking to improve their eyesight.
				   </div>

				   <div class='panelTestimonials-name'>
					  Jessica Ahuja 
				   </div>

				   <div class="stars">★★★★★</div>
			   </div>				  			   
			</div>
					
			<div class="col-lg-4">
	           <div class='panelTestimonials-box'>
				   <div class='panelTestimonials-txt'>
					  I always hated glasses, so I lived in my contact lenses 7 days per week, 17 hours per day. I was hesitant to get laser surgery because I was worried but I'm so glad I did it! I can travel, swim and run without worrying about contact lenses and contact lens hygiene. It makes parenting easier too - I can even pick my baby up with my bag, her bag, groceries and toys without knocking glasses off my face.
				   </div>

				   <div class='panelTestimonials-name'>
					  Anna Siu 
				   </div>

				   <div class="stars">★★★★★</div>		
			   </div>		  			   
			</div>
					
			<div class="col-lg-4">
	           <div class='panelTestimonials-box'>
				   <div class='panelTestimonials-txt'>
					  The surgery was quick and amazing. Dr Meades knows what she is doing and I felt safe and calm during the whole process. Recovery was easy and smooth and now I can see clearly, never having to worry about buying new glasses, just enjoying life glasses-free!
				   </div>

				   <div class='panelTestimonials-name'>
					  Elizabeth Arias 
				   </div>

				   <div class="stars">★★★★★</div>				  			   
				</div>						
		    </div>			
		</div>
	</div>
</div>