<div class="spring-campaign-header">                    	
	<a href="{{ url('') }}" title="{{ $company_name }}">                		               
		<img src="{{ url('') }}/images/site/logo-sm.png" title="{{ $company_name }}" alt="{{ $company_name }}">
	</a>	
	
	<div class="spring-campaign-header-h1">
	Feel the freedom of 20/20 vision with LASIK eye surgery	
	</div>                
</div>