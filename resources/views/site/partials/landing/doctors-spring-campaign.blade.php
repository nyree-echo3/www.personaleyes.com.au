 <div class="index-doctors index-doctor-spring">
	 <div class="container-fluid p-0">
        <div class="row">           
		   <div class="col-lg-6 div-doctors div-doctors1 div-left">
              <div class="index-doctors-txt index-doctors-txt-spring">
                  <h2>Why choose personalEYES?</h2>				  
				  <p>PersonalEYES was started 25 years ago with the purpose of delivering an altogether better experience than was available anywhere else in Australia. Our personalised approach and impeccable results have seen us grow to 10 state-of-the-art clinics with a large staff of supporting specialists and internationally-recognised surgeons.</p>
                  <button class="btn-booking-del summer-book-click" href="" data-toggle="modal" data-target="#deliveroo-modal">
                      BOOK YOUR FREE ASSESSMENT
                      <span>Send me my voucher code for a $40 deliveroo voucher!</span>
                  </button>
              </div>
           </div>
           <div class="col-lg-6 div-doctors div-right">
              <img src='{{ url('') }}/images/site/who-we-are.png' title="Dr Kerry Meades" alt="Dr Kerry Meades">
		   </div>
        </div>
   </div>
</div>
