 <div class="index-doctors">
	 <div class="container">
        <div class="row">           
		   <div class="col-lg-6 div-doctors div-doctors1 div-left">
              <div class="index-doctors-txt">
                  <h2>Why choose personalEYES</h2>				  
				  <p>With our unwavering commitment to your safety & a unique personalised care approach, personalEYES has become one of the most experienced corrective eye surgery groups in Australasia.  Our friendly team of specialists and internationally-recognized surgeons are committed to achieving the best possible vision for you.</p>              				  
              </div>
           </div>
           <div class="col-lg-6 div-doctors div-right">
              <img src='{{ url('') }}/images/site/who-we-are.png' title="Dr Kerry Meades" alt="Dr Kerry Meades">
		   </div>
        </div>
   </div>
</div>
