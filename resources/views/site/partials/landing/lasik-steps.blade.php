<div class="lasikStepsWrapper">
	<h2>What's the Process?</h2>
	<p>Have you been thinking about getting LASIK, but not sure what's involved?</p>
	<p>Well, we've mapped out the journey to 20/20 vision in 6 steps... it's easier than you may think!</p>
	
	<div class="container h-100">
	<div class="row h-100">
	
	<div class="col-lg-4 p-custom">	
	<div class="blue-box blue-box-vision">
		<div class="path-vision">
			<div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/1.png" /></div>

			<div class="path-vision-txt">
				<p>Make your <a data-target="#scheduling-page-clinic" data-toggle="modal" href="#">free appointment</a> easily online for one of our 10 state-of-art clinics in NSW and ACT</p>
			</div>
		</div>
	</div>
	</div>
	
	<div class="col-lg-4 p-custom">			
	<div class="blue-box blue-box-vision">
		<div class="path-vision">			
		    <div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/2.png" /></div>
		    
			<div class="path-vision-txt">
				<p>Over the hour appointment, our certified optometrists will thoroughly examine your eyes to determine if you&rsquo;re suitable for LASIK and which procedures are available to you. This appointment is completely obligation-free and will give you definitive treatment and pricing by the end of it.</p>
			</div>			
		</div>
	</div>
	</div>
	
	<div class="col-lg-4 p-custom">			
	<div class="blue-box blue-box-vision">
		<div class="path-vision">			
			<div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/3.png" /></div>

			<div class="path-vision-txt">
				<p>If you choose to proceed you will then meet with one of our world-renowned surgeons who will take you through all the in&#39;s and out&#39;s of your procedure. After this, your surgery can usually be booked within just a few weeks.</p>
			</div>
		</div>
	</div>
	</div>
	
	<div class="col-lg-4 p-custom">			
	<div class="blue-box blue-box-vision">
		<div class="path-vision">			
		    <div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/4.png" /></div>
		    
			<div class="path-vision-txt">
				<p>LASIK surgery is not painful and usually only takes less than 10 minutes per eye and does not require general anaesthesia. You should plan on being in the clinic for approximately 2 - 3 hours on your procedure day.</p>
			</div>			
		</div>
	</div>
	</div>
	
	<div class="col-lg-4 p-custom">			
	<div class="blue-box blue-box-vision">
		<div class="path-vision">			
			<div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/5.png" /></div>

			<div class="path-vision-txt">
				<p>Your eyes start healing immediately after your LASIK surgery and most people can drive the following day.</p>
			</div>
		</div>
	</div>
	</div>
	
	<div class="col-lg-4 p-custom">			
	<div class="blue-box blue-box-vision">
		<div class="path-vision">			
		    <div class="path-vision-img"><img alt="" src="{{ url('') }}/images/site/6.png" /></div>
		    
			<div class="path-vision-txt">
				<p>Regular follow-up visits are usually scheduled for six months at which point it should be completely stable. Most patients achieve 20/20 vision and often even better!</p>
			</div>			
		</div>
	</div>
	</div>
		
	</div>
    </div>
</div>