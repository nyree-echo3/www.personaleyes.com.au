<div class="footer-summer">
	<div class="container-fluid">
		<div class="row justify-content-center align-items-center">
			<div class="col-lg-1"></div>
			
			<div class="col-lg-8">
				<div class="footer-summer-h1">We donate $10 to Vision Australia on your behalf when you book a LASIK assessment with us! *</div>
				<div class="footer-summer-h2">Vision Australia makes a huge difference to Australians suffering with vision loss.</div>
				<div class="footer-summer-h3">* Enter code HELLOFRESH to ensure your donation!</div>
			</div>
			
			<div class="col-lg-2">
			    <div class="footer-summer-logo">
					<a href="" target="_blank">
						<img src="{{ url('') }}/images/site/logo-visionaustralia.png" alt="Vision Australia" text="Vision Australia"> 
					</a>
				</div>
			</div>
			
			<div class="col-lg-1"></div>
		</div>
	</div>
</div>