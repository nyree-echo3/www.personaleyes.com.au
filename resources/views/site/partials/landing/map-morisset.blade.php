<div class="panelMap">
	<div class="containter">
		<div class="row">
			<div class="col-lg-6">
		       <div class='panelMap-txt'>
				   <h2>Get it done with the best</h2>
				   <div class="stars">★★★★★</div>
				   <ul>
					  <li>Quick and painless procedure with permanent results. </li>
					  <li>An unwavering commitment to your safety.</li>
					  <li>Treat now, pay later with 0% interest options.</li>
					  <li>Access our Australian-first Satisfaction Guarantee. </li>
					  <li>10 state-of-the-art clinics throughout NSW and ACT</li>
				   </ul>
			   </div>
			</div>
			
			<div class="col-lg-6 panelMap-img">
			   <img src='{{ url('') }}/images/site/morisset-map.png' title="Locations Map" alt="Locations Map">
			</div>
		</div>
	</div>
</div>