<div class="homeVideoLasik">
    <div class="homeVideoLasik-wrapper">               
       <div class="homeVideoLasik-caption">	
           <div class="landing-logo">
			  <a href="{{ url('') }}" title="{{ $company_name }}">                
				<img src="{{ url('') }}/images/site/logo-sm.png" title="{{ $company_name }}" alt="{{ $company_name }}">
			  </a>
		   </div>
          
		   Are you a suitable LASIK candidate? 		   		   
		   
		   <div class="homeVideoLasik-caption-sml">
		   Take the quick online suitability quiz to find out!		   
           </div>	  
           
		   <div class='homeVideoLasik-booking'>   
			  <a href='#div-suitability-quiz' class='btn-booking'>Take the quiz</a>
		   </div>
	   </div>
    </div>        
	
    <div class="video-container-lasik">
		<iframe src="https://player.vimeo.com/video/374081225?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
		
		<div id="chevron-down" class="chevron-down">
			<a href='#div-suitability-quiz' id="btnSuitabilityQuizDown"><i class="fas fa-chevron-down"></i></a>
	    </div>
	</div>
</div>
