<div id="parallax" class="parallaxParent parallax-container">   
	<div class="parallax-img parallax-img-main">
		<div class="parallax-img-img"></div>
	</div>
</div>										
						
							
@section('scripts-parallex')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts-parallex') 
	<script>
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes				
		new ScrollMagic.Scene({triggerElement: "#parallax"})
						.setTween("#parallax > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax3-img").addClass('parallax3-img-mod');
		
	</script>
@endsection