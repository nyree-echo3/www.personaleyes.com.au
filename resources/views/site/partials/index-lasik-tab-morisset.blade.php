<div class="container lasik-tab-cont">
    <div class="row">
        <div class="col-sm-12">

            <ul class="nav nav-tabs nav-fill" id="lasik-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="types-tab" data-toggle="tab" href="#types" role="tab" aria-controls="types" aria-selected="true">
                        Types of LASIK
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="faqs-tab" data-toggle="tab" href="#faqs" role="tab" aria-controls="faqs" aria-selected="false">
                        FAQs
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="suitability-tab" data-toggle="tab" href="#suitability" role="tab" aria-controls="suitability" aria-selected="false">
                        Suitability
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="costs-tab" data-toggle="tab" href="#costs" role="tab" aria-controls="costs" aria-selected="false">
                        Costs
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane fade show active" id="types" role="tabpanel" aria-labelledby="types-tab">
                    <div class="container">
                        <div class="row scrollable">
                            <div class="col-md-12 col-lg-6 tab-type-box">
                                <div class="row no-gutters">
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-5">
                                        <img src="{{ asset('/images/site/PRK.png') }}" title="PRK" alt="PRK">
                                    </div>
                                    <div class="col-12 col-sm-7 col-md-8 col-lg-7">
                                        <h2>PRK</h2>
                                        <p>A surgeon uses a topical solution to remove the top layer of corneal cells, and a laser to reshape the exposed corneal tissue below.</p>
                                        <a href="{{ url('lasik/transprk-laser-surgery') }}" class="read-more">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 tab-type-box">
                                <div class="row no-gutters">
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-5">
                                        <img src="{{ asset('/images/site/SMILE.png') }}" title="PRK" alt="PRK">
                                    </div>
                                    <div class="col-12 col-sm-7 col-md-8 col-lg-7">
                                        <h2>SMILE</h2>
                                        <p>A surgeon makes a small incision in the cornea with a laser and then removes a small disc of underlying corneal tissue.</p>
                                        <a href="{{ url('lasik/smile-laser-surgery') }}" class="read-more">READ MORE</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-6 tab-type-box">
                                <div class="row no-gutters">
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-5">
                                        <img src="{{ asset('/images/site/LASIK.png') }}" title="PRK" alt="PRK">
                                    </div>
                                    <div class="col-12 col-sm-7 col-md-8 col-lg-7">
                                        <h2>LASIK</h2>
                                        <p>A surgeon uses lasers to create a flap in the cornea and reshape the underlying tissue before replacing the flap.</p>
                                        <a href="{{ url('lasik/bladeless-zlasik') }}" class="read-more">READ MORE</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-6 tab-type-box">
                                <div class="row no-gutters">
                                    <div class="col-12 col-sm-5 col-md-4 col-lg-5">
                                        <img src="{{ asset('/images/site/ICL.png') }}" title="PRK" alt="PRK">
                                    </div>
                                    <div class="col-12 col-sm-7 col-md-8 col-lg-7">
                                        <h2>ICL</h2>
                                        <p>An ICL is a custom lens, similar to a contact lens, that is created by a surgeon and implanted directly into the eye.</p>
                                        <a href="{{ url('lasik/icl-implantable-lens') }}" class="read-more">READ MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center booking-button-cont">
                            <button class="acuity-embed-button btn-booking-del summer-book-click" data-toggle="modal" data-target="#scheduling-page-morisset">
                                BOOK YOUR FREE ASSESSMENT
                            </button>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show" id="faqs" role="tabpanel" aria-labelledby="faqs-tab">
                    <div class="container faq-cont">
                        <div class="row scrollable">
                            <div class="col-12">
                                <div class="row no-gutters faq-item">
                                    <h2>Am I suitable for LASIK or Laser Eye Surgery?</h2>
                                    <p>Several factors are used to assess your suitability for treatment, these include your age (usually over 20), your vision (preferably unchanged for the previous 12 months), the health of your eyes (free of any eye disease) and your general health. It is also preferable that you are not pregnant.</p>
                                    <p>If you are not suitable for LASIK, there are now many other excellent options available please discuss with our doctors.</p>
                                </div>
                                <div class="row no-gutters faq-item">
                                    <h2>How long does it take?</h2>
                                    <p>For both eyes, the whole procedure takes about 10 minutes per eye, but you will be at the centre for a few hours to ensure all pre and post-operative checks have been completed.</p>
                                </div>
                                <div class="row no-gutters faq-item">
                                    <h2>Will I need to take time off work?</h2>
                                    <p>In most cases, you can go back to work the day after your procedure. Most patients regain 90% of their vision within 24 hours however you can expect to notice improvements and minor fluctuations over the next few weeks.</p>
                                </div>
                                <div class="row no-gutters faq-item">
                                    <h2>How soon can I drive after having LASIK eye surgery?</h2>
                                    <p>For most people, you can resume driving the next day. We would recommend you do not drive for a day or two after surgery.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center booking-button-cont">
                            <div class="booking-button"><a href="{{ url('faq/frequently-asked-questions') }}">READ MORE FAQs</a></div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show" id="suitability" role="tabpanel" aria-labelledby="suitability-tab">
                    <div class="container suitability-cont">
                        <div class="row scrollable">
                            <div class="col-12">
                                <div class="row no-gutters">
                                    <h2>Suitability for LASIK</h2>
                                    <p>Each pair of eyes is unique and LASIK isn’t suitable for everyone. Typically we say no to 1 in 5 patients because our patients safety is our number 1 priority. Several factors are used to assess your suitability for treatment, these include:</p>
                                    <ul>
                                        <li><span>your age (usually over 20 to 40/45)</span></li>
                                        <li><span>your lifestyle needs, e.g. work, sport or hobbies</span></li>
                                        <li><span>stable vision for up to 12 months</span></li>
                                        <li><span>vision correction and astigmatism inside acceptable ranges</span></li>
                                        <li><span>healthy eyes (free of any eye disease)</span></li>
                                        <li><span>good general health (it is also preferable that you are not pregnant)</span></li>
                                    </ul>
                                    <p>Your eyes will need to be examined thoroughly to determine your suitability and to enable an accurate assessment of the correct procedure for your eyes.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center booking-button-cont">
                            <div class="booking-button"><a href="{{ url('vision-correction-suitability') }}">TAKE THE ONLINE SUITABILITY QUIZ</a></div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show" id="costs" role="tabpanel" aria-labelledby="costs-tab">
                    <div class="container costs-cont">
                        <div class="row scrollable">
                            <div class="calculator">
                                <img src="{{ asset('/images/site/calculator.jpg') }}" title="PRK" alt="PRK">
                            </div>
                            <div class="col-12">
                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <h2>The cost of LASIK</h2>
                                        <p>LASIK prices vary from clinic to clinic across Australia. Here's a brief breakdown:</p>
                                        <div class="sub-header">UPFRONT COSTS - What you need to know</div>
                                    </div>
                                </div>
                                <div class="row table-row table-heading">
                                    <div class="col-3"></div>
                                    <div class="col-3">Other clinics</div>
                                    <div class="col-3 dark-blue">At personalEYES</div>
                                </div>
                                <div class="row table-row first-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Consultation fees</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Approx $250 each.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>Initial LASIK assessment is free</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Surgery cost</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Surgery ranges from $2,525 to $6,200 per eye in Australia, depending on the clinic and the type of procedure.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>Our packages start at $2,275 per eye</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">All-inclusive cost vs individual costs</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Some clinics in Australia charge for follow-up appointments, medications and diagnostic tests.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>personalEYES charges one flat rate that includes the full cost of the initial consultation, surgery and follow-up appointments.</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <div class="sub-header">ONGOING COSTS</div>
                                    </div>
                                </div>
                                <div class="row table-row table-heading">
                                    <div class="col-3"></div>
                                    <div class="col-3">Other clinics</div>
                                    <div class="col-3 dark-blue">At personalEYES</div>
                                </div>
                                <div class="row table-row first-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Follow-up appointments</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Follow-up appointments may cost between $70 and $150.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>Included in your surgery cost.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Post-op medication or treatments</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Eye-drops will cost around $50 whilst other medications will vary.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>Included in your surgery cost.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Emergencies</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>The price depends on the hospital, necessary interventions and your healthcare cover.</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>We offer free 24/7 after-care for 6-12 months</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-3 desc">Additional procedures</div>
                                    <div class="col-12 col-sm-12 col-md-3"><div class="mobile-heading">- Other clinics</div>Ranges from $2,000 to $12,600 depending on the procedure</div>
                                    <div class="col-12 col-sm-12 col-md-3 last-row dark-blue">
                                        <div class="mobile-heading">- At personalEYES</div>
                                        <ul class="fa-ul">
                                            <li><span class="fa-li"><i class="fa fa-check"></i></span>Lifetime of Vision program with retreatment costs covered.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-9 info-box">At personalEYES, safety is the first priority, and our retreatment rate is approximately 1% - which is all covered by our guarantee & Lifetime of Vision Program.</div>
                                </div>
                                <div class="row no-gutters">
                                    <div class="col-12">
                                        <div class="sub-header">COMPARISON OF LASER EYE SURGERY COSTS IN AUSTRALIA</div>
                                    </div>
                                </div>
                                <div class="row table-row table-heading">
                                    <div class="col-5">Procedure</div>
                                    <div class="col-4">Average price per-eye</div>
                                </div>
                                <div class="row table-row first-row">
                                    <div class="col-12 col-sm-12 col-md-5"><div class="mobile-heading last-table">- Procedure</div>LASIK (laser-assisted in situ keratomileusis)</div>
                                    <div class="col-12 col-sm-12 col-md-4 last-row">
                                        <div class="mobile-heading">- Average price per-eye</div>
                                        <div>$2,200-$3,400</div>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-5"><div class="mobile-heading last-table">- Procedure</div>PRK (photorefractive keratectomy) TransPRK</div>
                                    <div class="col-12 col-sm-12 col-md-4 last-row">
                                        <div class="mobile-heading">- Average price per-eye</div>
                                        <div>$2,000-$3,400</div>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-5"><div class="mobile-heading last-table">- Procedure</div>SMILE (small incision lenticular extraction)</div>
                                    <div class="col-12 col-sm-12 col-md-4 last-row">
                                        <div class="mobile-heading">- Average price per-eye</div>
                                        <div>$3,300-$3,700</div>
                                    </div>
                                </div>
                                <div class="row table-row">
                                    <div class="col-12 col-sm-12 col-md-5"><div class="mobile-heading last-table">- Procedure</div>ICL (implantable contact lenses)</div>
                                    <div class="col-12 col-sm-12 col-md-4 last-row">
                                        <div class="mobile-heading">- Average price per-eye</div>
                                        <div>$4,700-$6,200</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-9 disclaimer">Disclaimer: We’ve researched the average prices of various vision care methods in Australia, but prices will vary based on individual circumstances.</div>
                                </div>

                                <div class="row button-cont">
                                    <div class="col-12 col-sm-12 col-md-4">
                                        <a class="btn cost-button" href="{{ url('costs/lasiklaser-eye-surgery-cost') }}">LEARN MORE<span>about costs here</span></a>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-4">
                                        <a class="btn cost-button" href="#modalInfoPack">DOWNLOAD<span>our info Booklet & Pricing Plans</span></a>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-4">
                                        <a class="btn cost-button" href="{{ url('costs/cost-savings-calculator') }}">CALCULATE<span>how much LASIK could save you</span></a>
                                    </div>



                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>