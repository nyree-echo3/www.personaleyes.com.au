<link rel="stylesheet" href="{{ asset('css/site/hs-form.css?v=0.1') }}">
<div class="modal fade" id="deliveroo-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="col-sm-12">
					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
						hbspt.forms.create({
							region: "na1",
							portalId: "5064462",
							formId: "1b497f5b-dcaf-40cd-89dd-1fb48f166d49"
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>