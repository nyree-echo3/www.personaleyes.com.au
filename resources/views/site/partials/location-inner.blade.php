<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner location-page">
        <div class="carousel-item active" style="background-image: url('{{ url('') }}/{{ $header_image }}')">
            
            <div class="container">
                <div class="carousel-caption-inside">
                    <div class="carousel-caption-inner">
                        <div class="carousel-caption-h1">{{ $category_name }}</div>
                    </div>
                </div>
                <div class="carousel-location-name-cont">
                    <div class="location-name-header">Eye Specialists & Laser Eye Surgery in</div>
                    <div class="carousel-location-name">{{ $location->name }}</div>
                </div>
            </div>
        </div>
    </div>
</div>