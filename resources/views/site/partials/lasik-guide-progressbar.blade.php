<div class="lasik-guide-progressbar" data-toggle="affix">
    @php 
	   $pageCounter = 0;
	   $divCounter = 0;
	@endphp
	
	@if (isset($allPages))
		@foreach ($allPages as $page)
		   @php 
			  $pageCounter++;  
		   @endphp

		   @if ($pageCounter != 1 && $pageCounter != 3 )  
			   @php 
				 $divCounter++;  
			   @endphp

			   <div id="bar{{ $divCounter }}" class="bar">&nbsp;</div>	   
		   @endif
		@endforeach
	@endif
</div>