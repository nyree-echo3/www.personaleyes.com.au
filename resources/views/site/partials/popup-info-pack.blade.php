<!-- The Modal -->
<div class="modal fade modal-InfoPack" id="modalInfoPack">
    <div class="modal-dialog modal-lg modal-dialog-centered">

        <div class="modal-content">
            <div class="container no-gutters">
                <div class="row no-gutters">

                    <div class="col-lg-4">
                        <div class="div-overlay">
                            <div class="div-overlay-top">
                                <img class="guide-popup-img" src="{{ url('') }}/images/site/slideout-digitalinfopack.png" title="Info Booklet" alt="Info Booklet">
                                <img class="guide-popup-img-resp" src="{{ url('') }}/images/site/digitalinfopack-cover-mobile.png" title="Info Booklet" alt="Info Booklet">
                            </div>
                        </div>
                    </div><!-- .col-lg-4 -->

                    <div class="col-lg-8">
                        <div class="modal-content-txt">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <div class="modal-btn">
                                    <a class="btnMore" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/cross.png" title="Close" alt="Close"></a>
                                </div>

                                <div class="modal-header-content">
                                    <div class="modal-header-content-header">
                                        <div class="modal-header-content-header2">Download our in-depth LASIK Info Booklet & Pricing Plans</div>
                                        <div class="modal-header-content-p">
                                            <p>Everything you need to know about getting LASIK eye surgery done with personalEYES, from surgery options to pricing packages. </p>
                                            <p>Get your hands on it below…</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <input id="hs-form-show-info-pack" type="hidden" value="0">
                                <!-- HubSpot Form - START -->
                                <!--[if lte IE 8]>
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                <![endif]-->
                                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                                <!-- HubSpot Form - END -->

                                <div class="modal-body-body" id="hs-form-info-pack-container">
                                </div><!-- .modal-body-body -->
                            </div><!-- .modal-body -->
                        </div><!-- .modal-content-txt -->
                    </div><!-- .col-lg-9 -->
                </div><!-- .row -->
            </div><!-- .containter -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- .modal -->


@section('inline-scripts-popup-info-pack')
    <script type="text/javascript">
        $('a[href$="#modalInfoPack"]').on("click", function () {
            $('#modal-popup').hide();
            $('#modalInfoPack').modal();
        });

        $('#modalInfoPack').on('show.bs.modal', function (e) {
            console.log('show fired')

            if($("#hs-form-show-info-pack").val()=="0") {

                hbspt.forms.create({
                    portalId: "5064462",
                    formId: "cdb04485-a267-4df2-9492-e3424e4e2920",
                    target: "#hs-form-info-pack-container"
                });

            }

            $("#hs-form-show-info-pack").val('1');

        })
    </script>

@endsection