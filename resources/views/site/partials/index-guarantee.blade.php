
 <div class="home-guarantee">   
   <h2>Australia's first satisfaction guarantee to ensure you get the best outcome!</h2>
   
   <div class="home-guarantee-icons">
	   <img src="{{ asset('images/site/icon-LifetimeVisionProgram2.png') }}" alt="Lifetime Vision Program">
	   <img src="{{ asset('images/site/icon-PriceGuarantee2.png') }}" alt="Price Guarantee">
   </div>
</div>	
