<div class="home-panel-cta" id='home-panel-cta'>
   <div class="container">
	  <div class="row">         		   	 
		  <div class="col-lg-4 home-panel-cta-box">	            
		       <div class="home-panel-cta-box">
		           <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon1.png" alt="The Ultimate Australian Guide to LASIK" />			   
				   </div>
				   <div class="home-panel-cta-box-txt">					  
					   <h2>The Ultimate Australian Guide to LASIK</h2>					   
                   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('/lasik/the-ultimate-guide-to-lasik') }}" class="download-now-button">Download Now</a></div>
               </div>			  
		  </div><!-- /.col-lg-4 -->	
		  
		  <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/lead-magnet-suitability-quiz.gif" alt="Take the Online Suitability Quiz" />
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Take the Online Suitability Quiz</h2>					    
				   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('/vision-correction-suitability') }}" class="take-the-suitability-quiz-button">Take The Quiz</a></div>
               </div>             			   			   							   
		  </div><!-- /.col-lg-4 -->				  			
		
		   <!--<div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon2.png" alt="Take the Australian Online Eye Test" />
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Take the Australian Online Eye Test</h2>					    
				   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('/vision/online-eye-test') }}" class="take-the-test-button">Take The Test</a></div>
               </div>             			   			   							   
		  </div>--><!-- /.col-lg-4 -->				  
		
		  <!--
		  <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/icon3.png" alt="Calculate how much you could save with LASIK" />			
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Calculate how much you could save with LASIK</h2>					      			   			   				   
                   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="{{ url('costs/cost-savings-calculator') }}" class="calculate-now-button">Calculate Now</a></div>
               </div>
		  </div>
		  -->
		  <!-- /.col-lg-4 -->
		  		  
		  <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/lead-magnet-info-pack.gif" alt="Pricing & Surgery Options Booklet" />			
				   </div>
				   <div class="home-panel-cta-box-txt">					   
					   <h2>Pricing & Surgery Options Booklet</h2>					      			   			   				   
                   </div>
                   <div class="btn-cta btn-cta-mobile"><a href="#modalInfoPack" class="calculate-now-button">Download Now</a></div>
               </div>
		  </div>		 
		  <!-- /.col-lg-4 -->
		  
		  <div class="col-lg-4 home-panel-cta-box btn-cta-desktop">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('/lasik/the-ultimate-guide-to-lasik') }}" class="download-now-button">Download Now</a></div>
			 </div>
		  </div>
		  
		  <div class="col-lg-4 home-panel-cta-box btn-cta-desktop">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('/vision-correction-suitability') }}" class="take-the-suitability-quiz-button">Take The Quiz</a></div>
			 </div>
		  </div>
		  
		  <!--<div class="col-lg-4 home-panel-cta-box btn-cta-desktop">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('/vision/online-eye-test') }}" class="take-the-test-button">Take The Test</a></div>
			 </div>
		  </div>-->
		  
		  <!--
		  <div class="col-lg-4 home-panel-cta-box btn-cta-desktop">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="{{ url('costs/cost-savings-calculator') }}" class="calculate-now-button">Calculate Now</a></div>
			 </div>
		  </div>	
  	      -->
	  	  <div class="col-lg-4 home-panel-cta-box btn-cta-desktop">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="#modalInfoPack" class="calculate-now-button">Download Now</a></div>
			 </div>
		  </div>						
		  				
	   </div>
   </div>
</div>
