<link rel="stylesheet" href="{{ asset('css/site/hs-form.css?v=0.1') }}">
<div class="modal fade" id="scheduling-page-morisset" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row booking-header-part no-gutters">
                    <div class="col-sm-12 text-center">
                        <img src="{{ url('') }}/images/site/logo-onlinebooking.gif" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
                        <h2 class="scheduling-page-header">Let's book your free LASIK Assessment!</h2>
                    </div>
                </div>

                <div class="row scheduling-page-footer divInvisible">
                    <div class="col-sm-12 divNoTimeWrapper divHide">
                        <a href='#' onclick='btnNoTime()' class='divNoTime'>Can't find a time that suits? <i class='fa fa-chevron-right'></i></a>
                        <input id="hs-form-show" type="hidden" value="0">
                    </div>

                    <!--[if lte IE 8]>
                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                    <![endif]-->
                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>

                    <div class="col-sm-12 divNoTimeForm divHide" id="hs-form-container"></div>
                </div>

                <div class="divBookingLocation-details">
                </div>

                <div class="divBookingLocation-acuity">
                    <script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
                </div>

            </div>
        </div>
    </div>
</div>

@section('inline-scripts-scheduling-modal-clinic')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#scheduling-page-clinic').on('show.bs.modal', function (e) {
            //$("#txtPostcode").val('');
        })

        $('a[href$="#modalBookingLasik"]').on("click", function () {
            $('#modal-popup').hide();
            $('#modalBookingLasik').modal();
        });


        $("#txtPostcode").keypress(function (event) {
            return /\d/.test(String.fromCharCode(event.keyCode));
        });

        $("#txtPostcode").keyup(function (event) {
            if ($("#txtPostcode").val().length == 4) {
                btnFind("", "");
            } else {
                $('.iconLoader').hide();
                $('.iconArrow').show();
                $('.divBookingLocationResults').hide();
                $('.divBookingLocationResultsNone').show();

                html = $('.divAllClinics').html();
                $('.divBookingLocationResults').html(html);
            }
        });

        $("#btnFind").click(function () {
            postcode = $('#txtPostcode').val();

            if (postcode.length == 4) {
                btnFind("", "");
            }
        });

        function btnFind(latitude, longitude) {
            $('.iconArrow').hide();
            $('.iconLoader').show();

            $.ajax({
                type: "POST",
                url: "{{ url('/booking-location') }}",
                data: {
                    'postcode': $('#txtPostcode').val(),
                    'latitude': latitude,
                    'longitude': longitude,
                },
                success: function (response) {
                    if (response.status == "success") {
                        $('.divBookingLocationResults').html("");

                        locations = response.locations;

                        html = "";
                        html += "<div class='container'>";
                        html += "<div class='row'>";

                        strClass = "col-lg-4 col-sm-12";

                        for (i = 0; i < locations.length; i++) {
                            locationName = locations[i]["name"];
                            locationName = locationName.replace("Clinic, ", "");
                            locationName = locationName.replace(" NSW", "");
                            locationName = locationName.replace(" ACT", "");

                            if (i == 3) {
                                strClass = "col-lg-3 col-sm-12 divClinic";
                                html += "<div class='col-12'>";
                                html += "<a href='#' onclick='btnLocationMore()' class='divLocationMore'>See more available clinics <i class='fa fa-chevron-right'></i></a>";
                                html += "</div>";
                            }

                            html += "<div class='" + strClass + "'>";
                            html += "<a href='#' onclick='btnClinic(" + locations[i]["id"] + ")'>";
                            html += "<div id='divLocation" + locations[i]["id"] + "' class='divLocation'>";
                            html += "<div id='divLocationName" + locations[i]["id"] + "' class='divLocationName'>" + locationName + "</div>";
                            html += "<div id='divLocationAddress" + locations[i]["id"] + "' class='divLocationAddress divHide'>" + locations[i]["location"] + "</div>";
                            //html += "<div class='divLocationMore'>View Availabilities <i class='fa fa-chevron-right'></i></div>";
                            html += "</div>";
                            html += "</a>";
                            html += "</div>";
                        }

                        html += "</div>";
                        html += "</div>";

                        $('.divBookingLocationResults').append(html);
                        $('.divBookingLocationResults').show();

                        $('.iconLoader').hide();
                        $('.iconArrow').show();
                        $('.divBookingLocationResultsNone').hide();

                        // Show closest clinic calendar
                        btnClinic(locations[0]["id"], false);
                    }
                }
            });
        }

        function btnClinic(clinicID, buttonClick = true) {

            $('.divLocation').removeClass('divBookingLocationActive');
            $('#divLocation' + clinicID).addClass('divBookingLocationActive');

            $('.scheduling-page-footer').css("visibility", "visible");
            $('.scheduling-page-footer').css("height", "42");
            resetArrow('divNoTime');
            $('.divNoTimeWrapper').hide();

            html = "<div class='divLocationName'>" + $('#divLocationName' + clinicID).html() + "</div>";
            html += "<div class='divLocationAddress'>" + $('#divLocationAddress' + clinicID).html() + "</div>";

            $('.divBookingLocation-details').html(html);
            $('.divBookingLocation-details').show();

            html = "";
            html += '<iframe src="https://personaleyes-lasik-booking.as.me/?appointmentType=10019609&calendarID=' + clinicID + '" width="100%" height="650" frameBorder="0"></iframe>';
            $('.divBookingLocation-acuity').html(html);
            //$('.divBookingLocationResults').hide();
            $('.divBookingLocation-acuity').show();

            $('#scheduling-page-clinic .hbspt-form').hide();

            if (buttonClick && $(window).width() <= 480) {
                //$('.divBookingLocationResults').hide();
                $('.divClinic').hide();
            }

            $('#hs-form-container').hide();

            $('.divNoTimeWrapper').fadeIn(5000);
        }

        btnClinic(2964801);

        function btnNoTime() {
            $('.scheduling-page-footer').css("height", "auto");

            if ($('#hs-form-container').is(":visible")) {
                $('#hs-form-container').hide();
            } else if ($('#hs-form-container').is(":hidden")) {
                $('#hs-form-container').show();
            }

            if ($("#hs-form-show").val() == "0") {

                hbspt.forms.create({
                    portalId: "5064462",
                    formId: "808dff57-6d05-4dcf-8c0f-a7a863797ce3",
                    target: "#hs-form-container"
                });
            }

            $("#hs-form-show").val('1');

            changeArrow('divNoTime');
        }

        function btnLocationMore() {
            $('.divBookingLocationResults').show();
            $('.divClinic').toggle();

            changeArrow('divLocationMore');
        }

        function changeArrow(className) {
            if ($('.' + className + ' i').hasClass('fa-chevron-right')) {
                $('.' + className + ' i').removeClass('fa-chevron-right');
                $('.' + className + ' i').addClass('fa-chevron-down');
                $('.divNoTimeForm').css("background-color", "#e6e6e6");
            } else {
                $('.' + className + ' i').removeClass('fa-chevron-down');
                $('.' + className + ' i').addClass('fa-chevron-right');
                $('.divNoTimeForm').css("background-color", "#fff");
            }
        }

        function resetArrow(className) {
            $('.' + className + ' i').removeClass('fa-chevron-down');
            $('.' + className + ' i').addClass('fa-chevron-right');
            $('.divNoTimeForm').css("background-color", "#fff");
        }

        function btnCurrentLocation() {
            $('#txtPostcode').val('');
            getLocation();
        }

        function getLocation() {
            //btnFind(-37.8109, 144.9644); // Doncaster VIC

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(usePosition);
            } else {
                alert("Sorry, geolocation is not supported by this browser.");
            }
        }

        function usePosition(position) {
            //alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
            btnFind(position.coords.latitude, position.coords.longitude);
        }

        $(".divBookingLocation-acuity").focus(function () {
            alert("Handler for .focus() called.");
        });

        function btnShowClinic() {
            $('.divClinic').css("height", "auto");
            $('.divClinic').toggle();

            changeArrow('divNoTime');
        }

    </script>

@endsection