<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

<script>
$.fn.cycle.defaults.autoSelector = '.links-carousel';
</script>

<div class="home-links">
   <div class="container">
		<div class="links-carousel" 
		    ddata-cycle-fx=carousel
		    data-cycle-carousel-visible=7
		    ddata-cycle-timeout=1
			ddata-cycle-speed=5000
			ddata-cycle-throttle-speed=true
		    ddata-cycle-easing=linear
			ddata-cycle-slides=span
			>
			
				@foreach ($home_links as $home_link)
				   <span><a href='{{ $home_link->url }}' target='_blank' title='{{ $home_link->name }}'><img src='{{ url('') }}/{{ $home_link->image }}' ></a></span>		
				@endforeach
						
		</div> 
   </div>
</div>