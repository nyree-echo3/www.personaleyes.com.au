<link rel="stylesheet" href="{{ asset('css/site/hs-form.css?v=0.1') }}">
<div class="modal fade" id="scheduling-page" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row booking-header-part">
					<div class="col-sm-8">
						<img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="scheduling-page-logo">
						<h2 class="scheduling-page-header">LASIK Assessment Online Booking</h2>
						Book your free appointment in under 2 minutes.
					</div>
					<div class="col-sm-4 d-none d-lg-block">
						<img class="img-fluid" src="{{ url('') }}/images/site/booking-emma.png" title="Secure" alt="Secure">
					</div>
				</div>
				<iframe id="scheduling-iframe" width="100%" height="920" frameBorder="0"></iframe><script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
				<div class="row scheduling-page-footer">

					<div class="col-sm-12">
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
                            hbspt.forms.create({
                                portalId: "5064462",
								formId: "808dff57-6d05-4dcf-8c0f-a7a863797ce3"
                            });
						</script>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 bookingFooterAwardsWrapper">
						<a href="https://www.google.com.au/search?source=hp&ei=5gcxXYyHI9fB9QPepo-YDQ&q=personaleyes&oq=personaleyes&gs_l=psy-ab.3..0l8j0i10j0.981.5143..5221...3.0..0.194.1856.0j12......0....1..gws-wiz.....0..0i131.JBQ53A4pF5Y&ved=2ahUKEwjH86q51r_jAhUEOSsKHXzbAykQvS4wAHoECAcQLA&uact=5&npsic=0&rflfq=1&rlha=0&rllag=-33799595,151105731,11802&tbm=lcl&rldimm=12210723549260763504&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:650661978133997922;mv:!1m2!1d-33.098168367811844!2d151.26419858984377!2m2!1d-34.716511351066714!2d149.16580991796877!4m2!1d-33.91118087282168!2d150.21500425390627!5i9" target="_blank">
							<img class="img-fluid" src="{{ url('') }}/images/site/Google-Reviews.gif" title="Google Reviews" alt="Google Reviews">
						</a>
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-celebrating-22years-bladeless-vision-correction.gif" title="Celebrating 22years Bladeless Vision Correction" alt="Celebrating 22years Bladeless Vision Correction">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-lasik.gif" title="Award - Bladeless LASIK" alt="Award - Bladeless LASIK">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-cataract.gif" title="Award - Bladeless Cataract" alt="Award - Bladeless Cataract">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-icl.gif" title="Award - ICL" alt="Award - ICL">
						<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-secure.gif" title="Secure" alt="Secure">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>