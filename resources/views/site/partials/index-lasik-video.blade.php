<div class="homeVideoLasik">
    <div class="homeVideoLasik-wrapper">
       <div class="homeVideoLasik-caption">
		   <div class="homeVideoLasik-caption-sml">
		      personalEYES - Your LASIK Specialists
		   </div>
		   Feel the freedom of 20/20 vision with LASIK eye surgery
		   <div class='homeVideoLasik-booking'>   
			  <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">Book Your Free Assessment Today</button>
		   </div>
	   </div>
    </div>        
	
    <div class="video-container-lasik">
		<div id="lasik-video-container"></div>
		<div id="chevron-down" class="chevron-down">
			<a href='#home-panel-cta'><i class="fas fa-chevron-down"></i></a>
	    </div>	    	    
	</div>
</div>