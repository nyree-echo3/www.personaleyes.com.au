<div class="home-intro">
    <div class="container">
        <h2>The personalEYES difference</h2>
        
        <div class="row">
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue1.gif" alt="Years performing eye surgery in Australia" title="Years performing eye surgery in Australia">
                <div class="home-intro-num"><span class='count' data-count='25'></span>+</div>
                <div class="home-intro-txt">Years bladeless vision correction in Australia</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue2.gif" alt="Patients" title="Patients">
                <div class="home-intro-num"><span class='count' data-count='30000'></span></div>
                <div class="home-intro-txt">Happy patients and counting</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue3.gif" alt="Of our patients are completely satisfied after their surgery" title="Of our patients are completely satisfied after their surgery">
                <div class="home-intro-num"><span class='count' data-count='98'></span>%</div>	
                <div class="home-intro-txt">Of our patients are ‘completely satisfied’ with their treatment</div>
            </div>
            
            <div class="col-lg-3 home-intro-img">
                <img src="{{ url('') }}/images/site/icon-blue4.gif" alt="Clinics in Australia" title="Clinics in Australia">
                <div class="home-intro-num"><span class='count' data-count='10'></span></div>	
                <div class="home-intro-txt">State-of-the-art clinics across NSW and ACT</div>
            </div>            
        </div>
        
        <div class='home-intro-booking'>   
			<a class="btn-booking" href='{{ url('') }}/make-a-booking'>Book an Appointment Today</a>
		</div>
    </div>
</div>