<footer class='footer'>
    <div class='footerContactsWrapper'>


        <div class='footerContacts'>
            <div class="panelNav">
                <div class="container-fluid">
                    <div class="row justify-content-center">

                        <div class="col-lg-2">
                            <div class="footer-logo">
                                @if ((isset($home_style_override) && $home_style_override) || (!isset($home_style_override) && $service_slug == ""))
                                   <a href="{{ url('') }}" title="{{ $company_name }}"><img class="img-fluid" src="{{ url('') }}/images/site/logo-bottom.gif" title="{{ $company_name }}" alt="personalEYES: Eye Specialist – Sydney’s Leaders in Laser Eye Surgery"></a>
                                @else
                                   <a href="{{ url('') }}" title="{{ $company_name }}"><img class="img-fluid" src="{{ url('') }}/images/site/logo-lasik-bottom.png" title="{{ $company_name }}" alt="personalEYES: Eye Specialist – Sydney’s Leaders in Laser Eye Surgery"></a>
                                @endif
                                
                                <div id="footer-social">
                                    <h2>Follow Us</h2>
                                    @if ( $social_facebook != "")
                                        <a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
                                    @if ( $social_instagram != "")
                                        <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
                                    @if ( $social_twitter != "")
                                        <a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif
                                    @if ( $social_youtube != "")
                                        <a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a> @endif
                                    @if ( $social_linkedin != "")
                                        <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
                                    @if ( $social_googleplus != "")
                                        <a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif
                                    @if ( $social_pinterest != "")
                                        <a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif
                                </div>

                                <!--<div class="google-review">
                                    <a href="https://www.google.com.au/search?source=hp&ei=5gcxXYyHI9fB9QPepo-YDQ&q=personaleyes&oq=personaleyes&gs_l=psy-ab.3..0l8j0i10j0.981.5143..5221...3.0..0.194.1856.0j12......0....1..gws-wiz.....0..0i131.JBQ53A4pF5Y&ved=2ahUKEwjH86q51r_jAhUEOSsKHXzbAykQvS4wAHoECAcQLA&uact=5&npsic=0&rflfq=1&rlha=0&rllag=-33799595,151105731,11802&tbm=lcl&rldimm=12210723549260763504&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:650661978133997922;mv:!1m2!1d-33.098168367811844!2d151.26419858984377!2m2!1d-34.716511351066714!2d149.16580991796877!4m2!1d-33.91118087282168!2d150.21500425390627!5i9" target="_blank">
                                        <img class="img-fluid" src="{{ url('') }}/images/site/Google-Reviews.png" title="Google Reviews" alt="Google Reviews">
                                    </a>
                                </div>
                                
                                <div class="footer-awards">
                                   <img class="img-fluid" src="{{ url('') }}/images/site/footer-icons.png" title="Awards" alt="Awards">
								</div>-->
                            </div>
                        </div>
                        @php
                            if(isset($footer_override)){
                                $navigation_footer = $footer_override;
                            }
                        @endphp

                        @if(count($navigation_footer))

                            @php
                            $counter = 0;
                            $articles_index = 0;
                            foreach($navigation_footer as $nav_key => $nav_item){
                                if($nav_item->text=='Articles'){
                                    $articles_index = $nav_key;
                                }
                            }

                            $display = false;
                            $index = $articles_index;

                            while($display == false){
                                $display = $navigation_footer{$index-1}->display;
                                $index--;
                            }
                            @endphp

                            @foreach($navigation_footer as $nav_key => $nav_item)                                

                                @if ($nav_item->display && $nav_item->text!='Articles')

                                    @if($nav_item->slug=='locations')
                                    <div class="col-lg-1">
                                    @else
                                    <div class="col-lg-2">
                                    @endif
                                        <div class="footer-txt footer-menu-top {{ ($counter > 0 ? "footer-menu-top-gap" : "") }}">
                                            <a href="{{ url('') }}/{{ $nav_item->href }}">{{ $nav_item->text }}</a>
                                        </div>

                                        @if (isset($nav_item->children))

                                            @foreach($nav_item->children as $sub_key => $sub_item)

                                                @if ($sub_item->display)
                                                    <div class="footer-txt footer-menu">
                                                        @if ($sub_item->type == 'manual')
                                                           <a href="{{ $sub_item->href }}">{{ $sub_item->text }}</a>
                                                        @else
                                                           <a href="{{ url('') }}/{{ $sub_item->href }}">{{ $sub_item->text }}</a>
                                                        @endif
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif

                                        @php
                                        if($nav_key == $index){
                                        @endphp
                                        <div class="footer-txt footer-menu-top footer-article {{ ($counter > 0 ? "footer-menu-top-gap" : "") }}">
                                            <a href="{{ url('news') }}">Articles</a>
                                        </div>
                                        @php
                                        }
                                        @endphp
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            
            <div class="footerAwardsWrapper">
                <a href="https://www.google.com.au/search?source=hp&ei=5gcxXYyHI9fB9QPepo-YDQ&q=personaleyes&oq=personaleyes&gs_l=psy-ab.3..0l8j0i10j0.981.5143..5221...3.0..0.194.1856.0j12......0....1..gws-wiz.....0..0i131.JBQ53A4pF5Y&ved=2ahUKEwjH86q51r_jAhUEOSsKHXzbAykQvS4wAHoECAcQLA&uact=5&npsic=0&rflfq=1&rlha=0&rllag=-33799595,151105731,11802&tbm=lcl&rldimm=12210723549260763504&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:2#rlfi=hd:;si:650661978133997922;mv:!1m2!1d-33.098168367811844!2d151.26419858984377!2m2!1d-34.716511351066714!2d149.16580991796877!4m2!1d-33.91118087282168!2d150.21500425390627!5i9" target="_blank">
					<img class="img-fluid" src="{{ url('') }}/images/site/Google-Reviews.gif" title="Google Reviews" alt="Google Reviews">
				</a>
                                    
            	<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-celebrating-22years-bladeless-vision-correction.gif" title="Celebrating 22years Bladeless Vision Correction" alt="Celebrating 22years Bladeless Vision Correction">
            	<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-lasik.gif" title="Award - Bladeless LASIK" alt="Award - Bladeless LASIK">
            	<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-bladeless-cataract.gif" title="Award - Bladeless Cataract" alt="Award - Bladeless Cataract">
            	<img class="img-fluid" src="{{ url('') }}/images/site/footer-icon-icl.gif" title="Award - ICL" alt="Award - ICL">
            </div>
        </div>

        <!--<div class="footerContactsWrapperImg"></div></div>-->

        <div class='footerContainer'>
            <div id="footer-txt-bot">
                @if ( $company_name != "")
                    <a href="https://www.personaleyes.com.au/" target="_blank">&copy; {{ date('Y') }} personalEYES </a> | @endif
                <a href="{{ url('') }}/privacy" rel=”nofollow”>Privacy</a> | <a href="{{ url('') }}/terms-conditions" rel=”nofollow”>Terms</a> |
                <a href="{{ url('') }}/contact">Contact</a> | <a href="{{ url('') }}/site-map">Site Map</a> |
                <a href="https://www.personaleyes.com.au/coronavirus-update">Coronavirus Update</a> |
                <a href="https://www.echo3.com.au" target="_blank" rel=”nofollow”>Website by Echo3</a>

            </div>
        </div>
</footer>