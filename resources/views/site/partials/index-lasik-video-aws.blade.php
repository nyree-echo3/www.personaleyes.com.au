<div class="homeVideoLasik">
    <div class="homeVideoLasik-wrapper">
       <div class="homeVideoLasik-caption">		   
		   <div class='homeLasikOffers'>   
			  <img src="{{ url('') }}/images/site/PE-spring2021-overlay-306x308px.png" title="Special Offers" alt="Special Offers">
		   </div>
		   Enjoy $500 off our Premium Package
		   
		   <div class='homeVideoLasik-booking'>
			  <!--<a class="acuity-embed-button btn-booking video-booking-button" href="{{ url('throw-away-your-glasses-for-good') }}">Get My Voucher Code</a>-->
			  <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">Book Your Free Assessment Today</button>
		   </div>		   
		   
		   <div class="homeVideoLasik-caption-sml">
		      + Limited time offer. Conditions <a href="https://www.personaleyes.com.au/special-offer">apply</a>.
		   </div>
	   </div>
    </div>        
	
    <div class="video-container-lasik">

		<video id="lasik-vid" preload="auto" autoplay muted loop poster="{{ asset('images/site/poster.jpg') }}">
			<source src="https://personaleyes.s3-ap-southeast-2.amazonaws.com/PersonalEYES.mp4" type="video/mp4">
			<p>This browser does not support the video element.</p>
		</video>

		<div id="chevron-down" class="chevron-down">
			<a href='#home-panel-cta'><i class="fas fa-chevron-down"></i></a>
	    </div>	    	    
	</div>
</div>