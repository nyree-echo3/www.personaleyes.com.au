 <div class="home-faqs">
	 <div class="container home-faqs-divider">
        <a href="{{ url('') }}/faqs/lasik-faqs">
           <img src= "{{ url('') }}/images/site/icon-faq.png" alt="Icon FAQS" title="Icon FAQS">
           <h2>FAQs</h2>
        </a>
        
      <div class="row">         
         <div class="col-lg-12">
            
            
            @php
               $counter = 0;
               $halfway = 0;
               if (isset($home_pages)) {               
                  $halfway = round(count($home_pages) / 2);
               }             
            @endphp
            
            
            <div class="container">
			  <div class="row">
					<div class="col-lg-12">
						<ul class="home-faq-list">            
							@foreach($home_faqs as $home_faq)                							
							   <li><a class="accordion-toggle" data-toggle="collapse" href="#faqitem{{ $loop->iteration }}">{{$home_faq->title}}</a></li>		
							   
							   <div id="faqitem{{ $loop->iteration }}" class="panel-collapse collapse in"> {!! $home_faq->description !!} </div> 							  			      					   
							@endforeach
						</ul>               
					</div>
			  </div>
			</div>
		  
		  
		 </div>
                 
      </div>
   </div>

</div>
