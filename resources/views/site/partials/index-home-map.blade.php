 <div class="home-map">
	 <div class="container no-gutters">
        <div class="row no-gutters">
           <div class="col-lg-7">
              <img src="{{ url('') }}/images/site/map.gif" title="Locations Map" alt="Locations Map">
		   </div>
		   <div class="col-lg-5">
              <div class="home-map-txt">
				  <h2>Come visit us</h2>
				  <p>With our unwavering commitment to your safety & a unique personalised care approach, personalEYES has become one of the most experienced corrective eye surgery groups in Australasia.</p>
				  <p>We have 10 convenient locations in Australia.</p>
				  <a href="{{ url('locations') }}">Find Out More</a>
              </div>
           </div>
        </div>
   </div>
</div>
