<div class="home-intro1">
    <div class="container">
        <div class="row">            
            <div class="col-lg-12">
                <div class="home-intro-lasik">
					<?php echo $home_intro_text; ?>

					 <div class='home-intro-booking'>   
						<button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page">Book a Free Assessment Today </button>
					 </div>
				 </div>
            </div>
        </div>                                                
    </div>
</div>

<div class="home-doctors-mobile">
   <img src="{{ url('') }}/images/site/drs-bala-meades-mobile.png" alt="A/Prof Chandra Bala & Dr Kerrie Meades" title="A/Prof Chandra Bala & Dr Kerrie Meades">
</div> 