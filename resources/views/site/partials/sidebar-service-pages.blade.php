<div class="col-xl-2 col-lg-2 col-md-12 blog-sidebar">
    <div class="sidebar-module">
        @if($side_nav_parent_text)
            <h4>{{ $side_nav_parent_text }}</h4>
        @endif
        <ol class="list-group list-unstyled list-group-flush">
            {!! $side_nav !!}
        </ol>
    </div>
</div>