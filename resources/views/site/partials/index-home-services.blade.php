<div class="home-services">
   <h2>Eye Treatment Options</h2>
   
   <div class="container">
	  <div class="row">   
	      <div class="col-lg-1">	          
		  </div><!-- /.col-lg-1 -->
		        		   	 
		  <div class="col-lg-2 home-panel-cta-box">	            
		       <div class="home-panel-cta-box">
	               <a href="{{ url('lasik') }}" >
					   <div class="home-panel-cta-box-img">
						  <img src="{{ url('') }}/images/site/h-icon1.gif" alt="LASIK" />
					   </div>
				   
					   <div class="home-panel-cta-box-txt">					  
						   <h2>LASIK</h2>	
						   <!--<p>Throw away bothersome lenses forever with this quick and permanent life-changing procedure.</p> -->
					   </div>
				   </a>
                   <!--<div class="btn-cta btn-cta-mobile"><a href="{{ url('lasik') }}" >Tell Me More</a></div>-->
               </div>			  
		  </div><!-- /.col-lg-2 -->	
		  
		  <div class="col-lg-2 home-panel-cta-box">
	           <div class="home-panel-cta-box">
                   <a href="{{ url('cataracts') }}" >
					   <div class="home-panel-cta-box-img">
						  <img src="{{ url('') }}/images/site/h-icon3.gif" alt="Cataracts" />
					   </div>
					   <div class="home-panel-cta-box-txt">					   
						   <h2>Cataracts</h2>		
						   <!-- <p>Cloudy, blurry or double vision? Cataract surgery can restore vision and often reduce dependency on lenses.</p> -->			      			   			   				   
					   </div>
				   </a>
                   <!-- <div class="btn-cta btn-cta-mobile"><a href="{{ url('cataracts') }}" >Find Out More</a></div> -->
               </div>
		  </div><!-- /.col-lg-2 -->			
		
		   <div class="col-lg-2 home-panel-cta-box">
	           <div class="home-panel-cta-box">
                   <a href="{{ url('other-services/retina-treatment') }}" >
					   <div class="home-panel-cta-box-img">
						  <img src="{{ url('') }}/images/site/icon3-retinaservices.gif" alt="Retinal" />
					   </div>
					   <div class="home-panel-cta-box-txt">					   
						   <h2>Retinal</h2>
						   <!-- <p>Flashes, floaters, or distorted vision? We can help. Come talk to us about retinal disease treatments.</p> -->					    
					   </div>
				   </a>
                   <!-- <div class="btn-cta btn-cta-mobile"><a href="{{ url('other-services/retina-treatment') }}" >Read More</a></div> -->
               </div>             			   			   							   
		  </div><!-- /.col-lg-2 -->				  				  
		  
		  <div class="col-lg-2 home-panel-cta-box">
	           <div class="home-panel-cta-box">
                   <a href="{{ url('vision/glaucoma') }}" >
					   <div class="home-panel-cta-box-img">
						  <img src="{{ url('') }}/images/site/icon4-glaucoma.gif" alt="Glaucoma" />
					   </div>
					   <div class="home-panel-cta-box-txt">					   
						   <h2>Glaucoma</h2>	
						   <!-- <p>We offer a range of comprehensive glaucoma treatments that can help stop the progression of the disease.</p>	-->			      			   			   				   
					   </div>
				   </a>
                   <!-- <div class="btn-cta btn-cta-mobile"><a href="{{ url('other-services/glaucoma-surgery') }}" >Find Out More</a></div> -->
               </div>
		  </div><!-- /.col-lg-2 -->
		  
		  <div class="col-lg-2 home-panel-cta-box">
	           <div class="home-panel-cta-box">
                   <a href="{{ url('other-services/paediatric-ophthalmology') }}" >
					   <div class="home-panel-cta-box-img">
						  <img src="{{ url('') }}/images/site/icon5-pediatric.gif" alt="Pediatric" />
					   </div>
					   <div class="home-panel-cta-box-txt">					   
						   <h2>Pediatric</h2>	
						   <!-- <p>We offer a range of comprehensive pediatric treatments.</p> -->				      			   			   				   
					   </div>
				   </a>
                   <!-- <div class="btn-cta btn-cta-mobile"><a href="{{ url('other-services/paediatric-ophthalmology') }}" >Find Out More</a></div> -->
               </div>
		  </div><!-- /.col-lg-2 -->
		  
		  <div class="col-lg-1">	          
		  </div><!-- /.col-lg-1 -->
		  
		  <div class="col-lg-1">	          
		 </div><!-- /.col-lg-1 -->		  		  
		  
		  <!-- <div class="col-lg-2 home-panel-cta-box">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('lasik') }}" >Tell Me More</a></div>
			 </div>
		  </div>
		  
		  <div class="col-lg-2 home-panel-cta-box">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="{{ url('cataracts') }}" >Find Out More</a></div>
			 </div>
		  </div>	
		  
		  <div class="col-lg-2 home-panel-cta-box">
	  	     <div class="home-panel-cta-box">
		  	    <div class="btn-cta btn-cta-full"><a href="{{ url('other-services/retina-treatment') }}" >Read More</a></div>
			 </div>
		  </div>		  		  
	  				
	  	  <div class="col-lg-2 home-panel-cta-box">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="{{ url('other-services/glaucoma-surgery') }}" >Find Out More</a></div>
			 </div>
		  </div>		
	  				
	  	  <div class="col-lg-2 home-panel-cta-box">
	         <div class="home-panel-cta-box">
		        <div class="btn-cta btn-cta-full"><a href="{{ url('other-services/paediatric-ophthalmology') }}" >Find Out More</a></div>
			 </div>
		  </div>
	  				
	  	  <div class="col-lg-1">	          
		  </div>															
		  -->			
	   </div>
   </div>  
   
   <div class="btn-cta btn-cta-services"><a href="{{ url('services') }}" >Looking for other eye treatment options?</a></div>
</div>
