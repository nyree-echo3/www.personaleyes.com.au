<div class="coronavirusUpdates">	   
	<div>
		<i class="fas fa-exclamation-circle"></i> <strong>SPECIAL ANNOUNCEMENT:</strong> We continue to be at your service throughout the lockdown, and strive to keep our clinics safe for all our patients. All patient-facing staff are fully-vaccinated & tested weekly. We observe all COVID-safe measures implemented by NSW Health.
	</div>
</div>