<div class="panel-videos">        
    <div class="container">
        <h3>More Videos</h3>
        
        <div class="row">             
            <div class="col-lg-1">
			</div>
                      
            <div class="col-lg-2 col-sm-4 col-6 panel-videos-item">
                <a data-toggle="modal" href="#modalMeetDrBala">
                    <div class="panel-video">                        
                        <img src="{{ url('') }}/images/site/video-small-Meet_A_Prof_Chandra_Bala.jpg" alt="Video" title="Video">
                        <div class="panel-video-txt">Meet A/Prof Chandra Bala</div>
                    </div>
                </a>
            </div>

            <div class="col-lg-2 col-sm-4 col-6 panel-videos-item">
                <a data-toggle="modal" href="#modalPersonalEYESDifference">
                    <div class="panel-video">                       
                        <img src="{{ url('') }}/images/site/video-small-What_to_expect_when_you_choose_personalEYES.jpg" alt="Video" title="Video">
                        <div class="panel-video-txt">What to expect when you choose personalEYES</div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-2 col-sm-4 col-6 panel-videos-item">
                <a data-toggle="modal" href="#modalLASIKWow">
                    <div class="panel-video">                        
                        <img src="{{ url('') }}/images/site/video-small-The_LASIK_WOW!_ Why_should_you_consider_having_LASIK.jpg" alt="Video" title="Video">
                        <div class="panel-video-txt">Why should you consider having LASIK </div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-2 col-sm-4 col-6 panel-videos-item">
                <a data-toggle="modal" href="#modalLASIKTechnology">
                    <div class="panel-video">                        
                        <img src="{{ url('') }}/images/site/video-small-LASIK_Technology.jpg" alt="Video" title="Video">
                        <div class="panel-video-txt">LASIK Technology</div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-2 col-sm-4 col-6 panel-videos-item">
                <a data-toggle="modal" href="#modalMeetDrKerrieMeades">
                    <div class="panel-video">                       
                        <img src="{{ url('') }}/images/site/video-small-Meet_Dr_Kerrie_Meades.jpg" alt="Video" title="Video">
                        <div class="panel-video-txt">Meet Dr Kerrie Meades </div>
                    </div>
                </a>
            </div>
            
            <div class="col-lg-1">
			</div>                    

        </div>
    </div>
</div>

<!-- The Modal - Meet Dr Bala -->
<div class="modal fade" id="modalMeetDrBala">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9">                   
                    <iframe id="MeetDrBalaIframe" class="embed-responsive-item"  src="https://player.vimeo.com/video/386370308" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- The Modal - The personalEYES Difference -->
<div class="modal fade" id="modalPersonalEYESDifference">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9">                    
                    <iframe id="PersonalEYESDifferenceIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379880469" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- The Modal - The LASIK WOW -->
<div class="modal fade" id="modalLASIKWow">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9">                    
                    <iframe id="LASIKWowIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379882800" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- The Modal - LASIK Technology -->
<div class="modal fade" id="modalLASIKTechnology">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9">                    
                    <iframe id="LASIKTechnologyIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379883993" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- The Modal - Meet Dr Kerrie Meades -->
<div class="modal fade" id="modalMeetDrKerrieMeades">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9">                    
                    <iframe id="MeetDrKerrieMeadesIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379884779" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                </div>
            </div>

        </div>
    </div>
</div>

@section('inline-scripts-videos')
    <script language="javascript">

        var meet_dr_bala_player = new Vimeo.Player(document.getElementById('MeetDrBalaIframe'));

        $('#modalMeetDrBala').on('shown.bs.modal', function () {
            meet_dr_bala_player.play();
        });
        $('#modalMeetDrBala').on('hidden.bs.modal', function () {
            meet_dr_bala_player.pause();
        });
		

        var personalEYESDifferenceIframe_player = new Vimeo.Player(document.getElementById('PersonalEYESDifferenceIframe'));

        $('#modalPersonalEYESDifference').on('shown.bs.modal', function () {
            personalEYESDifferenceIframe_player.play();
        })
        $('#modalPersonalEYESDifference').on('hidden.bs.modal', function () {
            personalEYESDifferenceIframe_player.pause();
        })
		
		
		var LASIKWowIframe_player = new Vimeo.Player(document.getElementById('LASIKWowIframe'));

        $('#modalLASIKWow').on('shown.bs.modal', function () {
            LASIKWowIframe_player.play();
        })
        $('#modalLASIKWow').on('hidden.bs.modal', function () {
            LASIKWowIframe_player.pause();
        })
		
				
		var LASIKTechnologyIframe_player = new Vimeo.Player(document.getElementById('LASIKTechnologyIframe'));

        $('#modalLASIKTechnology').on('shown.bs.modal', function () {
            LASIKTechnologyIframe_player.play();
        })
        $('#modalLASIKTechnology').on('hidden.bs.modal', function () {
            LASIKTechnologyIframe_player.pause();
        })
		
				
		var MeetDrKerrieMeadesIframe_player = new Vimeo.Player(document.getElementById('MeetDrKerrieMeadesIframe'));

        $('#modalMeetDrKerrieMeades').on('shown.bs.modal', function () {
            MeetDrKerrieMeadesIframe_player.play();
        })
        $('#modalMeetDrKerrieMeades').on('hidden.bs.modal', function () {
            MeetDrKerrieMeadesIframe_player.pause();
        })
		
    </script>
@endsection