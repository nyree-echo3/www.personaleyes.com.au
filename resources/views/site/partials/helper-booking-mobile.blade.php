@php
   $service_slug = Cookie::get('services_slug');       
@endphp
  
<div class='navbar-booking-mobile'>   
   @if ((isset($home_style_override) && $home_style_override) || (!isset($home_style_override) && $service_slug == ""))
       <a href="{{url('') }}/make-a-booking" target="_blank" class="acuity-embed-button btn-booking-mobile" >Book Now</a>                
   @else
       <a href="https://personaleyes-lasik-booking.as.me/schedule.php" target="_blank" class="acuity-embed-button btn-booking-mobile" >Book Now</a>       
   @endif
    
   <a class="contact-icon" href="{{ url('contact') }}"><i class="fas fa-envelope"></i></a>
   <a id="phone-number-mobile" class="phone-icon" href='tel:{{ str_replace(' ', '', $phone_number) }}' class="btn-booking"><i class="fas fa-phone-alt"></i></a>
</div>