<div class="col-xl-2 col-lg-2 col-md-12 blog-sidebar">
	<div class="sidebar-module">
		<h4>{{ $category_name }}</h4>
		<ol class="list-group list-unstyled list-group-flush">
			@foreach ($side_nav as $item)
			   <li><a href="{{ url('') }}/{{ $item["url"] }}">{{ $item["title"] }}</a></li>
			@endforeach
			<li><a href="{{ url('') }}/general-booking-page/ophthalmology-referral">Refer a Patient</a></li>
		</ol>
	</div>
</div>