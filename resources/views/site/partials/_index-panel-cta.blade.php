<div class="home-panel-cta">
   <div class="home-panel-cta-inner">
	  <div class="row">         		   	 
		  <div class="col-lg-4 home-panel-cta-box">	           
		       <div class="home-panel-cta-box1">
		           <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/panel1.jpg" alt="The ultimate Australian guide to Lasik" />			   
				   </div>
				   <div class="home-panel-cta-box-txt">
					   <h2>Get Started Here</h2>
					   <h3>The ultimate Australian guide to Lasik</h3>					   
                   </div>
                   <div class="btn-cta"><a href="{{ url('') }}" >Read More</a></div>               			   			   							   
               </div>			  
		  </div><!-- /.col-lg-4 -->				
		
		   <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box2">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/panel2.jpg" alt="Take the Australian Online Eye Test" />
				   </div>
				   <div class="home-panel-cta-box-txt">
					   <h2>How's your eyesight?</h2>
					   <h3>Take the Australian Online Eye Test</h3>					    
				   </div>
                   <div class="btn-cta"><a href="{{ url('pages/calculators/australian-online-eye-test') }}" >Take The Test</a></div>
               </div>             			   			   							   
		  </div><!-- /.col-lg-4 -->				  
		
		  <div class="col-lg-4 home-panel-cta-box">
	           <div class="home-panel-cta-box3">
	               <div class="home-panel-cta-box-img">
				      <img src="{{ url('') }}/images/site/panel3.jpg" alt="Calculate how much you could save with Lasik" />			
				   </div>
				   <div class="home-panel-cta-box-txt">
					   <h2>Save with Lasik</h2>
					   <h3>Calculate how much you could save with Lasik</h3>					      			   			   				   
                   </div>
                   <div class="btn-cta"><a href="{{ url('savings-calculator') }}" >Calculate Now</a></div>
               </div>
		  </div><!-- /.col-lg-4 -->		
	   </div>
   </div>
</div>
