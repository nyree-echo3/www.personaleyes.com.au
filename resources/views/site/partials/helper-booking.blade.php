@php
     $service_slug = Cookie::get('services_slug');       
  @endphp
  
@if ((isset($home_style_override) && $home_style_override) || (!isset($home_style_override) && $service_slug == ""))
	<div class='navbar-booking'>   
		<button class="btn-booking" onclick="window.location.href='{{url('') }}/make-a-booking'">Book An Appointment </button>
	</div>
@else
    <div class='navbar-booking'>   
		<button class="acuity-embed-button btn-booking header-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">Book Free Assessment </button>
	</div>	
@endif