<?php
// Set Meta Tags
$meta_title_inner = $page->meta_title;
$meta_keywords_inner = $page->meta_keywords;
$meta_description_inner = $page->meta_description;
?>

@extends('site/layouts/app')

@if($page->slug=='savings-calculator')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/calculator-tab.css') }}">
    <link rel="stylesheet" href="{{ asset('components/rangeslider.js/dist/rangeslider.css') }}">
@endsection
@endif

@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @if ($side_nav)
                    @if($sidebar_mode=='array')
                        @include('site/partials/sidebar-pages')
                    @endif

                    @if($sidebar_mode=='prepared')
                        @include('site/partials/sidebar-service-pages')
                    @endif
                @endif
                
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        @if (isset($page))
                            <h1 class="blog-post-title">{{$page->title}}</h1>
                            {!! $page->body !!}
                        @endif
                    </div><!-- /.blog-post -->
                    @if($page->slug=='savings-calculator')
                        @include('site/savings-calculator/calculator')
                    @endif
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
    @include('site/partials/online-eye-test-modal') 
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
@endsection

@if($page->slug=='savings-calculator')
@section('scripts')
    <script src="{{ asset('components/rangeslider.js/dist/rangeslider.js') }}"></script>
    <script src="{{ asset('js/site/savings-calculator.js') }}"></script>
@endsection
@endif

@section('inline-scripts')
    <script type="text/javascript">
        $('#online-eye-test').modal('handleUpdate');

        @if($page->slug=='events')
        window.addEventListener('message', messagesHandler);

        function popup_params(width, height) {
            var a = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft;
            var i = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop;
            var g = typeof window.outerWidth!='undefined' ? window.outerWidth : document.documentElement.clientWidth;
            var f = typeof window.outerHeight != 'undefined' ? window.outerHeight: (document.documentElement.clientHeight - 22);
            var h = (a < 0) ? window.screen.width + a : a;
            var left = parseInt(h + ((g - width) / 2), 10);
            var top = parseInt(i + ((f-height) / 2.5), 10);
            return 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top + ',scrollbars=1';
        }

        function validURL(url) {
            if (url.indexOf("http://") == 0 || url.indexOf("https://") == 0) {
                return true;
            }
            return false;
        }

        function messagesHandler(ev) {

            if(ev.origin=='https://events.personaleyes.com.au'){
                if(ev.data=='scrolltop'){
                    scrollTo(0, 0);
                }else if(validURL(ev.data)){
                    window.open(ev.data, "PersonalEYES", popup_params(640, 800));
                }
            }
        }
        @endif
    </script>
@endsection