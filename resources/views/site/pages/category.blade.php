<?php
// Set Meta Tags
if ($category->meta_title != "") {
   $meta_title_inner = $category->meta_title;
}
if ($category->meta_keywords != "") {
   $meta_keywords_inner = $category->meta_keywords;
}
if ($category->meta_description != "") {
   $meta_description_inner = $category->meta_description;
}
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @if ($side_nav)

                    @if($sidebar_mode=='array')
                        @include('site/partials/sidebar-pages')
                    @endif

                    @if($sidebar_mode=='prepared')
                        @include('site/partials/sidebar-service-pages')
                    @endif
                @endif
                
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        @if (isset($category))
                            <h1 class="blog-post-title">{{$category->name}}</h1>
                            {!! $category->body !!}
                        @endif
                    </div><!-- /.blog-post -->                    
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
    
    @if (isset($page) && $page->popup_type != "")    
        @include('site/partials/popup') 
    @endif
@endsection
