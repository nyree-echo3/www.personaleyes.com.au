@php
// Set Meta Tags
if ($category->meta_title != "") {
   $meta_title_inner = $category->meta_title;
}
if ($category->meta_keywords != "") {
   $meta_keywords_inner = $category->meta_keywords;
}
if ($category->meta_description != "") {
   $meta_description_inner = $category->meta_description;
}
@endphp

@extends('site.layouts.app')
@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">
             
                    <div class="blog-post">
                       <h1>{{ $side_nav_parent_text }}</h1>
                        {!! $category->body !!}
                       <div class="services-nav">
                          <ul>
                             @php                             
                                $side_nav = str_replace('list-group-item">', 'list-group-item"><h2>',$side_nav);
							    $side_nav = str_replace("</li>", "</h2></li>",$side_nav);
                             
                                $side_nav = str_replace("list-group-item", "list-group-item", $side_nav);
                                $side_nav = str_replace('list-group-item">', 'list-group-item">',$side_nav);
                             @endphp
                            
                             {!! $side_nav !!} 
						  </ul>                       
                       </div>
                    </div><!-- /.blog-post -->                    
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->    
@endsection
