@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid" id="cont">
        <div class="row justify-content-center">
            <div class="col-12 test-container email-result">                
                
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->               
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>					
                    hbspt.forms.create({
                        portalId: "5064462",
                        formId: "9689edf8-c536-4b60-bfe0-1d11a8d7180d",
						onFormReady: function($form, ctx){		
							@if (session()->get('countryHubSpotForm')=='Australia')
							    $('.hbspt-form').show();		
							@else
								$('#hsForm_9689edf8-c536-4b60-bfe0-1d11a8d7180d').hide();	
								$('.hbspt-form').append("<p style='text-align:center;'>Sorry we only email results if you are located in Australia.</p>");
							    $('.hbspt-form').append("<p style='text-align:center;'><a href='{{ url('online-eye-test/direct-result') }}' class='result-button'>View results</a></p>");
 							    $('.hbspt-form').show();
							@endif						
						},
                        onFormSubmit: function ($form) {

							$('.divSuitabilityQuiz').delay(1000).fadeIn();
							
                            var formData = $form.serialize();
                            $.ajax({
                                type: 'POST',
                                url: '{{ url('online-eye-test/send-result') }}',
                                data: formData
                            });
                        }
                    });
                </script>  
                
                <div class="divSuitabilityQuiz">
                   <div class="cta-suitability-quiz"><a href="{{ url('') }}/suitability-quiz">Take The Test</a></div>                 	
                </div>   
            </div>
        </div>
    </div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
			$('#oet-iframe', window.parent.document).height('700px');									            
        });
    </script>
@endsection