@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid" id="cont">
        <div class="row justify-content-center">
            <div class="col-12 test-container email-result">
                Use the form below to tell us what you think about the online eye test, its features, and any improvements you would like to see.
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        portalId: "5064462",
                        formId: "49a9a34b-cd9d-4c82-8061-db3c0ccbc1ee",
                        cssClass: 'feedback-from'
                    });
                </script>
            </div>
        </div>
    </div>
@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#oet-iframe', window.parent.document).height('750px');
        });
    </script>
@endsection