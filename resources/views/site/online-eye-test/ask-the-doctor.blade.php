@if(Session::get('country')=='Australia')
<div class="row no-gutters justify-content-center ask-the-doctor-band">
    <div class="atd-text-container">
       <p>Worried about your eyesight?</p>
       <p><a href='{{ url('') }}'>Visit personalEYES – Australia’s trusted eye specialists</a></p>        
    </div>
    <div class="atd-image-container d-none d-sm-block">
        <img src="{{ asset('images/site/online-eye-test-emma.png') }}" alt="Ask the Doctor">
    </div>
    <div id="ask-the-doctor-container">
        <div id="atd-close">
            <i class="fas fa-times"></i>
            Close
        </div>
		<p>Worried about your eyesight?<br><a href='{{ url('') }}'>Visit personalEYES – Australia’s trusted eye specialists</a></p>        
    </div>
</div>
@endif