@extends('site/layouts/online-eye-test')

@section('content')
    <div class="container-fluid" id="cont">
        <div class="row justify-content-center">
            <div class="col-12 test-container email-result">
                <p>&nbsp{{ session()->get('counter') }}</p>
            </div>
        </div>
    </div>
@endsection