<?php 
   // Set Meta Tags
   $meta_title_inner = "Resources | personalEYES"; 
   $meta_keywords_inner = "Resources | personalEYES"; 
   $meta_description_inner = "Welcome to the personalEYES resource centre where you will find news, research, whitepapers, case studies, interviews, new technology & eye health information."; 
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">                
                            
                    <div class="blog-post"> 
                       
                       <div class="container news-list">
                            <h1>Articles</h1>
                                                       
                            <div class="news-categories">
                                <div class="container no-gutters">
                                    <div class="row">
										@foreach($side_nav as $item) 									   
										   <div class="col-3 news-categories-col">
										      <div class="news-categories-a {{ ($category_slug != "" && $category_slug == $item->slug ? "news-categories-active" : "") }}">
											     <a href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a>
											  </div>
										   </div>                            	       
										@endforeach
									</div>
								</div>
                            </div>
                            
							<div class="card-columns">

								    @foreach($items as $item)    
										<div class="card">										
											<div class="card-body">
											<div class="panel-news-item">	
												<a  class="projects-more"  href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
												    @if ($item->thumbnail != "")											   
														<div class="div-img">
														   <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
														</div>			
													@endif	                                    

													<div class="panel-news-item-title"><h2>{{$item->title}}</h2></div>
													<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

													<div class="panel-news-item-readmore">Read More ></div>													                                               
												</a>
												  </div>  
											</div>



										</div>
									@endforeach   
									
							</div><!-- .card-columns -->
						</div><!--  .container news-list -->
                        

					</div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- blog-masthead -->
@endsection

