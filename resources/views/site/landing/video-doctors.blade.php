@php
	$meta_robots = "noindex";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@include('site/partials/index-doctors')

<div class='home-intro-booking text-center'>   
	<button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page-clinic">Book a Free Assessment Today </button>
</div>	   

@include('site/partials/index-lasik')  				
@include('site/partials/index-guarantee')

@endsection

@section('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
@endsection