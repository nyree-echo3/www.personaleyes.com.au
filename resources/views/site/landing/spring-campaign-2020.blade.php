@php
    $meta_title_inner = "Spring Campaign 2020 | personalEYES";
	$meta_keywords_inner = "Spring Campaign 2020";
	$meta_description_inner = "Spring Campaign 2020";
	
	$meta_robots = "noindex";
	$landing_page = true;
@endphp

@extends('site/layouts/app')

@section('content')

<div class="carousel carousel-spring">
    <div class="carousel-inner">        
            <div class="carousel-item active">          
                <img class="slide-img slide" src="{{ url('') }}/images/site/slider-springcampaign2020-v2.jpg" alt="Spring Campaign 2020"> 
                <img class="slide-img-resp slide" src="{{ url('') }}/images/site/slider-springcampaign2020-v2-mobile.jpg" alt="Spring Campaign 2020">               

                
                    <div class="row align-items-center h-100 carousel-caption">
                        <div class="carousel-caption-inner justify-content-center"> 
                                <div class="carousel-caption-logo">                          
                                   <img src="{{ url('') }}/images/site/logo-lasik.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-lg">
							    </div>
                               
                                <div class="carousel-caption-h1">Tired of glasses or contacts slowing you down? </div>                           
                                <div class="carousel-caption-h2">
                                   Well this Spring, we’re giving away $40 Rebel vouchers with every LASIK assessment.
                                   So, why not book your free appointment to find out whether laser eye surgery is right for you.
							    </div>
                            
                                <div class="carousel-caption-cta">
                                    <div class="carousel-caption-cta-dbl">                                       
                                        <button class="carousel-caption-cta-1" href="" data-toggle="modal" data-target="#scheduling-page-clinic">Book Your FREE Assessment</button>
                                    </div>                                    
                                </div>   
                                
                                <div class="spring-campaign-terms"><a href="{{ url('') }}/competition-and-giveaway-terms" target="_blank">Giveaway Terms</a></div>                                
                        </div>
                    </div>

               
            </div>  
            
               
    </div>
</div>

<div id="div-spring-campaign">
   <div class="container-fluid">
	   <div class="row align-items-center h-100">
		   <div class="col-lg-1"></div>
		   
		   <div class="col-lg-3 img-dr">
		      <img src="{{ url('') }}/images/site/dr meades1.jpg" alt="Dr Kerrie Meades" title="Dr Kerrie Meades" class="img-fluid">
		      
		      <div class="btn-video">
	             <a data-toggle="modal" href="#modalLASIKWow">
		            <img src="{{ url('') }}/images/site/bttn-meetourdoctors.png" alt="Video Button" title="Video Button">
				 </a>
		      </div>
		   </div>

		   <div class="col-lg-6 p-4 justify-content-center">
			    <h2>Hi there. I’m Dr Kerrie Meades Eye Surgeon & founder of personalEYES.</h2>
			    <p>If you struggle with glasses or lenses, you might have wondered if LASIK could help you.</p> 
				<p>Since every pair of eyes is unique, LASIK isn’t suitable for everyone. Typically we say no to 1 in 5 patients because our patient's safety is our number 1 priority. I am proud to say that 98% of our patients report being ‘completely satisfied’ with their results - many even achieving better than 20/20 vision.</p>
			    <p>To know if you could have the same success, I invite you to have your eyes assessed by one of our friendly optometrists, located across our 10 clinics in NSW.</p>
			    <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">Book Your Free Assessment</button>
		   </div>
	   </div>
   </div>
</div>	

@include('site/partials/landing/lasik-steps')
@include('site/partials/landing/doctors-spring-campaign')
@include('site/partials/landing/awards')

<div class="spring-campaign-book">   
   <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">
      Book Your Free Assessment 
      <img src="{{ url('') }}/images/site/icon-cursor.gif" alt="Cursor" title="Cursor" class="img-cursor">
   </button>   
</div>	

<div class="spring-campaign-map"> 
   @include('site/partials/landing/map')
</div>

<div class="spring-campaign-testimonials">
   @include('site/partials/landing/testimonials')
</div>

<div class="spring-campaign-book-block">
    <div class="container">
       <div class="row">
		   <div class="col-12">
				<div class="spring-campaign-book-txt">Find out if LASIK could work for you</div>

				<div class="spring-campaign-book">     
				   <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">
					  Book Your Free Assessment
					  <img src="{{ url('') }}/images/site/icon-cursor.gif" alt="Cursor" title="Cursor">
				   </button>
				</div>
		    </div>
		</div>
	</div>	
</div>

@include('site/partials/index-guarantee-spring-campaign')

<!-- The Modal - The LASIK WOW -->
<div class="modal fade" id="modalLASIKWow">
    <div class="modal-dialog modal-dialog-centered modal-video">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <div class="embed-responsive embed-responsive-16by9" id="LASIKWowIframe-container"></div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
        
    <script type="text/javascript">	  
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();
	   });
		
	   var load = 0;

       $( "body" ).mousemove(function(event) {

            if(load==0){                
                $('#LASIKWowIframe-container').html('<iframe id="LASIKWowIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379882800" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');                
                load = 1;
            }
       });
		
		
		var LASIKWowIframe_player = new Vimeo.Player(document.getElementById('LASIKWowIframe'));

        $('#modalLASIKWow').on('shown.bs.modal', function () {
            LASIKWowIframe_player.play();
        })
        $('#modalLASIKWow').on('hidden.bs.modal', function () {
            LASIKWowIframe_player.pause();
        })
	</script>		
@endsection