@php
    $meta_title_inner = "Laser Vision Correction (Lasik) Suitability Test | personalEYES";
	$meta_keywords_inner = "LASIK Suitability Quiz";
	$meta_description_inner = "Are you suitable for laser vision correction? Take our online test to find out. For more information, book a free consultation with an experienced surgeon.";
	
	//$meta_robots = "noindex";
	$landing_page = true;
@endphp

@extends('site/layouts/app')

@section('content')

<div class="homeVideoLasik">
    <div class="homeVideoLasik-wrapper">
       <div class="homeVideoLasik-caption">
		   <div class="homeVideoLasik-caption-sml">
		      personalEYES - Your LASIK Specialists
		   </div>
		   Feel the freedom of 20/20 vision with LASIK eye surgery

		   <div class='homeVideoLasik-booking'>   
			  <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-clinic">Book Your Free Assessment Today</button>
		   </div>
	   </div>
    </div>        
	
    <div class="video-container-lasik">
        <img src="{{ url('') }}/images/site/suitability-quiz/slider8.jpg" alt="LASIK Suitability Quiz" text="LASIK Suitability Quiz">		
		
		<div id="chevron-down" class="chevron-down">
			<a href='#div-suitability-quiz'><i class="fas fa-chevron-down"></i></a>
	    </div>	    	    
	</div>								
</div>

<div id='div-suitability-quiz'>
   @include('site/suitability-quiz/partials/suitability-quiz')
</div>	

@include('site/partials/landing/doctors')
@include('site/partials/landing/awards')
@include('site/partials/landing/map')
@include('site/partials/landing/testimonials')
@include('site/partials/index-guarantee')


@endsection

@section('scripts')    
    <script type="text/javascript">
	   $(document).ready(function () {
			$('#frmSuitabilityQuiz').attr('action', "{{ url('') }}/are-you-suitable-for-lasik");	
	   });
				
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();
	   });
		
	   $('#btnSuitabilityQuizDown').click(function(){			   
		   $('#chevron-down').fadeOut();
	   });	

	</script>		
@endsection