@php
    $meta_title_inner = "Laser Vision Correction (Lasik) Suitability Test | personalEYES";
	$meta_keywords_inner = "LASIK Suitability Quiz";
	$meta_description_inner = "Are you suitable for laser vision correction? Take our online test to find out. For more information, book a free consultation with an experienced surgeon.";
	
	$meta_robots = "noindex";
	$landing_page = true;
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/landing/lasik-video')

<div id='div-suitability-quiz'>
   @include('site/suitability-quiz/partials/not-suitable')
</div>	

@include('site/partials/landing/doctors')
@include('site/partials/landing/awards')
@include('site/partials/landing/map')
@include('site/partials/landing/testimonials')
@include('site/partials/index-guarantee')


@endsection

@section('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
    
    <script type="text/javascript">
	   $(document).ready(function () {
			$(document).scrollTop($('#div-suitability-quiz').offset().top);
		    $('#chevron-down').hide();
	   });			

	</script>		
@endsection