@php
	$meta_robots = "noindex";
	$landing_page = true;	
@endphp

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/lasik-guide.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/popup.css') }}">
@endsection

<div class="landing-lasik-logo">
   <a href="{{ url('') }}" title="{{ $company_name }}">                
	  <img src="{{ url('') }}/images/site/logo-sm.png" title="{{ $company_name }}" alt="{{ $company_name }}">
   </a>
</div>
		   
@include('site/partials/lasik-guide-progressbar')
@include('site/partials/lasik-guide-menu')
    
@include('site/pages/lasik-guide')
