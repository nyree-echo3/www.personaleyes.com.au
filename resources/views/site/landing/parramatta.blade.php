@php
    $meta_title_inner = "personalEYES - Tired of wearing Glasses or Contacts?";
	$meta_keywords_inner = "personalEYES - Tired of wearing Glasses or Contacts?";
	$meta_description_inner = "personalEYES - Tired of wearing Glasses or Contacts?";
	
	$meta_robots = "noindex";
	$landing_page = 'deliveroo';
@endphp

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/lasik-tab.css?v=0.1') }}">
    <link rel="stylesheet" href="{{ asset('css/site/popup-info-pack.css') }}">
@endsection

@section('content')

    <div class="carousel carousel-spring carousel-deliveroo">

        <div class="carousel-spring carousel-caption deliveroo-logo">
            <a href="{{ url('/') }}">
                <img src="{{ url('') }}/images/site/logo-lasik.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="logo-lg">
            </a>
        </div>

        <div id="carousel-desktop"  class="carousel-inner">
            <div class="carousel-item active carousel-normal">
                <img class="slide-img slide" src="{{ url('') }}/images/site/parramatta-slider-image.jpg" alt="Campaign">
                <img class="slide-img-resp slide" src="{{ url('') }}/images/site/parramatta-slider-image-mobile.jpg" alt="Campaign">

                <div class="row align-items-center h-100 carousel-caption">
                    <div class="carousel-caption-inner justify-content-center">
                        <div class="deliveroo-header parramatta-header">
                            <img src="{{ url('images/site/parramara-campaign-headline.png') }}" title="Tired of wearing Glasses or Contacts?" alt="Tired of wearing Glasses or Contacts?">
                        </div>
                        <div class="carousel-caption-h2">
                            Our Parramatta clinic has the latest technology in Laser Eye Surgery, delivering safer & better results.
                        </div>

                        <div class="carousel-caption-cta">
                            <div class="carousel-caption-cta-dbl">
                                <button class="carousel-caption-cta-del summer-book-click" href="" data-toggle="modal" data-target="#scheduling-page-parramatta">
                                    BOOK YOUR FREE ASSESSMENT
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item carousel-slim">
                <img class="slide-img slide" src="{{ url('') }}/images/site/parramatta-slider-image-thinner.jpg" alt="Campaign">
                <img class="slide-img-resp slide" src="{{ url('') }}/images/site/parramatta-slider-image-mobile.jpg" alt="Campaign">

                <div class="row align-items-center carousel-caption">
                    <div class="carousel-caption-inner">
                        <div class="row">
                            <div class="col-12">
                                <div class="par-headline-cont">
                                    <a href="{{ url('/') }}">
                                        <img src="{{ url('') }}/images/site/logo-lasik.png" title="{{ $company_name }}" alt="{{ $company_name }}" class="deliveroo-logo-slim logo-lg">
                                    </a>
                                </div>
                                <div class="carousel-caption-cta par-button-cont">
                                    <div class="carousel-caption-cta-dbl">
                                        <button class="carousel-caption-cta-del summer-book-click" href="" data-toggle="modal" data-target="#scheduling-page-parramatta">
                                            BOOK YOUR FREE ASSESSMENT
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <img src="{{ url('images/site/tired_of_wearing_glasses_or_contacts.png') }}" title="Dinner is on us" class="parramatta-slim-headline" alt="Dinner is on us">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div id="carousel-mobile"  class="carousel-inner">
            <div class="carousel-item active">
                <img class="slide-img slide" src="{{ url('') }}/images/site/parramatta-slider-image.jpg" alt="Campaign">
                <img class="slide-img-resp slide" src="{{ url('') }}/images/site/parramatta-slider-image-mobile.jpg" alt="Campaign">

                <div class="row align-items-center h-100 carousel-caption">
                    <div class="carousel-caption-inner justify-content-center">
                        <div class="deliveroo-header">
                            <img src="{{ url('images/site/parramara-campaign-headline.png') }}" title="Tired of wearing Glasses or Contacts?" alt="Tired of wearing Glasses or Contacts?">
                        </div>
                        <div class="carousel-caption-h2">
                            Our Parramatta clinic has the latest technology in Laser Eye Surgery, delivering safer & better results.
                        </div>

                        <div class="carousel-caption-cta">
                            <div class="carousel-caption-cta-dbl">
                                <button class="carousel-caption-cta-del summer-book-click" href="" data-toggle="modal" data-target="#scheduling-page-parramatta">
                                    BOOK YOUR FREE ASSESSMENT
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="div-spring-campaign">
        <div class="container-fluid">
            <div class="row align-items-end h-100">
                <div class="col-lg-1"></div>

                <div class="col-lg-3 img-dr">
                    <img src="{{ url('') }}/images/site/dr-balaNEW.png" alt="Dr Chandra Bala" title="Dr Chandra Bala"
                         class="img-fluid">

                    <div class="btn-video">
                        <a data-toggle="modal" href="#modalLASIKWow">
                            <img src="{{ url('') }}/images/site/bttn-meetourdoctors.png" alt="Video Button"
                                 title="Video Button">
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 p-4 justify-content-center">
                    <h2>Hi there, I’m A/Prof Chandra Bala Eye Surgeon & co-founder of personalEYES</h2>
                    <p>If you struggle with glasses or lenses, you might have wondered if LASIK could help you.</p>
                    <p>Since every pair of eyes is unique, LASIK isn’t suitable for everyone. Typically we say no to 1
                        in 5 patients because our patient's safety is our number 1 priority. I am proud to say that 98%
                        of our patients report being ‘completely satisfied’ with their results - many even achieving
                        better than 20/20 vision.</p>
                    <button class="acuity-embed-button btn-booking-del summer-book-click" data-toggle="modal" data-target="#scheduling-page-parramatta">
                        BOOK YOUR FREE ASSESSMENT
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('site/partials/landing/lasik-steps')
    @include('site/partials/landing/doctors-parramatta-campaign')
    @include('site/partials/index-panel-cta')
    @include('site/partials/index-lasik-tab-parramatta')
    @include('site/partials/landing/awards')
    @include('site/partials/popup-info-pack')

    <div class="spring-campaign-map">
        @include('site/partials/landing/map-parramatta')
    </div>

    <div class="spring-campaign-testimonials">
        @include('site/partials/landing/testimonials')
    </div>

    <div class="spring-campaign-book-block">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="spring-campaign-book-txt">Find out if LASIK could work for you</div>

                    <div class="spring-campaign-book">
                        <button class="btn-booking-del summer-book-click" href="" data-toggle="modal" data-target="#scheduling-page-parramatta">
                            BOOK YOUR FREE ASSESSMENT
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('site/partials/index-guarantee-spring-campaign')

    @include('site/partials/scheduling-modal-parramatta')

    <!-- The Modal - The LASIK WOW -->
    <div class="modal fade" id="modalLASIKWow">
        <div class="modal-dialog modal-dialog-centered modal-video">
            <div class="modal-content">

                <!-- Modal body -->
                <div class="modal-body">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <div class="embed-responsive embed-responsive-16by9" id="LASIKWowIframe-container"></div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/site/jquery.sticky.js') }}"></script>
    <script src="https://player.vimeo.com/api/player.js"></script>

    <script type="text/javascript">
        $(window).scroll(function () {
            $('#chevron-down').fadeOut();
        });

        var load = 0;

        $("body").mousemove(function (event) {

            if (load == 0) {
                $('#LASIKWowIframe-container').html('<iframe id="LASIKWowIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379882800" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                load = 1;
            }

            var LASIKWowIframe_player = new Vimeo.Player(document.getElementById('LASIKWowIframe'));

            $('#modalLASIKWow').on('shown.bs.modal', function () {
                LASIKWowIframe_player.play();
            });
            $('#modalLASIKWow').on('hidden.bs.modal', function () {
                LASIKWowIframe_player.pause();
            });
        });

        @if($show_booking_popup=="true")
        $( document ).ready(function() {
            $('#scheduling-page-clinic').modal('show');
        });
        @endif

        $("#carousel-desktop").sticky({ topSpacing: 0 });

        $('#carousel-desktop').on('sticky-start', function() {
            $(".carousel-normal").hide();
            $(".carousel-slim").show();

        });

        $('#carousel-desktop').on('sticky-end', function() {
            $(".carousel-normal").show();
            $(".carousel-slim").hide();
        });

        /*$(window).scroll(function(e){

            console.log($(this).scrollTop());

            //var isPositionFixed = ($el.css('position') == 'fixed');
            if ($(this).scrollTop() > 200){
                $('#carousel-deliveroo').hide();
                $('#sticky-header').show();
                $('body').style('margin-top','436px');
            }
            if ($(this).scrollTop() < 200){
                $('#carousel-deliveroo').show();
                $('#sticky-header').hide();
                $('body').style('margin-top','0px');
            }
        });*/
    </script>
@endsection

