@php
	$meta_robots = "noindex";
@endphp

@extends('site/layouts/app')

@section('content')

<div id="videoCarousel" class="carousel">	
	<div class="carousel-inner">
		<div class="active carousel-item" data-slide-number="0">			        
		    <div class="carousel-item-h2">Meet A/Prof Chandra Bala</div>	
		    
			<div class="embed-responsive embed-responsive-16by9">                   			    
				<iframe id="MeetDrBalaIframe" class="embed-responsive-item"  src="https://player.vimeo.com/video/386370308" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>			
		</div>
		
		<div class="carousel-item" data-slide-number="1">
		    <div class="carousel-item-h2">What to expect when you choose personalEYES</div>
		    
			<div class="embed-responsive embed-responsive-16by9">                    
				<iframe id="PersonalEYESDifferenceIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379880469" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
		
		<div class="carousel-item" data-slide-number="2">
		    <div class="carousel-item-h2">Why should you consider having LASIK</div>
		    
			<div class="embed-responsive embed-responsive-16by9">                    
				<iframe id="LASIKWowIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379882800" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
		
		<div class="carousel-item" data-slide-number="3">
		    <div class="carousel-item-h2">LASIK Technology</div>
		    
			<div class="embed-responsive embed-responsive-16by9">                    
				<iframe id="LASIKTechnologyIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379883993" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>
		
		<div class="carousel-item" data-slide-number="4">
		    <div class="carousel-item-h2">Meet Dr Kerrie Meades</div>
		    
			<div class="embed-responsive embed-responsive-16by9">                    
				<iframe id="MeetDrKerrieMeadesIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379884779" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
			</div>
		</div>                    

		<a class="carousel-control-prev" href="#videoCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#videoCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

	</div>	

	<ul class="carousel-indicators list-inline mx-auto">
		<li class="list-inline-item active">
			<a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#videoCarousel">
				<img src="{{ url('') }}/images/site/video-small-Meet_A_Prof_Chandra_Bala.jpg" alt="Video" title="Video" class="img-fluid">
				<div class="carousel-selector-h3">Meet A/Prof Chandra Bala</div>
			</a>
		</li>
		<li class="list-inline-item">
			<a id="carousel-selector-1" data-slide-to="1" data-target="#videoCarousel">
				<img src="{{ url('') }}/images/site/video-small-What_to_expect_when_you_choose_personalEYES.jpg" alt="Video" title="Video" class="img-fluid">
				<div class="carousel-selector-h3">What to expect when you choose personalEYES</div>
			</a>
		</li>
		<li class="list-inline-item">
			<a id="carousel-selector-2" data-slide-to="2" data-target="#videoCarousel">
				<img src="{{ url('') }}/images/site/video-small-The_LASIK_WOW!_ Why_should_you_consider_having_LASIK.jpg" alt="Video" title="Video" class="img-fluid">
				<div class="carousel-selector-h3">Why should you consider having LASIK</div>
			</a>
		</li>
		<li class="list-inline-item">
			<a id="carousel-selector-3" data-slide-to="3" data-target="#videoCarousel">
				<img src="{{ url('') }}/images/site/video-small-LASIK_Technology.jpg" alt="Video" title="Video" class="img-fluid">
				<div class="carousel-selector-h3">LASIK Technology</div>
			</a>
		</li>
		<li class="list-inline-item">
			<a id="carousel-selector-4" data-slide-to="4" data-target="#videoCarousel">
				<img src="{{ url('') }}/images/site/video-small-Meet_Dr_Kerrie_Meades.jpg" alt="Video" title="Video" class="img-fluid">
				<div class="carousel-selector-h3">Meet Dr Kerrie Meades</div>
			</a>
		</li>                    
	</ul>
</div>

<div class='home-intro-booking text-center'>   
	<button class="acuity-embed-button btn-booking" data-toggle="modal" data-target="#scheduling-page-clinic">Book a Free Assessment Today </button>
</div>	   

@include('site/partials/index-lasik')  				
@include('site/partials/index-guarantee')

@endsection

@section('inline-scripts-videos-carousel')
    <script src="https://player.vimeo.com/api/player.js"></script>
    
    <script language="javascript">
		$(document).ready(function() {				
			
			var sUrl = window.location.toString();			
			sUrl = sUrl.split('/');
			
			if (sUrl.length == 5)  { 
				slideNum = sUrl[4] - 1;				
				$('#videoCarousel').carousel(slideNum);
				$('#videoCarousel').carousel('pause');
			}
		});
    </script>		
@endsection