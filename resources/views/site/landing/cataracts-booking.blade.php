@php
    $meta_title_inner = "Cataracts Booking | personalEYES";
	$meta_keywords_inner = "Cataracts Booking";
	$meta_description_inner = "Cataracts Booking";
	
	$meta_robots = "noindex";
	$landing_page = true;
@endphp

@extends('site/layouts/app')

@section('content')

<div class="landing-cataracts-logo">                          
	<a href="{{ url('') }}"><img src="{{ url('') }}/images/site/logo-lasik.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
</div> 
							    
<div class="carousel carousel-cataracts">
    <div class="carousel-inner">        
            <div class="carousel-item active">                          
                <img class="slide-img slide" src="{{ url('') }}/images/site/header-cataracts.jpg" alt="Cataracts Booking">    
                <img class="slide-img-resp slide" src="{{ url('') }}/images/site/header-cataracts-mobile.jpg" alt="Spring Campaign 2020">         				                              
            </div>  
            
               
    </div>
</div>

<div>
   <div class="container-fluid cataracts-page">
	   <div class="row align-items-center h-100">		  
		   <div class="col-lg-2"></div>
		   <div class="col-lg-8">
		        <p>&nbsp;</p>
		        
			    <h2>PersonalEYES offers laser cataract surgery and CustomLens&trade; to patients in <a href="https://www.personaleyes.com.au/locations/canberra">Canberra</a>, <a href="https://www.personaleyes.com.au/locations/parramatta">Parramatta</a>, and other locations in NSW. Cataract surgery is one of the most commonly performed procedures around the world today because it is an effective and routine method of treating cataracts.</h2>

			    <div class="spring-campaign-book cataracts-book">   
				   <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-cataracts">
					  Book Your Cataracts Assessment 
					  <img src="{{ url('') }}/images/site/icon-cursor.gif" alt="Cursor" title="Cursor" class="img-cursor">
				   </button>   
				</div>	
				
				<p class="cataracts-sml">We have limited online Cataract bookings, please call us on <a href='tel:1300683937'>1300 683 937</a> if the dates are not available.</p>	
				
				<p>&nbsp;</p>

				<h2>What Are Cataracts?</h2>

				<p>Around half of Australians aged between over 65 to 74 have cataracts. Cataracts generally develop as part of the ageing process &ndash; a &lsquo;cloud&rsquo; forms in the lens of the eye, causing blurry or double vision and increased sensitivity to light. If left untreated, cataracts will lead to complete blindness. Cataract symptoms can include:</p>

				<ul>
					<li>cloudy or foggy vision</li>
					<li>blurry or distorted vision</li>
					<li>changes in colour vision</li>
					<li>frequent increases in eyeglass or contact lens prescriptions</li>
					<li>poor night vision (especially affected by headlights)</li>
					<li>progressive loss of vision</li>
					<li>halos or glare around lights</li>
					<li>double vision</li>
					<li>loss of contrast</li>
					<li>a white or &lsquo;milky&rsquo; spot over the pupil of the eye</li>
				</ul>

				<div style="background:#474747;"><var data-presentation="45e133c6-4423-4c5b-4787-40e843bf44b6" style="width: 100%; padding-bottom: 56.25%; display: block;"><a href="//eyemag.in/6PR-bf" style="display: none;">View Video</a></var><script src="//showecho.com/whitelabel/embed_js" type="text/javascript"></script></div>
		
                <p>&nbsp;</p>
                
	            <p>A cataract may display a number of different symptoms and be caused by a variety of factors, including age, injury, and certain diseases. While some people have cataracts at birth or develop them early in life, the majority of cataract diagnoses occur in patients who are in their 60s or 70s.</p>
	            
		        <p>&nbsp;</p>
		        
				<div class="spring-campaign-book cataracts-book">   
				   <button class="acuity-embed-button btn-booking video-booking-button" data-toggle="modal" data-target="#scheduling-page-cataracts">
					  Book Your Cataracts Assessment 
					  <img src="{{ url('') }}/images/site/icon-cursor.gif" alt="Cursor" title="Cursor" class="img-cursor">
				   </button>   
				</div>
				
				<p class="cataracts-sml">We have limited online Cataract bookings, please call us on <a href='tel:1300683937'>1300 683 937</a> if the dates are not available.</p>	
					
				<p>&nbsp;</p>
		   </div>
	   </div>
   </div>
</div>	

@include('site/partials/landing/doctors-spring-campaign')
@include('site/partials/landing/awards')

@endsection