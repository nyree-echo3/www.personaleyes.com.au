@php
	// Set Meta Tags
	$meta_title_inner = "Laser Vision Correction (Lasik) Suitability Test | personalEYES";
	$meta_keywords_inner = "LASIK Suitability Quiz";
	$meta_description_inner = "Are you suitable for laser vision correction? Take our online test to find out. For more information, book a free consultation with an experienced surgeon.";
	
	$meta_robots = "noindex";
@endphp

@extends('site/layouts/app')

@section('content')
   @include('site/partials/carousel-inner')
   @include('site/suitability-quiz/partials/suitable')
@endsection
