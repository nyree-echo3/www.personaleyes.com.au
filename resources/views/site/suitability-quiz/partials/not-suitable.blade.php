@section('styles')	
	<link rel="stylesheet" href="{{ asset('css/site/suitability-quiz.css') }}">	
@endsection

@if(env('APP_ENV')=='production')
	<!-- Event snippet for QUIZ SUBMISSION (ECHO) conversion page --> 
	<script>
	  ga('event', 'conversion', {'send_to': 'AW-1064487622/CaoxCK_28fMBEMaVy_sD'});
	</script>
@endif

<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Online Suitability Quiz</h1> 
                   
                    <div class="results-icon"><img src="{{ url('') }}/images/site/suitability-quiz/no.png" alt="Not Suitable" title="Not Suitable"></div>
                    
                    <p>Thanks so much for filling in the Online Suitability Quiz {{ $request->txtFirstName}}</span>! From your answers there’s a chance you may not be suitable for LASIK.</p>                                           
                    
					<p>
                       <b>Reason:</b> 
                       @if ($request->radAgeGroup == "Under 18" || $request->radAgeGroup == "Over 56 years")
                           <br><span class="quiz-question">What's your age group?</span>
						   <br><span class="quiz-answer">{{ $request->radAgeGroup}}</span>
                       @endif
                       
                       @if ($request->radGlassesContacts == "Only for reading")
                           <br><span class="quiz-question">When do you wear glasses or contacts?</span>
						   <br><span class="quiz-answer">{{ $request->radGlassesContacts}}</span>
                       @endif
                       
                       @if ($request->radPrescription == "Yes")
                           <br><span class="quiz-question">Has your prescription changed in the last 12 months?</span>
						   <br><span class="quiz-answer">{{ $request->radPrescription}}</span>
                       @endif
                                                                                      
                       @if ($request->chkEyeConditionOcularHerpes == "Ocular herpes" || $request->chkEyeConditionRetinalDisease == "Retinal disease")
                           @php
                              $eyeConditions = $request->chkEyeConditionOcularHerpes  . ($request->chkEyeConditionOcularHerpes != "" ? "," : "") . " " . $request->chkEyeConditionRetinalDisease;
                           @endphp
                           <br><span class="quiz-question">Do you have any other eye conditions?</span>
						   <br><span class="quiz-answer">{{ $eyeConditions }}</span>
                       @endif
                       
                       
                   
                    </p>                                                            
                    
                    {!! $suitability_quiz_not_suitable !!}
                    
                    @include('site/partials/suitability-quiz-doctors')												
			        
			        <!--
			        @if (isset($request->chkEyeConditionOcularHerpes))  
                        {!! $ocular_herpes !!}	
			        @endif
			        
			        @if (isset($request->chkEyeConditionRetinalDisease))  
                        {!! $retinal_disease !!}	
			        @endif
			       -->
			        
			       <div><a class="cta-booking" href="{{ url("") }}/general-booking-page"><span>&nbsp;&nbsp;&nbsp;Book An Appointment&nbsp;&nbsp;&nbsp;</span></a></div>  
			
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@section('inline-scripts-suitability-quiz')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
	<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
	<script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
	<script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>
	<script src="{{ asset('js/site/suitability-quiz.js') }}"></script>
@endsection
