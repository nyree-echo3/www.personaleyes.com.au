@section('styles')	
	<link rel="stylesheet" href="{{ asset('css/site/suitability-quiz.css') }}">	
@endsection

@if(env('APP_ENV')=='production')
	<!-- Event snippet for QUIZ SUBMISSION (ECHO) conversion page --> 
	<script>
	  ga('event', 'conversion', {'send_to': 'AW-1064487622/CaoxCK_28fMBEMaVy_sD'});
	</script>
@endif

<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Online Suitability Quiz</h1>
                    
                    @php
                       $eyeConditions = "";
                       $eyeConditionsAlt = "";
                       $thisthese = "this condition";
                       
                       if ($request->chkEyeConditionKeratoconus == "Keratoconus" || $request->chkEyeConditionCataracts == "Cataracts") {                       
                          $eyeConditions = $request->chkEyeConditionKeratoconus  . ($request->chkEyeConditionKeratoconus != "" ? " and " : "") . " " . $request->chkEyeConditionCataracts;                        
                          $eyeConditionsAlt = $request->chkEyeConditionKeratoconus  . ($request->chkEyeConditionKeratoconus != "" ? ", " : "") . " " . $request->chkEyeConditionCataracts;                             
                       }
                       
                       if ($request->chkEyeConditionKeratoconus == "Keratoconus" && $request->chkEyeConditionCataracts == "Cataracts") { 
                          $thisthese = "these conditions"; 
                       }
                       
                    @endphp
                                           
                    <div class="results-icon"><img src="{{ url('') }}/images/site/suitability-quiz/maybe.png" alt="Maybe Suitable" title="Maybe Suitable"></div>
                                           
                    <p>Thanks so much for filling in the Online Suitability Quiz {{ $request->txtFirstName}}! You indicated that you have {{ $eyeConditions }}. To be suitable for LASIK we may need to treat {{ $thisthese }} first and then make an assessment of your suitability thereafter. The good news is that personalEYES has 25 years ophthalmic experience with {{ $eyeConditionsAlt }} and the most up-to-date treatment options available in Australia. </p>                    					                                                                                        
                    
                    {!! $suitability_quiz_maybe_suitable !!}
                    
                    @include('site/partials/suitability-quiz-doctors')												
			        
			        <!--
			        @if (isset($request->chkEyeConditionKeratoconus))  
                        {!! $keratoconus !!}	
			        @endif
			        
			        @if (isset($request->chkEyeConditionCataracts))  
                        {!! $cataracts !!}	
			        @endif
			       -->
			        
			       <div><a class="cta-booking" href="{{ url("") }}/general-booking-page"><span>&nbsp;&nbsp;&nbsp;Book An Appointment&nbsp;&nbsp;&nbsp;</span></a></div>  
			
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@section('inline-scripts-suitability-quiz')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
	<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
	<script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
	<script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>
	<script src="{{ asset('js/site/suitability-quiz.js') }}"></script>
@endsection
