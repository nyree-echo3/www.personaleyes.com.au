@section('styles')	
	<link rel="stylesheet" href="{{ asset('css/site/suitability-quiz.css') }}">	
@endsection

@if(env('APP_ENV')=='production')
	<!-- Event snippet for QUIZ SUBMISSION (ECHO) conversion page -->
	<script>
		gtag('event', 'conversion', {'send_to': 'AW-1064487622/CaoxCK_28fMBEMaVy_sD'});
	</script>

@endif
 
<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Thanks for taking the Online Suitability Quiz</h1>
                                                            
                    <div class="suitable_firstname">Congratulations {{ $request->txtFirstName}}! </div>
                    
                    <div class="results-icon"><img src="{{ url('') }}/images/site/suitability-quiz/yes.png" alt="Suitable" title="Suitable"></div>
                    
                    {!! $suitability_quiz_suitable !!}
					
					@include('site/partials/suitability-quiz-doctors')	
					
					<!--
					@if (isset($request->chkEyeConditionKeratoconus)) 
                        {!! $keratoconus !!}	
			        @endif			        			        
			        
			        @if (isset($request->chkEyeConditionCataracts))  
                        {!! $cataracts !!}	
			        @endif
			        -->
			        
			        <div><a class="cta-booking" data-target="#scheduling-page-clinic" data-toggle="modal" href="#"><span>&nbsp;&nbsp;&nbsp;Book Free Assessment&nbsp;&nbsp;&nbsp;</span></a></div>  
			        
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@section('inline-scripts-suitability-quiz')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
	<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
	<script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
	<script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>
	<script src="{{ asset('js/site/suitability-quiz.js') }}"></script>
@endsection
