@section('styles')
	<link rel="stylesheet" href="{{ asset('css/site/suitability-quiz.css?v=0.2') }}">
@endsection

<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-12 col-md-12 blog-main">
				<div class="blog-post">

					<h1 class="blog-post-title">Take the Quick Online Suitability Quiz</h1>
					{!! $suitability_quiz !!}

					<form id="frmSuitabilityQuiz" method="post" action="{{ url('vision-correction-suitability') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<h5><div class="divLinks"><div class="divSteps">1</div></div></h5>
						<section>
							<h2><span>1</span>What's your age group?</h2>

							<div class="questions">
								<div class="form-group"><input type="radio" name="radAgeGroup" value="Under 18" required>Under 18</div>
								<div class="form-group"><input type="radio" name="radAgeGroup" value="18-45 years" required>18-45 years</div>
								<div class="form-group"><input type="radio" name="radAgeGroup" value="46-55 years" required>46-55 years</div>
								<div class="form-group"><input type="radio" name="radAgeGroup" value="Over 56 years" required>Over 56 years</div>
							</div>

							<div class="question-icon"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 1.png" alt="Quiz Icon 1" title="Quiz Icon 1"></div>
						</section>

						<h5><div class="divLinks"><div class="divSteps">2</div></div></h5>
						<section>
							<h2><span>2</span>When do you wear glasses or contacts?</h2>

							<div class="questions">
								<div class="form-group radDiv"><input type="radio" name="radGlassesContacts" value="All the time" required><span class="radLabel">All the time</span></div>
								<div class="form-group radDiv"><input type="radio" name="radGlassesContacts" value="Only for reading" required><span class="radLabel">Only for reading</span></div>
								<div class="form-group radDiv"><input type="radio" name="radGlassesContacts" value="Only for distance" required><span class="radLabel">Only for distance</span></div>
								<div class="form-group radDiv"><input type="radio" name="radGlassesContacts" value="I don't wear glasses/contacts but I suffer poor vision" required><span class="radLabel">I don't wear glasses/contacts but I suffer poor vision</span></div>
							</div>

							<div class="question-icon"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 2.png" alt="Quiz Icon 2" title="Quiz Icon 2"></div>
						</section>

						<h5><div class="divLinks"><div class="divSteps">3</div></div></h5>
						<section>
							<h2><span>3</span>Do you have astigmatism?*</h2>

							<div class="questions">
								<div class="form-group"><input type="radio" name="radAstigmatism" value="Yes" required>Yes</div>
								<div class="form-group"><input type="radio" name="radAstigmatism" value="No" required>No</div>
								<div class="form-group"><input type="radio" name="radAstigmatism" value="Not Sure" required>Not Sure</div>
							</div>

							<div class="question-icon"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 3.png" alt="Quiz Icon 3" title="Quiz Icon 3"></div>
						</section>

						<h5><div class="divLinks"><div class="divSteps">4</div></div></h5>
						<section>
							<h2><span>4</span>Has your prescription changed in the last 12 months?*</h2>

							<div class="questions">
								<div class="form-group"><input type="radio" name="radPrescription" value="Yes" required>Yes</div>
								<div class="form-group"><input type="radio" name="radPrescription" value="No" required>No</div>
								<div class="form-group"><input type="radio" name="radPrescription" value="Not Sure" required>Not Sure</div>
							</div>

							<div class="question-icon"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 4.png" alt="Quiz Icon 4" title="Quiz Icon 4"></div>
						</section>

						<h5><div class="divLinks"><div class="divSteps">5</div></div></h5>
						<section>
							<h2><span>5</span>Do you have any other eye conditions?</h2>

							<div class="questions">
								<div class="form-group"><input type="checkbox" name="chkEyeConditionGlaucoma" value="Glaucoma">Glaucoma</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionDryEyes" value="Dry eyes">Dry eyes</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionKeratoconus" value="Keratoconus">Keratoconus</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionCornealScarring" value="Corneal Scarring">Corneal Scarring</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionCataracts" value="Cataracts">Cataracts</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionOcularHerpes" value="Ocular herpes">Ocular herpes</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionRetinalDisease" value="Retinal disease">Retinal disease</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionNoneOfThese" value="None of these">None of these</div>
								<div class="form-group"><input type="checkbox" name="chkEyeConditionNotSure" value="Not Sure">Not Sure</div>
							</div>

							<div class="question-icon"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 5.png" alt="Quiz Icon 5" title="Quiz Icon 5"></div>
						</section>

						<h5><div class="divLinks divLink-end"><div class="divSteps">6</div></div></h5>
						<section>
							<h2><span>6</span>All done! We’re calculating your results now. Pop in your details below and we’ll send them over to you shortly.</h2>

							<div class="questions questions6">
								<div class="form-line">
									<div class="form-group form-group-inputs">
									   <div><label for="txtFirstName">First name *</label></div>
									   <div><input type="textbox" name="txtFirstName" value="" required></div>
									</div>

									<div class="form-group form-group-inputs">
									   <div><label for="txtFirstName">Last name *</label></div>
									   <div><input type="textbox" name="txtLastName" value="" required></div>
									</div>
								</div>

								<div class="form-line">
									<div class="form-group form-group-inputs">
									   <div><label for="txtEmail">Email *</label></div>
									   <div><input type="textbox" name="txtEmail" value="" required></div>
									</div>

									<div class="form-group form-group-inputs">
									   <div><label for="selState">State *</label></div>

									   <div><select name="selState" required>
											   <option value=""></option>
											   <option value="ACT">ACT</option>
											   <option value="NSW">NSW</option>
											   <option value="NT">NT</option>
											   <option value="QLD">QLD</option>
											   <option value="SA">SA</option>
											   <option value="TAS">TAS</option>
											   <option value="VIC">VIC</option>
											   <option value="WA">WA</option>
											   <option value="Not in Australia">Not in Australia</option>
										   </select></div>
									</div>
								</div>
							</div>

							<div class="question-icon question-icon6"><img src="{{ url('') }}/images/site/suitability-quiz/suitability quiz icon 6.png" alt="Quiz Icon 6" title="Quiz Icon 6"></div>
						</section>

						<div id="error"></div>
						<a href="#" class="suitability-button1"></a>
						<a href="#" class="suitability-button2"></a>
						<a href="#" class="suitability-button3"></a>
						<a href="#" class="suitability-button4"></a>
						<a href="#" class="suitability-button5"></a>
						<a href="#" class="suitability-button6"></a>
					</form>

					<div class="suitability-quiz">
						<!-- Hubspot Form -->
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
                            hbspt.forms.create({
                            portalId: "5064462",
                            formId: "3352a4d4-7a7e-4931-b2af-b43f97d20721",
							 onFormReady: function($form, ctx){
								setTimeout(updateValues, 1000);
                            },
							onFormSubmit: function($form) {
								console.log( $form.serialize() );
							}
                          });
						</script>
					</div>
				</div><!-- /.blog-post -->
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@section('inline-scripts-suitability-quiz')
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
	<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
	<script src="{{ asset('components/jquery-validation/dist/jquery.validate.js') }}"></script>
	<script src="{{ asset('components/jquery.steps/build/jquery.steps.js') }}"></script>
	<script src="{{ asset('js/site/suitability-quiz.js?v=0.27') }}"></script>
@endsection
