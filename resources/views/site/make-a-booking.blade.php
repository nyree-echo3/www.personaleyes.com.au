@php
	// Set Meta Tags
	$meta_title_inner = "Make A Booking | personalEYES";
	$meta_keywords_inner = "Make A Booking | personalEYES";
	$meta_description_inner = "Book your appointment or consultation with your eye doctor online. For more information, contact a member of our team.";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main">
				<div class="blog-book">  
				       
					<h1 class="blog-post-title">Make A Booking</h1>

				    <a class="acuity-embed-button tm-lasik-book" data-toggle="modal" data-target="#scheduling-page-clinic">
						<div class="booking-box lasik-booking">
						   <div class="free-band"></div>
						   <p>Click here for</p>
 						   <img src="{{ url('') }}/images/site/booking1.png" alt="Vision Correction Bookings" class="img-booking">
						   <p class="booking-header">Vision Correction</p>						  
						   <p class="booking-header2">Surgical correction of short sightedness, long sightedness and astigmatism only</p>
						</div>
                    </a>
                    
                    <a class="acuity-embed-button tm-lasik-book" data-toggle="modal" data-target="#scheduling-page-cataracts">
						<div class="booking-box general-booking">						   
						   <p>Click here for</p>
 						   <img src="{{ url('') }}/images/site/booking4.png" alt="Cataract Bookings" class="img-booking">
						   <p class="booking-header">Cataract Bookings</p>						  
						   <p class="booking-header2">We have limited online Cataract bookings, please call us if the dates are not available.</p>
						</div>
                    </a>

					<a href="{{ url('') }}/general-booking-page/">
						<div class="booking-box general-booking booking-box-last">
							<p>Click here for</p>
							<img src="{{ url('') }}/images/site/booking2.png" alt="General Bookings" class="img-booking">
							<p class="booking-header">General Bookings</p>
							<p class="booking-header2">Glaucoma, Retinal Diseases and Paediatric appointments</p>
						</div>
					</a>
                    
				</div>

                <!--<div class="blog-book">

					<a href="{{ url('') }}/telehealth-booking-page/">
						<div class="booking-box general-booking booking-box-last">
							<p>Click here for</p>
							<img src="{{ url('') }}/images/site/booking3.png" alt="Telehealth Bookings" class="img-booking">
							<p class="booking-header">Telehealth Bookings</p>
							<p class="booking-header2">Available 8am-6pm Monday to Friday. Bulk-billed.</p>
						</div>
					</a>

				</div> -->
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection

@section('inline-scripts')
<script type="text/javascript">
    $( document ).ready(function() {
	@if(app('request')->input('popup')=='true')
    	$('#scheduling-page').modal('show');
	@endif
    })
</script>
@endsection
