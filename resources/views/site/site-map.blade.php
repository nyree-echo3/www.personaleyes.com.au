@php
	// Set Meta Tags
	$meta_title_inner = "Sitemap | personalEYES";
	$meta_keywords_inner = "Sitemap | personalEYES";
	$meta_description_inner = "Navigate easily through the personalEYES website using our sitemap. For more information about eye conditions or procedures, please contact a member of our team.";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">               
        <div class="col-sm-9 blog-main">

          <div class="blog-sitemap">            
            <h1 class="blog-post-title">Site Map</h1>
            
            <div class="site-map">
               <ul>
			       {!! $navigation !!}
               </ul>
            </div>
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
