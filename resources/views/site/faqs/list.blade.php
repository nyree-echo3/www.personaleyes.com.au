@php
   $meta_title_inner =  "FAQ | personalEYES";
   $meta_keywords_inner = "FAQ | personalEYES";
   $meta_description_inner = "Find answers to the most common questions about eye conditions and treatment from the eye specialists at personalEYES. For more information, contact a member of our team.";	
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                
        <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

          <div class="blog-post faqs">
            <h1 class="blog-post-title">{{ $faq_category_name }}</h1>
                   
            @if (isset($items))        
				@foreach ($items as $item)					
					<h2><a class="accordion-toggle" data-toggle="collapse" href="#faqitem{{ $loop->iteration }}">{{$item->title}}</a></h2>
					
					<div id="faqitem{{ $loop->iteration }}" class="panel-collapse collapse in"> {!! $item->description !!} </div>                                   					
				@endforeach 
            @endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
