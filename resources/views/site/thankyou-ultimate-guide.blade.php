@php
	// Set Meta Tags
	$meta_title_inner = "The Ultimate Australian Guide to LASIK Thank-you | personalEYES";
	$meta_keywords_inner = "The Ultimate Australian Guide to LASIK Thank-you | personalEYES";
	$meta_description_inner = "Thank-you for download our ultimate Australian guide to LASIK.";
@endphp

@extends('site/layouts/app')

@section('content')

@if(env('APP_ENV')=='production')
	<!-- Event snippet for DOWNLOAD ULTIMATE GUIDE (ECHO) conversion page -->
	<script>
		gtag('event', 'conversion', {'send_to': 'AW-1064487622/oQ-NCOv-8PMBEMaVy_sD'});
	</script>

@endif

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main">	
			    
				    <div class="blog-book">  							       
						<h1 class="blog-post-title">The Ultimate Australian Guide to LASIK Download</h1>
						<p>Thanks for downloading our ultimate Australian Guide to LASIK.</p>
						<p>If you have any further questions, please <a href='{{ url('') }}contact'>contact us</a>.</p>
					</div>

				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@include('site/partials/index-panel-cta')        
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection

@section('inline-scripts')
<script type="text/javascript">
    $( document ).ready(function() {
	@if(app('request')->input('popup')=='true')
    	$('#scheduling-page').modal('show');
	@endif
    })
</script>
@endsection
