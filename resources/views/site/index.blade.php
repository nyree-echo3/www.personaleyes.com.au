@extends('site/layouts/app')

@section('styles')
	<link rel="stylesheet" href="{{ asset('/components/owl.carousel/dist/assets/owl.carousel.css') }}">
	<link rel="stylesheet" href="{{ asset('css/site/location.css?v=0.3') }}">
@endsection

@section('content')

@include('site/partials/carousel')

@include('site/partials/index-home-seo')

@include('site/partials/index-home-services')
@include('site/partials/index-home-map')
@include('site/partials/index-home-doctors-slider')
@include('site/partials/index-home')
@include('site/partials/index-locations-clinic')

@endsection

@section('scripts-cataract-booking-button')
	@if (isset($show_booking_form_cataract) && $show_booking_form_cataract)
	<script type="text/javascript">	 
		$( document ).ready(function() {			
		   $('#scheduling-page-cataracts').modal('show'); 
		});
	</script>		
	@endif	
@endsection

@section('scripts')
	<script src="{{ asset('/components/owl.carousel/dist/owl.carousel.js') }}"></script>
@endsection

@section('inline-scripts')
	<script type="text/javascript">

		var services = $('.owl-carousel-services');
		services.owlCarousel({
			loop: false,
			autoplay : false,
			//autoplaySpeed : 750,
			//autoplayTimeout : 6000,
			//autoplayHoverPause : true,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1,
				},
				992: {
					items: 2,
				},
				1200: {
					items: 3,
				}
			}
		});

		$('.owl-next-services').click(function () {
			services.trigger('next.owl.carousel');
		});

		$('.owl-prev-services').click(function () {
			services.trigger('prev.owl.carousel');
		});

		var specialists = $('.owl-carousel-specialists');
		specialists.owlCarousel({
			loop: false,
			autoplay : false,
			//autoplaySpeed : 750,
			//autoplayTimeout : 6000,
			//autoplayHoverPause : true,
			responsiveClass: true,
			responsive: {
				0: {
					items: 1,
				},
				992: {
					items: 2,
				},
				1200: {
					items: 3,
				}
			}
		});

		$('.owl-next-specialists').click(function () {
			specialists.trigger('next.owl.carousel');
		});

		$('.owl-prev-specialists').click(function () {
			specialists.trigger('prev.owl.carousel');
		});

	</script>
@endsection