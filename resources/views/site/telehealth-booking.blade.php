@php
	// Set Meta Tags
	$meta_title_inner = "Book Telehealth Appointment | personalEYES";
	$meta_keywords_inner = "Book Telehealth Appointment | personalEYES";
	$meta_description_inner = "To make a booking, complete our online booking form below or call 1300 683 937 and one of our refractive surgery consultants will contact with you within 48 hours";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Telehealth Booking</h1>
                    <p>In view of the COVID-19 situation, we now have the option of a telephone consultation with our specialists. These are now bulk-billed and no referrals are required. Use the form below to request an appointment and we’ll will be in contact with you. Alternatively call us on 1300 683 937 [1300 NuEyes] and we can help you over the phone.</p>
                    <a class="acuity-embed-button tm-lasik-book" data-toggle="modal" data-target="#scheduling-page-clinic">For free LASIK assessments please click here</a>

                    <!-- Hubspot Form -->
					<!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
						hbspt.forms.create({
							portalId: "5064462",
							formId: "ec8aa5a2-1ecd-496d-9e62-e6e27d66ba49"
						});
					</script>
					<!-- End HubSpot Form -->
					
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
