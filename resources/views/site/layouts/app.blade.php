@php
    if(isset($mode) && $mode == 'preview')
      $meta_title_inner = "Preview Page";
@endphp

<!doctype html>
<html lang="en">
<head>

    @if(env('APP_ENV')=='production')
    <script>
        window.dataLayer = window.dataLayer || [];
    </script>

    <!-- Anti-flicker snippet (recommended)  -->
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-MRWQW82':true});</script>

    <!-- Analytics tracking code with Optimize plugin -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2754995-2', 'auto');
        ga('require', 'GTM-MRWQW82');
		//ga('require','AW-1064487622'); // Google Adverts
    </script>

        <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KRSCWMC');</script>
    <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Ads: 1064487622 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1064487622"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-1064487622');
        </script>
    @endif

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
    <meta name="author" content="Echo3 Media">
    <meta name="web_author" content="www.echo3.com.au">
    <meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
    <meta name="robots" content="{{ (isset($meta_robots) ? $meta_robots : "all") }}">
    <meta name=google-site-verification content=8_XsmYE3H_t2fJIFifcahmePA7y8v47xsVB7DBNH6XM>

    @if(env('APP_ENV')=='production')
  
    <!-- Hotjar Tracking Code for https://www.personaleyes.com.au/ -->
	<script>
		(function(h,o,t,j,a,r){
			h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			h._hjSettings={hjid:1577252,hjsv:6};
			a=o.getElementsByTagName('head')[0];
			r=o.createElement('script');r.async=1;
			r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
    @endif
   
    <title>{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/css/all.css') }}">
    <link href="{{ asset('/css/site/carousel.css?v=0.2') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/general.css?v=0.32') }}">
    <link rel="stylesheet" href="{{ asset('css/site/scheduling-modal-clinic.css?v=2.1') }}">
    <link rel="stylesheet" href="{{ asset('css/site/scheduling-modal-cataracts.css?v=2.1') }}">
    
    @if (!isset($landing_page))				
		<link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
		<link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/base/all.css') }}">
		<link rel="stylesheet" href="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.css') }}">
		
		<link rel="stylesheet" href="{{ asset('css/site/popup-info-pack.css') }}">
    @endif
    
    @php
       $service_slug = Cookie::get('services_slug');       
    @endphp
    @if ((isset($home_style_override) && $home_style_override) || (!isset($home_style_override) && $service_slug == ""))
        <link rel="stylesheet" href="{{ asset('css/site/home.css') }}">
    @endif
    @if (isset($landing_page))
        @if($landing_page=='deliveroo')
        <link rel="stylesheet" href="{{ asset('css/site/landing-deliveroo.css?v=1.7') }}">
        @else
        <link rel="stylesheet" href="{{ asset('css/site/landing.css?v=1.3') }}">
        @endif
    @endif
    <link rel="stylesheet" href="{{ asset('css/site/animate.css') }}">
    <link href="{{ asset('/components/css-hamburgers/dist/hamburgers.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">

    @yield('styles')

    @if(env('APP_ENV')=='production')
    <!-- Google Analytics -->
    {!! $google_analytics !!}
    @endif
    
    <!-- Schema -->
    <script type="application/ld+json">
	{
	"@context": "http://schema.org",
	"@type": "Organization",
	"name" : “PersonalEYES",
	"url": "{{ url('') }}”,
	"logo": “{{ url('') }}/images/site/logo.png”
	"sameAs" : [
		"https://www.facebook.com/personalEYES”,	
		"https://twitter.com/personalEYES",
		"https://www.youtube.com/user/LasikEyeSurgery"
		"https://www.instagram.com/personaleyesaustralia/"
	]
	"contactPoint" : [{
		"@type" : "ContactPoint",
		"telephone" : "1300683937",
		"contactType" : "customer service"
	}]
	}
	</script>
	
	<!-- Twitter Cards -->
	<meta name="twitter:card" content="summary" />
	
	<!-- Facebook Open Graph -->
	<meta property="og:url"                content="{{ Request::url() }}" />
	<meta property="og:type"               content="{{ (isset($og_type) ? $og_type : "website") }}" />
	<meta property="og:title"              content="{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}" />
	<meta property="og:description"        content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}" />
	<meta property="og:image"              content="{{ url('') }}{{ (isset($og_image) ? $og_image : "/images/site/og-logo.jpg") }}" />

    <!-- Canonical Tag -->
    <link rel="canonical" href="{{ Request::url() }}" />
    
</head>
<body>
@if(env('APP_ENV')=='production')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRSCWMC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif

@if (!isset($landing_page))
<header>    
    @include('site/partials/preview')
    @include('site/partials/navigationV2')
</header>
@endif

<main role="main">
    @yield('content')
    
	@if (!isset($landing_page))
       @include('site/partials/footer')
       
       @include('site/partials/scheduling-modal')
       @include('site/partials/popup-info-pack')
    @endif 
    @include('site/partials/scheduling-modal-clinic') 
    @include('site/partials/scheduling-modal-clinic-cataracts')            
</main>

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('js/site/ga-event-tracking.js?v=0.1') }}"></script>

@if (!isset($landing_page))	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>	
	<script src="{{ asset('/ckeditor/plugins/mjTab/frontend/mjTab.js') }}"></script>
	<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>
	<script src="{{ asset('js/site/jquery.textresizer.js') }}"></script>
	<script src="{{ asset('js//site/affix.js') }}"></script>
	<script src="{{ asset('js/site/button-topofpage.js') }}"></script>
	<script src="{{ asset('js/site/number-counter.js') }}"></script>
@endif

@if (!isset($landing_page))
	<script type="text/javascript">
		function isScrolledIntoView(element) {
			var scrollBottomPosition = $(window).scrollTop() + $(window).height();
			return ($(element).offset().top < scrollBottomPosition);
		}

		function addClassIfVisible(element) {
			$(element).each(function () {      
				if (isScrolledIntoView(this)) {
				  $(this).addClass('is-visible');
				} else  {
					$(this).removeClass('is-visible');
				}
			});
		}

		addClassIfVisible('.img-doctor');
		addClassIfVisible('.img-doctors');
		addClassIfVisible('.footerContactsWrapperImg');

		$(window).scroll(function () {
			addClassIfVisible('.img-doctor');
			addClassIfVisible('.img-doctors');
			addClassIfVisible('.footerContactsWrapperImg');
		});

		$( document ).ready(function() {
			$('#scheduling-page').modal('handleUpdate');
			$('#scheduling-page-clinic').modal('handleUpdate');
			$('#scheduling-page-cataracts').modal('handleUpdate');
			$('#online-eye-test').modal('handleUpdate');

			$('.juiAccordion').accordion();
			$('.mj_tab').mjTab();


            $('#scheduling-page').on('show.bs.modal', function (e) {
                $('#scheduling-iframe').attr('src','https://personaleyes-lasik-booking.as.me/schedule.php');
            })
		});
	</script>
@endif
   
@yield('scripts')
@yield('inline-scripts')
@yield('inline-scripts-navigation')
@yield('scripts-parallex')
@yield('inline-scripts-parallex')
@yield('inline-scripts-popups')
@yield('inline-scripts-video')
@yield('inline-scripts-videos')
@yield('inline-scripts-suitability-quiz')
@yield('inline-scripts-videos-carousel')
@yield('inline-scripts-popup-info-pack')
@yield('inline-scripts-scheduling-modal-clinic')
@yield('inline-scripts-scheduling-modal-cataracts')
@yield('scripts-cataract-booking-button')
</body>
</html>