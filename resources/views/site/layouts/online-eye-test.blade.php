<!doctype html>
<html lang="en">
<head>

    @if(env('APP_ENV')=='production')
    <script>
        window.dataLayer = window.dataLayer || [];
    </script>

    <!-- Anti-flicker snippet (recommended)  -->
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
            h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
            {'GTM-MRWQW82':true});</script>

    <!-- Analytics tracking code with Optimize plugin -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2754995-2', 'auto');
        ga('require', 'GTM-MRWQW82');
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KRSCWMC');</script>
    <!-- End Google Tag Manager -->
    @endif


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    
    @if(env('APP_ENV')=='production')
    <!-- Crazyegg -->
    <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0087/7243.js" async="async"></script>

    <!-- Hotjar Tracking Code for https://www.personaleyes.com.au/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1577252,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    @endif

    <title>Online Eye Test</title>
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/site/online-eye-test.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png">
    @yield('styles')

</head>
<body>

@if(env('APP_ENV')=='production')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRSCWMC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@endif

<header class="blog-header">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <img src="{{ url('') }}/images/site/PE-logo-online-eye-test.png" title="PersonalEYES" alt="PersonalEYES - Online Eye Test"/>
        </div>
    </div>
</header>

<main role="main">
    @yield('content')
</main>

@include('site/online-eye-test/ask-the-doctor')

<footer class="blog-footer">
    <p>&copy; 2019 <a ref="nofollow" href="http://www.personaleyes.com.au/">personalEYES Pty Ltd</a>&nbsp;<br />
    Toll Free <a href="tel:1300683937">1300 68 3937</a> [1300 Nu Eyes] | Phone <a href="tel:0288337111">02 8833 7111</a> | Fax 02 8833 7112 | PO Box 301 Parramatta NSW 2150</p>
</footer>

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( document ).ready(function() {
        $("#seveninput").keyup(function() {

            if($(this).val().length==8){
                $(".app-tick").show();
            }else{
                $(".app-tick").hide();
            }
        });

        $("#ask-the-doctor").click(function() {
            $("#ask-the-doctor-container").toggle();
        });

        $("#atd-close").click(function() {
            $("#ask-the-doctor-container").hide();
        });
    });

    function closePopup(){
        $("#ask-the-doctor-container").hide();
    }
</script>
@yield('scripts')
@yield('inline-scripts')
</body>
</html>