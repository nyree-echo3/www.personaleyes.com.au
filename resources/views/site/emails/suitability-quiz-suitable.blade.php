<!DOCTYPE html>
<html>
<head>
    <style>
        body { font: 14px/18px normal Open Sans,Arial,sans-serif; color: #333333;}
		h2 {color: #3cb0bc;}
		h3 {text-align: center; color: #00a1bc;}
		a, a:active, a:visited {text-decoration: none!important; color:#284863; font-weight: 600;}
        a:hover {color:#269eb2;}
		.suitable_firstname {color: #3cb0bc; font-weight: 600; font-size: 20px; line-height: 150%; padding: 5px 0 5px 0; margin-top: 30px;}
		.divDoctors {width:100%;}
		.divDoctorVideo {width:45%; margin:0 2.5%; float:left;}
		.divDoctorVideo img {width: 290px;}
		.cta-booking {width:auto; background: #ff6903 !important; color:#fff !important; text-transform: uppercase !important;  text-align: left; border: 1px solid #ff6903 !important; text-decoration: none; font-size: 17px; text-decoration: none!important;}
		.cta-booking span {padding: 5px 20px !important;font-size: 17px; color:#fff !important;}
        .cta-booking a:hover {color:#ff6903 !important; background: #fff !important; border: 1px solid #ff6903 !important; padding: 5px 20px !important;} 
		.home-doctors-video-txt { background:#DDDDDD; color: #474747; padding:5px 10px; text-align: center; font-weight: 500; padding: 10px;}
    </style>
</head>
<body>
<table width="600" align="center">
    <tr>
        <td colspan='2'><img src="{{ url('') }}/images/site/online-eye-test-email/header.jpg"></td>
    </tr>
    <tr>
        <td colspan='2'>
            <p class="suitable_firstname">Congratulations {{ $first_name}}!</p>
            {!! $suitability_quiz_suitable !!}
            
            <h2>Meet The Surgeons</h2>
            <p>With our unwavering commitment to your safety & a unique personalised care approach, personalEYES has become one of the most experienced corrective eye surgery groups in Australasia.  Our friendly team of specialists and internationally-recognized surgeons are committed to achieving the best possible vision for you.</p>
        </td>
    </tr>
    <tr style="vertical-align: top;"> 
        <td>                           
		  <h3>A/Prof Chandra Bala</h3>
		  <a href='{{ url('') }}/our-doctors-and-ophthalmologists/dr-chandra-bala'>
			<img src="{{ url('') }}/images/site/suitability-quiz/video-drbala.jpg" />
		  </a>
		  <div class="home-doctors-video-txt">Surgical Director at personalEYES, A/Prof Bala discusses maintaining optimum safety and getting the absolute best results for our patients.</div>		  
        </td>
	    <td>
		  <h3>Dr Kerrie Meades</h3>
		  <a href='{{ url('') }}/our-doctors-and-ophthalmologists/dr-kerrie-meades'>
			 <img src="{{ url('') }}/images/site/suitability-quiz/video-drmeades.jpg" />
		  </a>	 
		  <div class="home-doctors-video-txt">LASIK pioneer and Chief Medical Director of personalEYES, Dr Meades outlines the fundamentals of LASIK.</div>		     			                           
        </td>
    </tr> 
    
    <tr>
        <td colspan='2'><br><a href="https://www.personaleyes.com.au/lasik-booking-page" class="cta-booking"><span>&nbsp;&nbsp;&nbsp;Book Free Assessment&nbsp;&nbsp;&nbsp;</span></a><br><br></div></td>
	</tr>           
    <tr>
        <td colspan='2'><p>Warm regards, <br />The team at personalEYES<p></td>
    </tr>
    <tr>
        <td colspan='2'>&nbsp;</td>
    </tr>
    <tr>
        <td colspan='2'>
            <img src="{{ url('') }}/images/site/online-eye-test-email/PE-logo.png" width="200" />
        </td>
    </tr>
    <tr>
        <td colspan='2'>&nbsp;</td>
    </tr>
    <tr>
        <td colspan='2'><a href="{{ url('') }}" target="_blank" style="font-size: 12px;">{{ url('') }}</a></td>
    </tr>
    <tr>
        <td colspan='2'>
            <p style="font-size: 12px;">
            Toll Free 1300 68 3937 [1300 Nu Eyes]<br />
            P (02) 8833 7111<br />
            F (02) 8833 7112 or 1800 789 077<br />
            E <a href="mailto:vision@personaleyes.com.au">vision@personaleyes.com.au</a><br />
            PO Box 301 Parramatta, NSW, 2150
            <p>
        </td>
    </tr>
</table>
</body>
</html>
