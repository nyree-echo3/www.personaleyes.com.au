@php   
   // Set Meta Tags
   $meta_title_inner = "Services";
   $meta_keywords_inner = "Services";
   $meta_description_inner = "Services";   
@endphp

@extends('site/layouts/app')
@section('content')

@include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">
             
                    <div class="blog-post">
                       <h1>Treatments</h1>
                       <h2>personalEYES is a full service eye centre and we offer a range of eye treatments, therapies and specialised testing. These include:</h2>
                       
                       <div class="services-nav">
                          <ul>
                          
                          @php                             
                             $side_nav = str_replace('list-group-item">', 'list-group-item"><h2>',$side_nav);
							  $side_nav = str_replace("</li>", "</h2></li>",$side_nav);
                             
                             $side_nav = str_replace("list-group-item", "list-group-item", $side_nav);
                             $side_nav = str_replace('list-group-item">', 'list-group-item">',$side_nav);
                          @endphp
                          
                          {!! $side_nav !!}                        
                          </ul>
                       </div>
                    </div><!-- /.blog-post -->                    
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->    
@endsection
