<?php
// Set Meta Tags
$meta_title_inner = $service->title;
$meta_keywords_inner = $service->title;
$meta_description_inner = $service->title;
?>

@extends('site/layouts/app')
@section('content')

    @php
       $header_image = $service->header_image;
       $category_name = $service->title;
    @endphp
    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">

                @if ($side_nav)
                    @include('site/partials/sidebar-service-pages')
                @endif

                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        @if (isset($service))
                            <h1 class="blog-post-title">{{$service->title}}</h1>
                            {!! $service->description !!}
                        @endif
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->    
@endsection
