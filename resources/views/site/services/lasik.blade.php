@php
   $meta_title_inner =  "Lasik Eye Surgery For Vision Correction | personalEYES";
   $meta_keywords_inner = "Lasik Eye Surgery For Vision Correction | personalEYES";
   $meta_description_inner = "Find out more about our LASIK eye surgery treatments to help you improve your vision. For more personalised information, book a FREE consultation with a vision correction specialist.";	
@endphp

@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/lasik-tab.css?v=0.1') }}">
@endsection

@section('content')

@include('site/partials/index-lasik-video-aws')
@include('site/partials/index-videos-carousel')
@include('site/partials/index-panel-cta')
@include('site/partials/index-doctors')
@include('site/partials/index-lasik-tab')
@php
/*
@include('site/partials/index-videos')
*/
@endphp

@include('site/partials/index-lasik')
@include('site/partials/index-guarantee')
@include('site/partials/index-locations')


@endsection

@section('scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
    
    <script type="text/javascript">	  	
	   $(window).scroll(function(){		   
		   $('#chevron-down').fadeOut();
	   });

	   var load = 0;

       $( "body" ).mousemove(function(event) {

            if(load==0){
                //$('#lasik-video-container').html('<iframe src="https://player.vimeo.com/video/374081225?autoplay=1&loop=1&autopause=0&background=1" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#MeetDrBalaIframe-container').html('<iframe id="MeetDrBalaIframe" class="embed-responsive-item"  src="https://player.vimeo.com/video/386370308" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#PersonalEYESDifferenceIframe-container').html('<iframe id="PersonalEYESDifferenceIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379880469" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#LASIKWowIframe-container').html('<iframe id="LASIKWowIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379882800" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#LASIKTechnologyIframe-container').html('<iframe id="LASIKTechnologyIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379883993" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#MeetDrKerrieMeadesIframe-container').html('<iframe id="MeetDrKerrieMeadesIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/379884779" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#DrBalaIframe-container').html('<iframe id="DrBalaIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/374299575" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                $('#DrKerrieIframe-container').html('<iframe id="DrKerrieIframe" class="embed-responsive-item" src="https://player.vimeo.com/video/374305673" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>');
                load = 1;

            }
       });
			      
	</script>		
	
	@if (isset($show_booking_form) && $show_booking_form)
	<script type="text/javascript">	 
		$( document ).ready(function() {
			$( ".acuity-embed-button" ).trigger( 'click' );
		});
	</script>		
	@endif	
@endsection