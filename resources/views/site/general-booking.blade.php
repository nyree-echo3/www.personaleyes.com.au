@php
	// Set Meta Tags
	$meta_title_inner = "Book your Eye Check Up or Appointment | personalEYES";
	$meta_keywords_inner = "Book your Eye Check Up or Appointment | personalEYES";
	$meta_description_inner = "To make a booking, complete our online booking form below or call 1300 683 937 and one of our refractive surgery consultants will contact with you within 48 hours";
@endphp

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Make A General Booking</h1>
                    <h3>Use the form below to make general booking requests</h3>
                    <a class="acuity-embed-button tm-lasik-book" data-toggle="modal" data-target="#scheduling-page-clinic">For free LASIK assessments please click here</a>
				    
                    <p>We offer a range of general appointments including General Check-ups, Cataract, Glaucoma & Retina Assessments. These appointments vary in cost. Use the form below to request an appointment and we’ll get back to you within 48hours to confirm the date and time the particular specialist is available at your local clinic. Alternatively call us on <a href="tel:1300683937">1300 683 937 [1300 NuEyes]</a> and we can help you over the phone.</p>
                    
                    <!-- Hubspot Form -->
                    <!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({
						portalId: "5064462",
						formId: "ca8a69d1-f00d-467f-bef4-d00080d03056"
					});
					</script>
					<!-- End HubSpot Form -->
					
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
