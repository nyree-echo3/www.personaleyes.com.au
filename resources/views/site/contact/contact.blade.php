@php
	// Set Meta Tags
	$meta_title_inner = "Contact Us | personalEYES";
	$meta_keywords_inner = "Contact Us | personalEYES";
	$meta_description_inner = "Contact a member of our team to get more information on our laser eye surgery options or to book a consultation with one of our eye specialists.";
@endphp

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

                    <div class="blog-post">
                        <h1 class="blog-post-title">Contact us</h1>
                        {!! $contact_details !!}
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "5064462",
                                formId: "7f6ae947-750c-45b3-8daa-c691e2b3521b"
                            });
                        </script>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->

@endsection
