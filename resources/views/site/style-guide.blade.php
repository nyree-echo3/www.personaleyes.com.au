@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">               
        <div class="col-sm-9 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Heading 1 lorem ipsum</h1>
            
			<p>P Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

			<blockquote>
			<p>Blockquote Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
			</blockquote>

			<ul>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
			</ul>
			
			<hr />
			<h2>Heading 2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h2>

			<h3>Heading 3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h3>
           
            <h4>Heading 4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>
            
            <h5>Heading 5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h5>
           
            <div class="disclaimer"><p>* As of October 2018</p></div>
            
            <h1>Extra Styles</h1>
            
            <div class="blue-box">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="blue-box">Your text goes inside this tag.</div>' }}
		    </div>
          
            <div class="blue-box-gradient">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="blue-box-gradient">Your text goes inside this tag.</div>' }}
		    </div> 
           
            <div class="blue-outline-box">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="blue-outline-box">Your text goes inside this tag.</div>' }}
		    </div>                            
            
            <h1>CTA Buttons</h1>
            
            <div class="cta-booking"><a data-target="#scheduling-page-clinic" data-toggle="modal" href="#">Book Free Assessment</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-booking"><a data-target="#scheduling-page-clinic" data-toggle="modal" href="#">Book Free Assessment</a></div>' }}
            </div>
            
            <div class="cta-booking"><a data-target="#scheduling-page-cataracts" data-toggle="modal" href="#">Book Cataracts Appointment</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-booking"><a data-target="#scheduling-page-cataracts" data-toggle="modal" href="#">Book Cataracts Appointment</a></div>' }}
            </div>            
            
            <div class="cta-download"><a href="https://www.personaleyes.com.au/lasik/the-ultimate-guide-to-lasik">Download the Ultimate Guide to LASIK</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-download"><a href="https://www.personaleyes.com.au/lasik/the-ultimate-guide-to-lasik">Download the Ultimate Guide to LASIK</a></div>' }}
			</div>
           
            <div class="cta-lasik-guide"><a href="https://www.personaleyes.com.au/lasik/the-ultimate-guide-to-lasik">Download Now</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-lasik-guide"><a href="https://www.personaleyes.com.au/lasik/the-ultimate-guide-to-lasik">Download Now</a></div>' }}
			</div>
                      
            <div class="cta-eye-test"><a href="https://www.personaleyes.com.au/vision/online-eye-test">Take The Test</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-eye-test"><a href="https://www.personaleyes.com.au/vision/online-eye-test">Take The Test</a></div>' }}
			</div>            
            
            <div class="cta-calculate-save"><a href="https://www.personaleyes.com.au/costs/cost-savings-calculator">Calculate Now</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-calculate-save"><a href="https://www.personaleyes.com.au/costs/cost-savings-calculator">Calculate Now</a></div>' }}
		    </div>    
               
            <div class="cta-suitability-quiz"><a href="https://www.personaleyes.com.au/vision-correction-suitability">Take The Quiz</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-suitability-quiz"><a href="https://www.personaleyes.com.au/vision-correction-suitability">Take The Test</a></div>' }}
			</div>          
                
            <div class="cta-booking-blue"><a data-target="#scheduling-page-clinic" data-toggle="modal" href="#">Book Your free appointment</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-booking-blue"><a data-target="#scheduling-page-clinic" data-toggle="modal" href="#">Book Your free appointment</a></div>' }}
		    </div>

            <div class="cta-booking-blue-header-needs"><a href="https://www.personaleyes.com.au/make-a-booking">Book an appointment today</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-booking-blue-header-needs"><a href="https://www.personaleyes.com.au/make-a-booking">BOOK AN APPOINTMENT TODAY</a></div>' }}
            </div>
            
            <div class="cta-info-pack"><a href="#modalInfoPack">Dowload Now</a></div>
            <div class="code-block">
               <h2>HTML</h2>
               {{ '<div class="cta-info-pack"><a href="#modalInfoPack">Download Now</a></div>' }}
            </div>
            
            <div class="box-cta-half">
                <div class="orange-gradient">
					<div><img src='{{ url('') }}/images/site/icon3.png' alt="Savings Calculator" text="Savings Calculator"></div>
					<div>Calculate how much you could save with LASIK</div>
					<div><a href="{{ url('') }}/costs/cost-savings-calculator">Calculate Now</a></div>            
                </div>
							
                <div class="blue-gradient">
					<div><img src='{{ url('') }}/images/site/icon4.png' alt="Suitability Quiz" text="Suitability Quiz"></div>
					<div>Take the Online Suitability Quiz</div>
					<div><a href="{{ url('') }}/vision-correction-suitability">Take The Quiz</a></div>            
                </div>
			</div>
            
            <div class="code-block">
               <h2>HTML</h2>
               
               {{ '<div class="box-cta-half">' }}<br><br>
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="orange-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon3.png" alt="Savings Calculator" text="Savings Calculator"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>Calculate how much you could save with LASIK</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/costs/cost-savings-calculator">Calculate Now</a></div>' }}<br>        
               &nbsp;&nbsp;{{ '</div>' }}<br><br>
							
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="blue-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon4.png" alt="Suitability Quiz" text="Suitability Quiz"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>Take the Online Suitability Quiz</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/vision-correction-suitability">Take The Quiz</a></div>' }}<br>
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br><br>
			   {{ '</div>' }}<br>
            </div>              
                    
            <div class="box-cta-third">
                <div class="orange-gradient">
					<div><img src='{{ url('') }}/images/site/icon1.png' alt="LASIK Guide" text="LASIK GUide"></div>
					<div>The Ultimate Australian Guide to LASIK</div>
					<div><a href="{{ url('') }}/lasik/the-ultimate-guide-to-lasik">Download Now</a></div>            
                </div>
							
                <div class="orange-gradient">
					<div><img src='{{ url('') }}/images/site/icon2.png' alt="Online Eye Test" text="Online Eye Test"></div>
					<div>Take the Australian Online Eye Test</div>
					<div><a href="{{ url('') }}/vision/online-eye-test">Take The Test</a></div>            
                </div>
                
                <div class="blue-gradient">
					<div><img src='{{ url('') }}/images/site/icon-calendar.png' alt="Free Appointment" text="Free Appointment"></div>
					<div>We're got you covered for all your ophthalmic needs</div>
					<div><a href="https://www.personaleyes.com.au/make-a-booking">Book An Appointment Today</a></div>            
                </div>
			</div> 
                    
            <div class="code-block">
               <h2>HTML</h2> 
                                   
               {{ '<div class="box-cta-third">' }}<br><br>
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="orange-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon1.png" alt="LASIK Guide" text="LASIK GUide"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>The Ultimate Australian Guide to LASIK</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/lasik/the-ultimate-guide-to-lasik">Download Now</a></div>' }}<br>            
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br><br>
							
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="orange-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon2.png" alt="Online Eye Test" text="Online Eye Test"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>Take the Australian Online Eye Test</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/vision/online-eye-test">Take The Test</a></div>' }}<br>            
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br><br>
                
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="blue-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon-calendar.png" alt="Free Appointment" text="Free Appointment"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>We\'re got you covered for all your ophthalmic needs</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="https://www.personaleyes.com.au/make-a-booking">Book An Appointment Today</a></div>' }}<br>            
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br><br> 
			   {{ '</div>' }}<br>                
            </div>
                    
            <div class="box-cta-half">
                <div class="blue-gradient">
					<div><img src='{{ url('') }}/images/site/icon5.png' alt="Info Booklet & Pricing" text="Info Booklet & Pricing"></div>
					<div>Get our in-depth LASIK Info Booklet & Pricing Plans</div>
					<div><a href="{{ url('') }}/lasik#modalInfoPack">Download Now</a></div>            
                </div>
                	
                <div class="blue-gradient">
					<div><img src='{{ url('') }}/images/site/icon-calendar.png' alt="Free Appointment" text="Free Appointment"></div>
					<div>We're got you covered for all your ophthalmic needs</div>
					<div><a href="https://www.personaleyes.com.au/make-a-booking">Book An Appointment Today</a></div>            
                </div>						                
			</div>
            
            <div class="code-block">
               <h2>HTML</h2>
               
               {{ '<div class="box-cta-half">' }}<br><br>
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="blue-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon5.png" alt="Info Booklet & Pricing" text="Info Booklet & Pricing"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>Get our in-depth LASIK Info Booklet & Pricing Plans</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/lasik#modalInfoPack">Calculate Now</a></div>' }}<br>        
               &nbsp;&nbsp;{{ '</div>' }}<br><br>
						              						              
			   &nbsp;&nbsp;&nbsp;&nbsp;{{ '<div class="blue-gradient">' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><img src="' . url('') . '/images/site/icon4.png" alt="Suitability Quiz" text="Suitability Quiz"></div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div>Take the Online Suitability Quiz</div>' }}<br>
			   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ '<div><a href="' . url('') . '/vision-correction-suitability">Take The Quiz</a></div>' }}<br>
               &nbsp;&nbsp;&nbsp;&nbsp;{{ '</div>' }}<br><br>			              
			   {{ '</div>' }}<br>
            </div> 
                                 
            <div class="cta-booking-blue-header-needs"><a href="https://www.personaleyes.com.au/make-a-booking">Book an appointment today</a></div>                  
                     
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
