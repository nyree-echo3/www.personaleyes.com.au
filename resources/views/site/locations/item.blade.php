<?php
// Set Meta Tags
$meta_title_inner = $location->meta_title;
$meta_keywords_inner = $location->meta_keywords;
$meta_description_inner = $location->meta_description;
?>
@extends('site/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/owl.carousel/dist/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/location-carousel.css?v=0.4') }}">
    <link rel="stylesheet" href="{{ asset('css/site/location.css?v=0.4') }}">
@endsection

@section('content')

    @include('site/partials/location-inner')

    <div class="blog-masthead ">
        <div class="container location-cont">
            <div class="row">
                <div class="col-12">
                    <h1>{{ $location->name }}</h1>
                </div>
            </div>
            <div class="row">
                @if($location->thumbnail)
                    <div class="col-12 col-md-8 location-description">
                        <div class="cb-services-cont">
                        @foreach($services as $service)
                        <div class="cb-service">
                            <i class="fas fa-check"></i> {{ $service['title'] }}
                        </div>
                        @endforeach
                        <div class="cb-service">
                            <i class="fas fa-check"></i> Glaucoma Management
                        </div>
                        <div class="cb-service">
                            <i class="fas fa-check"></i> Aviation Assessment
                        </div>
                        @php
                        $tick = false;
                        foreach($location->members as $specialist){
                            if($specialist->name == 'Dr Richard Parker' || $specialist->name == 'Dr Freny Kalapesi'){
                                $tick = true;
                            }
                        }
                        @endphp

                        @if($tick)
                        <div class="cb-service">
                            <i class="fas fa-check"></i> Oculoplastic
                        </div>
                        <div class="cb-service">
                            <i class="fas fa-check"></i> Eyelid Surgery
                        </div>
                        @endif

                        </div>
                        {!! $location->description !!}
                    </div>
                    <div class="col-4 d-none d-md-block">
                        <div class="location-thumbnail" style="background-image: url('{{ url('') }}/{{ $location->thumbnail }}')"></div>
                    </div>
                @else
                    <div class="col-12 location-description">{!! $location->description !!}</div>
                @endif
            </div>
            <div class="row contact-row">
                <div class="col-12">
                    <div class="contact cont-col">
                        <div class="head">Contact</div>
                        @if($location->phone)
                        <div class="contact-item">
                            <span>P</span>
                            <a href='tel:{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", $location->phone))) }}'>{{$location->phone}}</a>
                        </div>
                        @endif

                        @if($location->email)
                        <div class="contact-item">
                            <span>E</span>
                            <a href='mailto:{{ $location->email }}'>{{$location->email}}</a>
                        </div>
                        @endif

                        @if($location->fax)
                        <div class="contact-item">
                            <span>F</span> {{ $location->fax }}
                        </div>
                        @endif
                        <div class="btn-cont">
                            <a href="{{ url('make-a-booking') }}" class="location-contact-btn">BOOK AN APPOINTMENT</a>
                        </div>
                    </div>
                    <div class="address cont-col">
                        <div class="inner-cont">
                            <div class="head">Address</div>
                            {{ strip_tags($location->address) }} {{ $location->address2 }} </br> {{ $location->suburb }} {{ $location->state }} {{ $location->postcode }}
                            <div class="btn-cont">
                                <a href="{{ $location->fileName }}" target='_blank' class="location-contact-btn">DOWNLOAD DIRECTIONS</a>
                            </div>
                        </div>
                    </div>
                    <div class="opening-hours cont-col">
                        <div class="inner-cont">
                            <div class="head">Opening Hours</div>
                            {!! $location->opening_hours !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
        <div class="container-fluid map-container">{!! $location->map !!}</div>

    <!--<div class="container-fluid suburbs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Suburbs Sydney CBD Clinic serves:</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
        </div>-->

        <div class="container-fluid our-services">
            <div class="container">
                <div class="row justify-content-center no-gutters">
                    <div class="col-12 service-header-cont">
                        <h2>Our Services</h2>
                        <p>Some of the services that we provide in the {{ $location->name }} are:</p>
                    </div>
                </div>
                <div class="row justify-content-center no-gutters">

                    <div class="arrow-cont">
                        <div class="arrow owl-prev-services">
                            <i class="fas fa-angle-left"></i>
                        </div>
                    </div>
                    <div class="slides-cont">
                        <div class="owl-carousel-services owl-carousel owl-theme">
                            @foreach($services as $service)
                            <div class="slide-cont">
                                <div class="slide-icon">
                                    <a href="{{ url('') }}/{{ $service['url'] }}">
                                        <img src="{{ url('') }}/{{ $service['icon'] }}" />
                                    </a>
                                </div>
                                <div class="slide-title">{{ $service['title'] }}</div>
                                <div class="slide-desc">{{ $service['desc'] }}</div>
                                <div class="slide-more-button">
                                    <a href="{{ url('') }}/{{ $service['url'] }}">VIEW MORE</a>
                                </div>
                            </div>

                            @endforeach
                        </div>
                    </div>

                    <div class="arrow-cont">
                        <div class="arrow owl-next-services">
                            <i class="fas fa-angle-right"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        @if(count($location->members)>0)
        <div class="container-fluid our-specialists">
            <div class="container">
                <div class="row justify-content-center no-gutters">
                    <div class="col-12 service-header-cont">
                        <h2>Our Specialists</h2>
                        <p>Some of the services that we provide in the {{ $location->name }} are:</p>
                    </div>
                </div>
                <div class="row justify-content-center no-gutters">

                    <div class="arrow-cont">
                        <div class="arrow owl-prev-specialists">
                            <i class="fas fa-angle-left"></i>
                        </div>
                    </div>
                    <div class="slides-cont">
                        <div class="owl-carousel-specialists owl-carousel owl-theme">
                            @foreach($location->members as $specialist)
                                <div class="slide-cont">
                                    <div class="specialist-photo">
                                        <a href="{{ url('') }}/{{ $specialist->url }}">
                                            <img src="{{ url('') }}/{{ $specialist->photo }}" />
                                        </a>
                                    </div>
                                    <div class="slide-title">{{ $specialist->name }}</div>
                                    <div class="slide-desc">{{ $specialist->job_title }}</div>
                                    <div class="slide-role">{{ $specialist->role }}</div>
                                    <div class="slide-more-button">
                                        <a href="{{ url('') }}/{{ $specialist->url }}">VIEW PROFILE</a>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>

                    <div class="arrow-cont">
                        <div class="arrow owl-next-specialists">
                            <i class="fas fa-angle-right"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endif

        <div class="container-fluid pe-difference">
            <div class="container">
                <div class="diff-text-cont">
                    <div class="diff-text">
                        <h2>Our Services</h2>
                        <ol class="fa-ul">
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>25+ years specializing in Opthalmology</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>10 state-of-the-art clinics across NSW & ACT</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Safer & better results with the latest technology in Australia</li>
                            <li><span class="fa-li"><i class="fas fa-check"></i></span>Highly-experienced & industry-awarded specialists & surgeons</li>
                        </ol>
                        <div class="difference-book-btn-cont">
                        <a href="{{ url('make-a-booking') }}" class="difference-book-btn">BOOK AN APPOINTMENT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /.blog-masthead -->
@endsection

@section('scripts')
    <script src="{{ asset('/components/owl.carousel/dist/owl.carousel.js') }}"></script>
@endsection

@section('inline-scripts')
    <script type="text/javascript">

        var services = $('.owl-carousel-services');
        services.owlCarousel({
            loop: false,
            autoplay : false,
            //autoplaySpeed : 750,
            //autoplayTimeout : 6000,
            //autoplayHoverPause : true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                },
                992: {
                    items: 2,
                },
                1200: {
                    items: 3,
                }
            }
        });

        $('.owl-next-services').click(function () {
            services.trigger('next.owl.carousel');
        });

        $('.owl-prev-services').click(function () {
            services.trigger('prev.owl.carousel');
        });

        var specialists = $('.owl-carousel-specialists');
        specialists.owlCarousel({
            loop: false,
            autoplay : false,
            //autoplaySpeed : 750,
            //autoplayTimeout : 6000,
            //autoplayHoverPause : true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                },
                992: {
                    items: 2,
                },
                1200: {
                    items: 3,
                }
            }
        });

        $('.owl-next-specialists').click(function () {
            specialists.trigger('next.owl.carousel');
        });

        $('.owl-prev-specialists').click(function () {
            specialists.trigger('prev.owl.carousel');
        });

    </script>
@endsection