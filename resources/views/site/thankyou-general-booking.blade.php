@php
	// Set Meta Tags
	$meta_title_inner = "General Booking Thank-you | personalEYES";
	$meta_keywords_inner = "General Booking Thank-you | personalEYES";
	$meta_description_inner = "Thank-you for making a general booking appointment.";
@endphp

@extends('site/layouts/app')

@section('content')

@if(env('APP_ENV')=='production')
	<!-- Event snippet for BOOK GENERAL APPOINTMENT (ECHO) conversion page -->
	<script>
		gtag('event', 'conversion', {
			'send_to': 'AW-1064487622/KeFTCJmH8vMBEMaVy_sD',
			'transaction_id': ''
		});
	</script>

@endif

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 blog-main">	
			    
				    <div class="blog-book">  							       
						<h1 class="blog-post-title">General Booking Appointment</h1>
						<p>Thanks for making a general booking appointment.</p>
						<p>If you have any further questions, please <a href='{{ url('') }}contact'>contact us</a>.</p>
					</div>

				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@include('site/partials/index-panel-cta')
        
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection

@section('inline-scripts')
<script type="text/javascript">
    $( document ).ready(function() {
	@if(app('request')->input('popup')=='true')
    	$('#scheduling-page').modal('show');
	@endif
    })
</script>
@endsection
