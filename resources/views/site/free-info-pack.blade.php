@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">
	<div class="container">

		<div class="row">
			<div class="col-xl-8 col-lg-8 col-md-12 blog-main">
				<div class="blog-post">  
				       
					<h1 class="blog-post-title">Free Information Pack</h1>
                    
                    <!-- Hubspot Form -->
                    <!--[if lte IE 8]>
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
					<![endif]-->
					<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
					<script>
					  hbspt.forms.create({
						portalId: "5064462",
						formId: "5b13f84e-6e4b-41b7-8bf2-9e9cff9a3efb"
					});
					</script>
					<!-- End HubSpot Form -->
					
				</div><!-- /.blog-post -->                  
			</div><!-- /.blog-main -->
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@include('site/partials/index-news')
@include('site/partials/index-projects')
@include('site/partials/index-products')

@endsection
