@php
	// Set Meta Tags
	$meta_title_inner = "Our Eye Doctors, Surgeons & Ophthalmologists | personalEYES";
	$meta_keywords_inner = "Our Eye Doctors, Surgeons & Ophthalmologists | personalEYES";
	$meta_description_inner = "Our Eye Surgeons and Ophthalmologists have all been expertly trained within Australia and are all members of the RANZCO. Find out more about our team.";
@endphp

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                <div class="col-xl-10 col-lg-10 col-md-12 blog-main">                
                            
                    <div class="blog-post row">                        
                        @if($items)
                            <div class="col-lg-12">	
                               <h1>{{ $items[0]->category->name }}</h1>
						    </div>
							<div class="col-lg-12">
								<p>When it comes to your eyes, choosing the specialists with years of experience gives you the peace of mind you're getting the best possible advice. We’re proud to have a wonderfully friendly team of vastly experienced eye doctors, eye surgeons, ophthalmologists and other specialists at personalEYES.</p>
							</div>
                            @foreach($items as $item)                              
								<div class="col-lg-6">
							         <div class="team-a">
										 <a class='btn-back' href='{{  url('') . '/' . $module->slug . '/' . $item->slug }}'>		
											 <div class="team-div-img">									  							  
												 <div class="div-img">
													<img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}"> 											
												 </div>												
											 </div>
										 </a>

										 <div class="team-txt">
											 <div class="team-name-band-name"><h2>{{ $item->name }}</h2></div>
											<div class="team-name-band-title">{{ $item->job_title }}</div>
											<div class="team-name-band-role">{!! $item->role !!}</div>																																									
										 </div>																			     
								     </div>
								     
									 <a class='btn-back btn-back-team' href='{{  url('') . '/' . $module->slug . '/' . $item->slug }}'>READ MORE <i class="fas fa-chevron-right"></i></a>	
									 
									 <div class="team-a">
										 <a class='btn-back' href='{{ url("") }}/general-booking-page'>	
											<div class="cta-booking-dr">Book appointment<br>with {{ $item->name }}</div>						  										  										 					
										 </a>
									 </div>			 	
									 				 	
								</div>                       
                            @endforeach                       
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
