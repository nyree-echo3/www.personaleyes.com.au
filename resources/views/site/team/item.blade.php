<?php
// Set Meta Tags
$meta_title_inner = $team_member->meta_title;
$meta_keywords_inner = $team_member->meta_keywords;
$meta_description_inner = $team_member->meta_description;
?>

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/dist/baguetteBox.css') }}">
@endsection

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">                
        <div class="col-xl-10 col-lg-10 col-md-12 blog-main">

          <div class="blog-post">            			   
				<div class='row team-row'>								   					    		
						<div class="team-list-header">							
						    <div class="team-list-header-img">
							   @if ($team_member->photo)
								  <img src="{{ url('') }}{{$team_member->photo}}" alt="{{$team_member->name}}">   
							   @endif
							</div>
							
							<div class="team-list-header-txt">
								<h1 class="blog-post-title">{{ $team_member->name }}</h1>

								@if($team_member->job_title)
								<h3>{{ $team_member->job_title }}</h3>
								@endif
								@if($team_member->role)
								<h4>{{ $team_member->role }}</h4>
								@endif
								
								<div class="cta-booking-dr"><a href="{{ url("") }}/general-booking-page">Book An Appointment with {{ $team_member->name }}</a></div>
							</div>				      				            
					      </div>

							@if($team_member->phone)
							<strong>Phone</strong> : {{ $team_member->phone }}
							@endif
							@if($team_member->mobile)
							<strong>Mobile</strong> : {{ $team_member->mobile }}
							@endif
							@if($team_member->email)
							<strong>Email</strong> : {{ $team_member->email }}
							@endif
							@if($team_member->body)
							<div >{!! $team_member->body !!}</div>
							@endif

							<div class='btn-back'>
							   <a class='btn-back' href='javascript: window.history.go(-1);'><i class='fa fa-chevron-left'></i> back</a>
							</div>

					</div>                                
                                                                    
            </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/dist/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            baguetteBox.run('.cards-team', {animation: 'slideIn'});
        });
    </script>
@endsection