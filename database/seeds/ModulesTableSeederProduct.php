<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeederProduct extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('modules')->insert([
            'name' => 'Product',
            'display_name' => 'Products',
            'slug' => 'products',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
