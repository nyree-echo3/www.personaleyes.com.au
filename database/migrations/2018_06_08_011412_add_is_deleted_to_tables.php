<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDeletedToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact_messages', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('position');
        });

        Schema::table('document_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('position');
        });

        Schema::table('faq_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('gallery_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('members', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('member_types', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('archive_date');
        });

        Schema::table('news_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('position');
        });

        Schema::table('page_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('position');
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('position');
        });

        Schema::table('project_categories', function (Blueprint $table) {
            $table->enum('is_deleted', ['true','false'])->default('false')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
